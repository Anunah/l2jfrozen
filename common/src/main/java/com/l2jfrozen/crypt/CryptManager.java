package com.l2jfrozen.crypt;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.RSAKeyGenParameterSpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jfrozen.util.random.Rnd;

/**
 * User: vdidenko Date: 03.12.13 Time: 0:16
 */
public class CryptManager
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CryptManager.class.getName());
	// RSA Config
	private static final int KEYS_SIZE = 10;
	private static KeyPair[] _keyPairs;
	private static CryptManager cryptManager;

	private CryptManager()
	{
		try
		{
			loadRSAKeys();
			LOGGER.info("Cached " + _keyPairs.length + " RSA keys for Game Server communication.");
		}
		catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e)
		{
			e.printStackTrace();
		}
	}

	public static CryptManager getInstance()
	{
		if (cryptManager == null)
		{
			cryptManager = new CryptManager();
		}
		return cryptManager;
	}

	public KeyPair getKeyPair()
	{
		return _keyPairs[Rnd.nextInt(10)];
	}

	private void loadRSAKeys() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException
	{
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
		RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(512, RSAKeyGenParameterSpec.F4);
		keyGen.initialize(spec);

		_keyPairs = new KeyPair[KEYS_SIZE];
		for (int i = 0; i < KEYS_SIZE; i++)
		{
			_keyPairs[i] = keyGen.genKeyPair();
		}
	}
}
