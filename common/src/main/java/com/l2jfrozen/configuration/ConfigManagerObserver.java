package com.l2jfrozen.configuration;

/**
 * User: vdidenko Date: 11/20/13 Time: 5:42 PM
 */
public interface ConfigManagerObserver
{

	public void configurationLoad();
}
