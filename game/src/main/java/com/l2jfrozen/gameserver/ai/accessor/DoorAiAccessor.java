package com.l2jfrozen.gameserver.ai.accessor;

import com.l2jfrozen.gameserver.ai.L2DoorAI;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.L2Skill;
import com.l2jfrozen.gameserver.model.actor.instance.L2DoorInstance;
import com.l2jfrozen.gameserver.model.actor.position.L2CharPosition;
import com.l2jfrozen.gameserver.model.actor.stat.DoorStat;

/**
 * User: vadimDidenko
 * Date: 17.12.13
 * Time: 21:46
 */
public abstract class DoorAiAccessor extends AIAccessor<DoorStat, L2DoorAI, L2DoorInstance> {

    @Override
    public void moveTo(int x, int y, int z, int offset) {
    }

    @Override
    public void moveTo(int x, int y, int z) {
    }

    @Override
    public void stopMove(L2CharPosition pos) {
    }

    @Override
    public void doAttack(L2Character target) {
    }

    @Override
    public void doCast(L2Skill skill) {
    }
}
