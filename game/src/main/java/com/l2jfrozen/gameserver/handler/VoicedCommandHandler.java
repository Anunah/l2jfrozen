/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.handler;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.configuration.PowerPakConfig;
import com.l2jfrozen.gameserver.GameServer;
import com.l2jfrozen.gameserver.handler.voicedcommandhandlers.*;
import javolution.util.FastMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * This class ...
 *
 * @version $Revision: 1.1.4.6 $ $Date: 2009/05/12 19:44:09 $
 */
public class VoicedCommandHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(GameServer.class.getName());

    private static VoicedCommandHandler _instance;

    private Map<String, IVoicedCommandHandler> _datatable;

    public static VoicedCommandHandler getInstance() {
        if (_instance == null) {
            _instance = new VoicedCommandHandler();
        }

        return _instance;
    }

    private VoicedCommandHandler() {
        _datatable = new FastMap<>();

        registerVoicedCommandHandler(new StatsCmd());

        if (PowerPakConfig.CHAR_REPAIR) {
            registerVoicedCommandHandler(new RepairCmd());
        }

        if (PowerPakConfig.VOTE_COMMAND_ENABLED) {
            registerVoicedCommandHandler(new VotingCmd());
        }

        if (GameServerConfig.BANKING_SYSTEM_ENABLED) {
            registerVoicedCommandHandler(new BankingCmd());
        }

        if (GameServerConfig.CTF_COMMAND) {
            registerVoicedCommandHandler(new CTFCmd());
        }

        if (GameServerConfig.TVT_COMMAND) {
            registerVoicedCommandHandler(new TvTCmd());
        }

        if (GameServerConfig.DM_COMMAND) {
            registerVoicedCommandHandler(new DMCmd());
        }

        if (GameServerConfig.L2JMOD_ALLOW_WEDDING) {
            registerVoicedCommandHandler(new WeddingCmd());
        }

        if (GameServerConfig.ALLOW_VERSION_COMMAND) {
            registerVoicedCommandHandler(new VersionCmd());
        }

        if (GameServerConfig.ALLOW_AWAY_STATUS) {
            registerVoicedCommandHandler(new AwayCmd());
        }

        if (GameServerConfig.ALLOW_FARM1_COMMAND || GameServerConfig.ALLOW_FARM2_COMMAND || GameServerConfig.ALLOW_PVP1_COMMAND || GameServerConfig.ALLOW_PVP2_COMMAND) {
            registerVoicedCommandHandler(new FarmPvpCmd());
        }

        if (GameServerConfig.ALLOW_ONLINE_VIEW) {
            registerVoicedCommandHandler(new OnlineCmd());
        }

        if (GameServerConfig.OFFLINE_TRADE_ENABLE && GameServerConfig.OFFLINE_COMMAND2) {
            registerVoicedCommandHandler(new OfflineShopCmd());
        }

        LOGGER.info("VoicedCommandHandler: Loaded " + _datatable.size() + " handlers.");

    }

    public void registerVoicedCommandHandler(IVoicedCommandHandler handler) {

        for (String id : handler.getVoicedCommandList()) {
            if (GameServerConfig.HANDLER_DEBUG) {
                LOGGER.info("Adding handler for command " + id);
            }

            _datatable.put(id, handler);
        }
    }

    public IVoicedCommandHandler getVoicedCommandHandler(String voicedCommand) {
        String command = voicedCommand;

        if (voicedCommand.contains(" ")) {
            command = voicedCommand.substring(0, voicedCommand.indexOf(" "));
        }

        if (GameServerConfig.DEBUG) {
            LOGGER.debug("getting handler for command: " + command + " -> " + (_datatable.get(command) != null));
        }

        return _datatable.get(command);
    }

    /**
     * @return
     */
    public int size() {
        return _datatable.size();
    }
}