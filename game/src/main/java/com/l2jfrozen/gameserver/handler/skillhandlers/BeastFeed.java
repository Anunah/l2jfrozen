/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.handler.skillhandlers;

import com.l2jfrozen.gameserver.handler.ISkillHandler;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.L2Skill;
import com.l2jfrozen.gameserver.model.L2Skill.SkillType;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.util.GArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BeastFeed implements ISkillHandler<L2Character> {
    private static Logger LOGGER = LoggerFactory.getLogger(BeastFeed.class.getName());

    private static final SkillType[] SKILL_IDS = {SkillType.BEAST_FEED};

    @Override
    public <A extends L2Character> void useSkill(A activeChar, L2Skill skill, GArray<L2Character> targets) {
        if (!(activeChar instanceof L2PcInstance)) {
            return;
        }

        GArray<L2Character> targetList = skill.getTargetList(activeChar);

        if (targetList == null) {
            return;
        }

        LOGGER.debug("Beast Feed casting successes.");
    }

    @Override
    public SkillType[] getSkillIds() {
        return SKILL_IDS;
    }
}
