/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.handler.admincommandhandlers;

import com.l2jfrozen.database.manager.CommonManager;
import com.l2jfrozen.database.model.game.character.CharacterCustomData;
import com.l2jfrozen.gameserver.datatables.GmListTable;
import com.l2jfrozen.gameserver.handler.IAdminCommandHandler;
import com.l2jfrozen.gameserver.model.L2Object;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.model.entity.Announcements;
import com.l2jfrozen.gameserver.network.serverpackets.SocialAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AdminDonator implements IAdminCommandHandler {
    private static String[] ADMIN_COMMANDS =
            {
                    "admin_setdonator"
            };

    protected static final Logger LOGGER = LoggerFactory.getLogger(AdminDonator.class.getName());

    @Override
    public boolean useAdminCommand(String command, L2PcInstance activeChar) {
        /*
        if(!AdminCommandAccessRights.getInstance().hasAccess(command, activeChar.getAccessLevel())){
			return false;
		}
		
		if(Config.GMAUDIT)
		{
			Logger LOGGERAudit = LoggerFactory.getLogger("gmaudit");
			LogRecord record = new LogRecord( command);
			record.setParameters(new Object[]
			{
					"GM: " + activeChar.getName(), " to target [" + activeChar.getTarget() + "] "
			});
			LOGGERAudit.log(record);
		}
		*/

        if (activeChar == null) {
            return false;
        }

        if (command.startsWith("admin_setdonator")) {
            L2Object target = activeChar.getTarget();

            if (target instanceof L2PcInstance) {
                L2PcInstance targetPlayer = (L2PcInstance) target;
                boolean newDonator = !targetPlayer.isDonator();

                if (newDonator) {
                    targetPlayer.setDonator(true);
                    targetPlayer.updateNameTitleColor();
                    updateDatabase(targetPlayer, true);
                    sendMessages(true, targetPlayer, activeChar, false, true);
                    targetPlayer.broadcastPacket(new SocialAction(targetPlayer.getObjectId(), 16));
                    targetPlayer.broadcastUserInfo();
                } else {
                    targetPlayer.setDonator(false);
                    targetPlayer.updateNameTitleColor();
                    updateDatabase(targetPlayer, false);
                    sendMessages(false, targetPlayer, activeChar, false, true);
                    targetPlayer.broadcastUserInfo();
                }

                targetPlayer = null;
            } else {
                activeChar.sendMessage("Impossible to set a non Player Target as Donator.");
                LOGGER.info("GM: " + activeChar.getName() + " is trying to set a non Player Target as Donator.");

                return false;
            }

            target = null;
        }
        return true;
    }

    private void sendMessages(boolean forNewDonator, L2PcInstance player, L2PcInstance gm, boolean announce, boolean notifyGmList) {
        if (forNewDonator) {
            player.sendMessage(gm.getName() + " has granted Donator Status for you!");
            gm.sendMessage("You've granted Donator Status for " + player.getName());

            if (announce) {
                Announcements.getInstance().announceToAll(player.getName() + " has received Donator Status!");
            }

            if (notifyGmList) {
                GmListTable.broadcastMessageToGMs("Warn: " + gm.getName() + " has set " + player.getName() + " as Donator !");
            }
        } else {
            player.sendMessage(gm.getName() + " has revoked Donator Status from you!");
            gm.sendMessage("You've revoked Donator Status from " + player.getName());

            if (announce) {
                Announcements.getInstance().announceToAll(player.getName() + " has lost Donator Status!");
            }

            if (notifyGmList) {
                GmListTable.broadcastMessageToGMs("Warn: " + gm.getName() + " has removed Donator Status of player" + player.getName());
            }
        }
    }

    /**
     * @param player
     * @param newDonator
     */
    private void updateDatabase(L2PcInstance player, boolean newDonator) {

        try {
            // prevents any NPE.
            // ----------------
            if (player == null) {
                return;
            }

            if (newDonator) {
                CharacterCustomData characterCustomData = player.getCharacter().getCustomData();
                if (characterCustomData == null) {
                    characterCustomData = new CharacterCustomData();
                }

                characterCustomData.setCharacter(player.getCharacter());
                characterCustomData.setCharName(player.getName());
                characterCustomData.setHero(player.isHero() ? 1 : 0);
                characterCustomData.setNoble(player.isNoble() ? 1 : 0);
                characterCustomData.setDonator(1);
                if (characterCustomData.getId() == null) {
                    CommonManager.getInstance().saveNew(characterCustomData);
                    player.getCharacter().setCustomData(characterCustomData);
                } else {
                    CommonManager.getInstance().update(characterCustomData);
                }

            } else
            // deletes from database
            {
                CommonManager.getInstance().delete(player.getCharacter().getCustomData());
                player.getCharacter().setCustomData(null);
            }
        } catch (Exception e) {
            LOGGER.warn("Error: could not update database: ", e);
        }
    }

    /**
     * @return
     */
    @Override
    public String[] getAdminCommandList() {
        return ADMIN_COMMANDS;
    }
}
