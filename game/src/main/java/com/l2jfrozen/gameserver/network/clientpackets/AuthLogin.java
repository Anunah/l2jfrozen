/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.network.clientpackets;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.gameserver.network.L2GameClient;
import com.l2jfrozen.gameserver.network.serverpackets.L2GameServerPacket;
import com.l2jfrozen.gameserver.thread.LoginServerThread;
import com.l2jfrozen.gameserver.thread.LoginServerThread.SessionKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class AuthLogin extends L2GameClientPacket {
    private static Logger LOGGER = LoggerFactory.getLogger(AuthLogin.class.getName());

    // loginName + keys must match what the loginserver used.
    private String LOGGERinName;
    private int _playKey1;
    private int _playKey2;
    private int LOGGERinKey1;
    private int LOGGERinKey2;

    @Override
    protected void readImpl() {
        LOGGERinName = readS().toLowerCase();
        _playKey2 = readD();
        _playKey1 = readD();
        LOGGERinKey1 = readD();
        LOGGERinKey2 = readD();
    }

    @Override
    protected void runImpl() {
        SessionKey key = new SessionKey(LOGGERinKey1, LOGGERinKey2, _playKey1, _playKey2);

        if (GameServerConfig.DEBUG)
            LOGGER.info("DEBUG " + getType() + ": user: " + LOGGERinName + " key:" + key);

        L2GameClient client = getClient();

        // avoid potential exploits
        if (client.getAccountName() == null) {
            // Preventing duplicate login in case client login server socket was
            // disconnected or this packet was not sent yet
            if (LoginServerThread.getInstance().addGameServerLogin(LOGGERinName, client)) {
                client.setAccountName(LOGGERinName);
                LoginServerThread.getInstance().addWaitingClientAndSendRequest(
                        LOGGERinName, client, key);
            } else {
                client.close((L2GameServerPacket) null);
            }
        }
    }

    @Override
    public String getType() {
        return "[C] 08 AuthLogin";
    }
}