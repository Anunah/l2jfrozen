/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.network.clientpackets;

import com.l2jfrozen.database.manager.game.CharacterManager;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.database.model.game.character.CharacterFriendEntity;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.SystemMessageId;
import com.l2jfrozen.gameserver.network.serverpackets.FriendList;
import com.l2jfrozen.gameserver.network.serverpackets.SystemMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public final class RequestFriendDel extends L2GameClientPacket {
    private static Logger LOGGER = LoggerFactory.getLogger(RequestFriendDel.class.getName());

    private String _name;

    @Override
    protected void readImpl() {
        try {
            _name = readS();
        } catch (Exception e) {
            LOGGER.error("", e);

            _name = null;
        }
    }

    @Override
    protected void runImpl() {
        if (_name == null)
            return;

        SystemMessage sm;
        L2PcInstance activeChar = getClient().getActiveChar();
        if (activeChar == null)
            return;

        if (!activeChar.getFriendList().contains(_name)) {
            sm = new SystemMessage(SystemMessageId.S1_NOT_ON_YOUR_FRIENDS_LIST);
            sm.addString(_name);
            activeChar.sendPacket(sm);
            return;
        }

        try {
            L2PcInstance friend = L2World.getInstance().getPlayer(_name);
            CharacterEntity friendCharacter = null;

            Collection<CharacterFriendEntity> characterFriends = activeChar.getCharacter().getFriends().values();
            for (CharacterFriendEntity friend1 : characterFriends) {
                if (friend1.getFriendName().equals(_name)) {
                    friendCharacter = friend1.getFriend();
                }
            }

            if (friendCharacter == null) {
                sm = new SystemMessage(SystemMessageId.S1_NOT_ON_YOUR_FRIENDS_LIST);
                sm.addString(_name);
                activeChar.sendPacket(sm);
                return;
            }

            boolean success = CharacterManager.getInstance().deleteFriend(activeChar.getCharacter(), friendCharacter);
            if (success) {
                sm = new SystemMessage(SystemMessageId.S1_HAS_BEEN_DELETED_FROM_YOUR_FRIENDS_LIST);
                sm.addString(_name);
                activeChar.sendPacket(sm);
                activeChar.getFriendList().remove(_name);
                activeChar.sendPacket(new FriendList(activeChar));

                if (friend != null) {
                    friend.getFriendList().remove(activeChar.getName());
                    friend.sendPacket(new FriendList(friend));
                }
            }

        } catch (Exception e) {
            LOGGER.error("", e);

            LOGGER.info("could not del friend objectid: ", e);
        }
    }

    @Override
    public String getType() {
        return "[C] 61 RequestFriendDel";
    }
}