package com.l2jfrozen.gameserver.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * vadim.didenko
 * 2/27/14.
 */
public class ChatAudit {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChatAudit.class);
    private static ChatAudit instance;

    public static ChatAudit getInstance() {
        return instance == null ? instance = new ChatAudit() : instance;
    }

    public void chat(String message) {
        LOGGER.info(message);
    }
}
