/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.configuration;

import com.l2jfrozen.ClassMasterSettings;
import com.l2jfrozen.FService;
import com.l2jfrozen.gameserver.model.entity.olympiad.OlympiadPeriod;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.math.BigInteger;
import java.util.*;


@Service
public final class GameServerConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(GameServerConfig.class.getName());
    public static boolean ENABLE_OLYMPIAD_DISCONNECTION_DEBUG = false;
    //============================================================
    public static boolean EVERYBODY_HAS_ADMIN_RIGHTS;
    public static boolean SHOW_GM_LOGIN;
    public static boolean GM_STARTUP_INVISIBLE;
    public static boolean GM_SPECIAL_EFFECT;
    public static boolean GM_STARTUP_SILENCE;
    public static boolean GM_STARTUP_AUTO_LIST;
    public static String GM_ADMIN_MENU_STYLE;
    public static boolean GM_HERO_AURA;
    public static boolean GM_STARTUP_INVULNERABLE;
    public static boolean GM_ANNOUNCER_NAME;
    public static int MASTERACCESS_LEVEL;
    public static int USERACCESS_LEVEL;
    public static int MASTERACCESS_NAME_COLOR;
    public static int MASTERACCESS_TITLE_COLOR;
    //============================================================
    public static boolean CHECK_KNOWN;
    public static String DEFAULT_GLOBAL_CHAT;
    public static String DEFAULT_TRADE_CHAT;
    public static boolean TRADE_CHAT_WITH_PVP;
    public static int TRADE_PVP_AMOUNT;
    public static boolean GLOBAL_CHAT_WITH_PVP;
    public static int GLOBAL_PVP_AMOUNT;
    // Anti Brute force attack on login
    public static int BRUT_AVG_TIME;
    public static int BRUT_LOGON_ATTEMPTS;
    public static int BRUT_BAN_IP_TIME;
    public static int MAX_CHAT_LENGTH;
    public static boolean TRADE_CHAT_IS_NOOBLE;
    public static boolean PRECISE_DROP_CALCULATION;
    public static boolean MULTIPLE_ITEM_DROP;
    public static int DELETE_DAYS;
    public static int MAX_DRIFT_RANGE;
    public static boolean ALLOWFISHING;
    public static boolean ALLOW_MANOR;
    public static int AUTODESTROY_ITEM_AFTER;
    public static int HERB_AUTO_DESTROY_TIME;
    public static String PROTECTED_ITEMS;
    public static FastList<Integer> LIST_PROTECTED_ITEMS = new FastList<>();
    public static boolean DESTROY_DROPPED_PLAYER_ITEM;
    public static boolean DESTROY_EQUIPABLE_PLAYER_ITEM;
    public static boolean SAVE_DROPPED_ITEM;
    public static boolean EMPTY_DROPPED_ITEM_TABLE_AFTER_LOAD;
    public static int SAVE_DROPPED_ITEM_INTERVAL;
    public static boolean CLEAR_DROPPED_ITEM_TABLE;
    public static boolean ALLOW_DISCARDITEM;
    public static boolean ALLOW_FREIGHT;
    public static boolean ALLOW_WAREHOUSE;
    public static boolean WAREHOUSE_CACHE;
    public static int WAREHOUSE_CACHE_TIME;
    public static boolean ALLOW_WEAR;
    public static int WEAR_DELAY;
    public static int WEAR_PRICE;
    public static boolean ALLOW_LOTTERY;
    public static boolean ALLOW_RACE;
    public static boolean ALLOW_RENTPET;
    public static boolean ALLOW_BOAT;
    public static boolean ALLOW_CURSED_WEAPONS;
    public static boolean ALLOW_NPC_WALKERS;
    public static int MIN_NPC_ANIMATION;
    public static int MAX_NPC_ANIMATION;
    public static int MIN_MONSTER_ANIMATION;
    public static int MAX_MONSTER_ANIMATION;
    public static boolean ALLOW_USE_CURSOR_FOR_WALK;
    public static String COMMUNITY_TYPE;
    public static String BBS_DEFAULT;
    public static boolean SHOW_LEVEL_COMMUNITYBOARD;
    public static boolean SHOW_STATUS_COMMUNITYBOARD;
    public static int NAME_PAGE_SIZE_COMMUNITYBOARD;
    public static int NAME_PER_ROW_COMMUNITYBOARD;
    public static int PATH_NODE_RADIUS;
    public static int NEW_NODE_ID;
    public static String NEW_NODE_TYPE;
    public static boolean SHOW_NPC_LVL;
    public static int ZONE_TOWN;
    public static int DEFAULT_PUNISH;
    public static int DEFAULT_PUNISH_PARAM;
    public static boolean AUTODELETE_INVALID_QUEST_DATA;
    public static boolean GRIDS_ALWAYS_ON;
    public static int GRID_NEIGHBOR_TURNON_TIME;
    public static int GRID_NEIGHBOR_TURNOFF_TIME;
    public static boolean BYPASS_VALIDATION;
    public static boolean HIGH_RATE_SERVER_DROPS;
    public static boolean FORCE_COMPLETE_STATUS_UPDATE;
    //============================================================
    public static int PORT_GAME;
    public static String GAMESERVER_HOSTNAME;

    public static boolean ENABLE_DDOS_PROTECTION_SYSTEM;
    public static boolean ENABLE_DEBUG_DDOS_PROTECTION_SYSTEM;
    public static String DDOS_COMMAND_BLOCK;

    public static boolean RESERVE_HOST_ON_LOGIN = false;
    public static boolean RWHO_LOG;
    public static int RWHO_FORCE_INC;
    public static int RWHO_KEEP_STAT;
    public static int RWHO_MAX_ONLINE;
    public static boolean RWHO_SEND_TRASH;
    public static int RWHO_ONLINE_INCREMENT;
    public static float RWHO_PRIV_STORE_FACTOR;
    public static int RWHO_ARRAY[] = new int[13];
    //============================================================
    public static String SERVER_REVISION;
    public static String SERVER_BUILD_DATE;
    //============================================================
    public static boolean IS_TELNET_ENABLED;
    //============================================================
    public static IdFactoryType IDFACTORY_TYPE;
    public static boolean BAD_ID_CHECKING;
    public static ObjectMapType MAP_TYPE;
    public static ObjectSetType SET_TYPE;
    //============================================================
    public static int MAX_ITEM_IN_PACKET;
    public static boolean JAIL_IS_PVP;
    public static boolean JAIL_DISABLE_CHAT;
    public static int WYVERN_SPEED;
    public static int STRIDER_SPEED;
    public static boolean ALLOW_WYVERN_UPGRADER;
    public static int INVENTORY_MAXIMUM_NO_DWARF;
    public static int INVENTORY_MAXIMUM_DWARF;
    public static int INVENTORY_MAXIMUM_GM;
    public static int WAREHOUSE_SLOTS_NO_DWARF;
    public static int WAREHOUSE_SLOTS_DWARF;
    public static int WAREHOUSE_SLOTS_CLAN;
    public static int FREIGHT_SLOTS;
    public static FastList<Integer> LIST_NONDROPPABLE_ITEMS = new FastList<Integer>();
    public static String PET_RENT_NPC;
    public static FastList<Integer> LIST_PET_RENT_NPC = new FastList<Integer>();
    public static boolean EFFECT_CANCELING;
    public static double HP_REGEN_MULTIPLIER;
    public static double MP_REGEN_MULTIPLIER;
    public static double CP_REGEN_MULTIPLIER;
    public static double RAID_HP_REGEN_MULTIPLIER;
    public static double RAID_MP_REGEN_MULTIPLIER;
    public static double RAID_P_DEFENCE_MULTIPLIER;
    public static double RAID_M_DEFENCE_MULTIPLIER;
    public static double RAID_MINION_RESPAWN_TIMER;
    public static float RAID_MIN_RESPAWN_MULTIPLIER;
    public static float RAID_MAX_RESPAWN_MULTIPLIER;
    public static int STARTING_ADENA;
    public static int STARTING_AA;
    public static boolean ENABLE_AIO_SYSTEM;
    public static Map<Integer, Integer> AIO_SKILLS;
    public static boolean ALLOW_AIO_NCOLOR;
    public static int AIO_NCOLOR;
    public static boolean ALLOW_AIO_TCOLOR;
    public static int AIO_TCOLOR;
    public static boolean ALLOW_AIO_USE_GK;
    public static boolean ALLOW_AIO_USE_CM;
    public static boolean ANNOUNCE_CASTLE_LORDS;
    /**
     * Configuration to allow custom items to be given on character creation
     */
    public static boolean CUSTOM_STARTER_ITEMS_ENABLED;
    public static Map<Integer, Integer> STARTING_CUSTOM_ITEMS_F = new HashMap<>();
    public static Map<Integer, Integer> STARTING_CUSTOM_ITEMS_M = new HashMap<>();
    public static boolean DEEPBLUE_DROP_RULES;
    public static int UNSTUCK_INTERVAL;
    public static int DEATH_PENALTY_CHANCE;
    public static int PLAYER_SPAWN_PROTECTION;
    public static int PLAYER_TELEPORT_PROTECTION;
    public static boolean EFFECT_TELEPORT_PROTECTION;
    public static int PLAYER_FAKEDEATH_UP_PROTECTION;
    public static String PARTY_XP_CUTOFF_METHOD;
    public static int PARTY_XP_CUTOFF_LEVEL;
    public static double PARTY_XP_CUTOFF_PERCENT;
    public static double RESPAWN_RESTORE_CP;
    public static double RESPAWN_RESTORE_HP;
    public static double RESPAWN_RESTORE_MP;
    public static boolean RESPAWN_RANDOM_ENABLED;
    public static int RESPAWN_RANDOM_MAX_OFFSET;
    public static int MAX_PVTSTORE_SLOTS_DWARF;
    public static int MAX_PVTSTORE_SLOTS_OTHER;
    public static boolean PETITIONING_ALLOWED;
    public static int MAX_PETITIONS_PER_PLAYER;
    public static int MAX_PETITIONS_PENDING;
    public static boolean ANNOUNCE_MAMMON_SPAWN;
    public static boolean ENABLE_MODIFY_SKILL_DURATION;
    public static FastMap<Integer, Integer> SKILL_DURATION_LIST;
    /**
     * Chat Filter *
     */
    public static int CHAT_FILTER_PUNISHMENT_PARAM1;
    public static int CHAT_FILTER_PUNISHMENT_PARAM2;
    public static int CHAT_FILTER_PUNISHMENT_PARAM3;
    public static boolean USE_SAY_FILTER;
    public static String CHAT_FILTER_CHARS;
    public static String CHAT_FILTER_PUNISHMENT;
    public static ArrayList<String> FILTER_LIST = new ArrayList<String>();
    public static int FS_TIME_ATTACK;
    public static int FS_TIME_COOLDOWN;
    public static int FS_TIME_ENTRY;
    public static int FS_TIME_WARMUP;
    public static int FS_PARTY_MEMBER_COUNT;
    public static boolean ALLOW_QUAKE_SYSTEM;
    public static boolean ENABLE_ANTI_PVP_FARM_MSG;
    public static long CLICK_TASK;
    //============================================================
    public static float RATE_XP;
    public static float RATE_SP;
    public static float RATE_PARTY_XP;
    public static float RATE_PARTY_SP;
    public static float RATE_QUESTS_REWARD;
    public static float RATE_DROP_ADENA;
    public static float RATE_CONSUMABLE_COST;
    public static float RATE_DROP_ITEMS;
    public static float RATE_DROP_SEAL_STONES;
    public static float RATE_DROP_SPOIL;
    public static int RATE_DROP_MANOR;
    public static float RATE_DROP_QUEST;
    public static float RATE_KARMA_EXP_LOST;
    public static float RATE_SIEGE_GUARDS_PRICE;
    public static float RATE_DROP_COMMON_HERBS;
    public static float RATE_DROP_MP_HP_HERBS;
    public static float RATE_DROP_GREATER_HERBS;
    public static float RATE_DROP_SUPERIOR_HERBS;
    public static float RATE_DROP_SPECIAL_HERBS;
    public static int PLAYER_DROP_LIMIT;
    public static int PLAYER_RATE_DROP;
    public static int PLAYER_RATE_DROP_ITEM;
    public static int PLAYER_RATE_DROP_EQUIP;
    public static int PLAYER_RATE_DROP_EQUIP_WEAPON;
    public static float PET_XP_RATE;
    public static int PET_FOOD_RATE;
    public static float SINEATER_XP_RATE;
    public static int KARMA_DROP_LIMIT;
    public static int KARMA_RATE_DROP;
    public static int KARMA_RATE_DROP_ITEM;
    public static int KARMA_RATE_DROP_EQUIP;
    public static int KARMA_RATE_DROP_EQUIP_WEAPON;
    /**
     * RB rate *
     */
    public static float ADENA_BOSS;
    public static float ADENA_RAID;
    public static float ADENA_MINON;
    public static float ITEMS_BOSS;
    public static float ITEMS_RAID;
    public static float ITEMS_MINON;
    public static float SPOIL_BOSS;
    public static float SPOIL_RAID;
    public static float SPOIL_MINON;
    //============================================================
    public static boolean AUTO_LOOT;
    public static boolean AUTO_LOOT_BOSS;
    public static boolean AUTO_LOOT_HERBS;
    public static boolean REMOVE_CASTLE_CIRCLETS;
    public static double ALT_WEIGHT_LIMIT;
    public static boolean ALT_GAME_SKILL_LEARN;
    public static boolean AUTO_LEARN_SKILLS;
    public static boolean ALT_GAME_CANCEL_BOW;
    public static boolean ALT_GAME_CANCEL_CAST;
    public static boolean ALT_GAME_TIREDNESS;
    public static int ALT_PARTY_RANGE;
    public static int ALT_PARTY_RANGE2;
    public static boolean ALT_GAME_SHIELD_BLOCKS;
    public static int ALT_PERFECT_SHLD_BLOCK;
    public static boolean ALT_GAME_MOB_ATTACK_AI;
    public static boolean ALT_MOB_AGRO_IN_PEACEZONE;
    public static boolean ALT_GAME_FREIGHTS;
    public static int ALT_GAME_FREIGHT_PRICE;
    public static float ALT_GAME_SKILL_HIT_RATE;
    public static boolean ALT_GAME_DELEVEL;
    public static boolean ALT_GAME_MAGICFAILURES;
    public static boolean ALT_GAME_FREE_TELEPORT;
    public static boolean ALT_RECOMMEND;
    public static boolean ALT_GAME_SUBCLASS_WITHOUT_QUESTS;
    public static boolean ALT_RESTORE_EFFECTS_ON_SUBCLASS_CHANGE;
    public static boolean ALT_GAME_VIEWNPC;
    public static int ALT_CLAN_MEMBERS_FOR_WAR;
    public static int ALT_CLAN_JOIN_DAYS;
    public static int ALT_CLAN_CREATE_DAYS;
    public static int ALT_CLAN_DISSOLVE_DAYS;
    public static int ALT_ALLY_JOIN_DAYS_WHEN_LEAVED;
    public static int ALT_ALLY_JOIN_DAYS_WHEN_DISMISSED;
    public static int ALT_ACCEPT_CLAN_DAYS_WHEN_DISMISSED;
    public static int ALT_CREATE_ALLY_DAYS_WHEN_DISSOLVED;
    public static boolean ALT_GAME_NEW_CHAR_ALWAYS_IS_NEWBIE;
    public static boolean ALT_MEMBERS_CAN_WITHDRAW_FROM_CLANWH;
    public static int ALT_MAX_NUM_OF_CLANS_IN_ALLY;
    public static boolean LIFE_CRYSTAL_NEEDED;
    public static boolean SP_BOOK_NEEDED;
    public static boolean ES_SP_BOOK_NEEDED;
    public static boolean ALT_PRIVILEGES_SECURE_CHECK;
    public static int ALT_PRIVILEGES_DEFAULT_LEVEL;
    public static int ALT_MANOR_REFRESH_TIME;
    public static int ALT_MANOR_REFRESH_MIN;
    public static int ALT_MANOR_APPROVE_TIME;
    public static int ALT_MANOR_APPROVE_MIN;
    public static int ALT_MANOR_MAINTENANCE_PERIOD;
    public static boolean ALT_MANOR_SAVE_ALL_ACTIONS;
    public static int ALT_MANOR_SAVE_PERIOD_RATE;
    public static int ALT_LOTTERY_PRIZE;
    public static int ALT_LOTTERY_TICKET_PRICE;
    public static float ALT_LOTTERY_5_NUMBER_RATE;
    public static float ALT_LOTTERY_4_NUMBER_RATE;
    public static float ALT_LOTTERY_3_NUMBER_RATE;
    public static int ALT_LOTTERY_2_AND_1_NUMBER_PRIZE;
    public static int RIFT_MIN_PARTY_SIZE;
    public static int RIFT_SPAWN_DELAY;
    public static int RIFT_MAX_JUMPS;
    public static int RIFT_AUTO_JUMPS_TIME_MIN;
    public static int RIFT_AUTO_JUMPS_TIME_MAX;
    public static int RIFT_ENTER_COST_RECRUIT;
    public static int RIFT_ENTER_COST_SOLDIER;
    public static int RIFT_ENTER_COST_OFFICER;
    public static int RIFT_ENTER_COST_CAPTAIN;
    public static int RIFT_ENTER_COST_COMMANDER;
    public static int RIFT_ENTER_COST_HERO;
    public static float RIFT_BOSS_ROOM_TIME_MUTIPLY;
    public static float ALT_GAME_EXPONENT_XP;
    public static float ALT_GAME_EXPONENT_SP;
    public static boolean FORCE_INVENTORY_UPDATE;
    public static boolean ALLOW_GUARDS;
    public static boolean ALLOW_CLASS_MASTERS;
    public static boolean ALLOW_CLASS_MASTERS_FIRST_CLASS;
    public static boolean ALLOW_CLASS_MASTERS_SECOND_CLASS;
    public static boolean ALLOW_CLASS_MASTERS_THIRD_CLASS;
    public static boolean CLASS_MASTER_STRIDER_UPDATE;
    public static ClassMasterSettings CLASS_MASTER_SETTINGS;
    public static boolean ALT_GAME_KARMA_PLAYER_CAN_BE_KILLED_IN_PEACEZONE;
    public static boolean ALT_GAME_KARMA_PLAYER_CAN_SHOP;
    public static boolean ALT_GAME_KARMA_PLAYER_CAN_USE_GK;
    public static boolean ALT_GAME_KARMA_PLAYER_CAN_TELEPORT;
    public static boolean ALT_GAME_KARMA_PLAYER_CAN_TRADE;
    public static boolean ALT_GAME_KARMA_PLAYER_CAN_USE_WAREHOUSE;
    public static boolean ALT_KARMA_TELEPORT_TO_FLORAN;
    public static byte BUFFS_MAX_AMOUNT;
    public static byte DEBUFFS_MAX_AMOUNT;
    public static boolean AUTO_LEARN_DIVINE_INSPIRATION;
    public static boolean DIVINE_SP_BOOK_NEEDED;
    public static boolean ALLOW_REMOTE_CLASS_MASTERS;
    public static boolean MAX_LEVEL_DONT_LOSE_EXP;
    public static boolean DONT_DESTROY_SS;
    public static int MAX_LEVEL_NEWBIE;
    public static int MAX_LEVEL_NEWBIE_STATUS;
    public static int STANDARD_RESPAWN_DELAY;
    public static int ALT_RECOMMENDATIONS_NUMBER;
    public static int RAID_RANKING_1ST;
    public static int RAID_RANKING_2ND;
    public static int RAID_RANKING_3RD;
    public static int RAID_RANKING_4TH;
    public static int RAID_RANKING_5TH;
    public static int RAID_RANKING_6TH;
    public static int RAID_RANKING_7TH;
    public static int RAID_RANKING_8TH;
    public static int RAID_RANKING_9TH;
    public static int RAID_RANKING_10TH;
    public static int RAID_RANKING_UP_TO_50TH;
    public static int RAID_RANKING_UP_TO_100TH;
    public static boolean EXPERTISE_PENALTY;
    public static boolean MASTERY_PENALTY;
    public static int LEVEL_TO_GET_PENALITY;
    public static boolean MASTERY_WEAPON_PENALTY;
    public static int LEVEL_TO_GET_WEAPON_PENALITY;
    public static int ACTIVE_AUGMENTS_START_REUSE_TIME;
    public static boolean NPC_ATTACKABLE;
    /**
     * if npc_attackable is true, you can define who will receive 0 damages
     */
    public static List<Integer> INVUL_NPC_LIST;
    /**
     * Config for activeChar Attack Npcs in the list
     */
    public static boolean DISABLE_ATTACK_NPC_TYPE;
    /**
     * List of NPC types that won't allow casting
     */
    public static FastList<String> LIST_ALLOWED_NPC_TYPES = new FastList<String>();
    public static boolean SELL_BY_ITEM;
    public static int SELL_ITEM;
    public static int ALLOWED_SUBCLASS;
    public static byte BASE_SUBCLASS_LEVEL;
    public static byte MAX_SUBCLASS_LEVEL;
    public static String DISABLE_BOW_CLASSES_STRING;
    public static FastList<Integer> DISABLE_BOW_CLASSES = new FastList<Integer>();
    public static boolean ALT_MOBS_STATS_BONUS;
    public static boolean ALT_PETS_STATS_BONUS;
    //============================================================
    public static boolean ALT_GAME_REQUIRE_CASTLE_DAWN;
    public static boolean ALT_GAME_REQUIRE_CLAN_CASTLE;
    public static boolean ALT_REQUIRE_WIN_7S;
    public static int ALT_FESTIVAL_MIN_PLAYER;
    public static int ALT_MAXIMUM_PLAYER_CONTRIB;
    public static long ALT_FESTIVAL_MANAGER_START;
    public static long ALT_FESTIVAL_LENGTH;
    public static long ALT_FESTIVAL_CYCLE_LENGTH;
    public static long ALT_FESTIVAL_FIRST_SPAWN;
    public static long ALT_FESTIVAL_FIRST_SWARM;
    public static long ALT_FESTIVAL_SECOND_SPAWN;
    public static long ALT_FESTIVAL_SECOND_SWARM;
    public static long ALT_FESTIVAL_CHEST_SPAWN;
    //============================================================
    public static long CH_TELE_FEE_RATIO;
    public static int CH_TELE1_FEE;
    public static int CH_TELE2_FEE;
    public static long CH_ITEM_FEE_RATIO;
    public static int CH_ITEM1_FEE;
    public static int CH_ITEM2_FEE;
    public static int CH_ITEM3_FEE;
    public static long CH_MPREG_FEE_RATIO;
    public static int CH_MPREG1_FEE;
    public static int CH_MPREG2_FEE;
    public static int CH_MPREG3_FEE;
    public static int CH_MPREG4_FEE;
    public static int CH_MPREG5_FEE;
    public static long CH_HPREG_FEE_RATIO;
    public static int CH_HPREG1_FEE;
    public static int CH_HPREG2_FEE;
    public static int CH_HPREG3_FEE;
    public static int CH_HPREG4_FEE;
    public static int CH_HPREG5_FEE;
    public static int CH_HPREG6_FEE;
    public static int CH_HPREG7_FEE;
    public static int CH_HPREG8_FEE;
    public static int CH_HPREG9_FEE;
    public static int CH_HPREG10_FEE;
    public static int CH_HPREG11_FEE;
    public static int CH_HPREG12_FEE;
    public static int CH_HPREG13_FEE;
    public static long CH_EXPREG_FEE_RATIO;
    public static int CH_EXPREG1_FEE;
    public static int CH_EXPREG2_FEE;
    public static int CH_EXPREG3_FEE;
    public static int CH_EXPREG4_FEE;
    public static int CH_EXPREG5_FEE;
    public static int CH_EXPREG6_FEE;
    public static int CH_EXPREG7_FEE;
    public static long CH_SUPPORT_FEE_RATIO;
    public static int CH_SUPPORT1_FEE;
    public static int CH_SUPPORT2_FEE;
    public static int CH_SUPPORT3_FEE;
    public static int CH_SUPPORT4_FEE;
    public static int CH_SUPPORT5_FEE;
    public static int CH_SUPPORT6_FEE;
    public static int CH_SUPPORT7_FEE;
    public static int CH_SUPPORT8_FEE;
    public static long CH_CURTAIN_FEE_RATIO;
    public static int CH_CURTAIN1_FEE;
    public static int CH_CURTAIN2_FEE;
    public static long CH_FRONT_FEE_RATIO;
    public static int CH_FRONT1_FEE;
    public static int CH_FRONT2_FEE;
    //============================================================
    public static int DEVASTATED_DAY;
    public static int DEVASTATED_HOUR;
    public static int DEVASTATED_MINUTES;
    public static int PARTISAN_DAY;
    public static int PARTISAN_HOUR;
    public static int PARTISAN_MINUTES;
    //============================================================
    public static boolean L2JMOD_CHAMPION_ENABLE;
    public static int L2JMOD_CHAMPION_FREQUENCY;
    public static int L2JMOD_CHAMP_MIN_LVL;
    public static int L2JMOD_CHAMP_MAX_LVL;
    public static int L2JMOD_CHAMPION_HP;
    public static int L2JMOD_CHAMPION_REWARDS;
    public static int L2JMOD_CHAMPION_ADENAS_REWARDS;
    public static float L2JMOD_CHAMPION_HP_REGEN;
    public static float L2JMOD_CHAMPION_ATK;
    public static float L2JMOD_CHAMPION_SPD_ATK;
    public static int L2JMOD_CHAMPION_REWARD;
    public static int L2JMOD_CHAMPION_REWARD_ID;
    public static int L2JMOD_CHAMPION_REWARD_QTY;
    public static String L2JMOD_CHAMP_TITLE;
    //============================================================
    public static boolean L2JMOD_ALLOW_WEDDING;
    public static int L2JMOD_WEDDING_PRICE;
    public static boolean L2JMOD_WEDDING_PUNISH_INFIDELITY;
    public static boolean L2JMOD_WEDDING_TELEPORT;
    public static int L2JMOD_WEDDING_TELEPORT_PRICE;
    public static int L2JMOD_WEDDING_TELEPORT_DURATION;
    public static int L2JMOD_WEDDING_NAME_COLOR_NORMAL;
    public static int L2JMOD_WEDDING_NAME_COLOR_GEY;
    public static int L2JMOD_WEDDING_NAME_COLOR_LESBO;
    public static boolean L2JMOD_WEDDING_SAMESEX;
    public static boolean L2JMOD_WEDDING_FORMALWEAR;
    public static int L2JMOD_WEDDING_DIVORCE_COSTS;
    public static boolean WEDDING_GIVE_CUPID_BOW;
    public static boolean ANNOUNCE_WEDDING;
    //============================================================
    public static String TVT_EVEN_TEAMS;
    public static boolean TVT_ALLOW_INTERFERENCE;
    public static boolean TVT_ALLOW_POTIONS;
    public static boolean TVT_ALLOW_SUMMON;
    public static boolean TVT_ON_START_REMOVE_ALL_EFFECTS;
    public static boolean TVT_ON_START_UNSUMMON_PET;
    public static boolean TVT_REVIVE_RECOVERY;
    public static boolean TVT_ANNOUNCE_TEAM_STATS;
    public static boolean TVT_ANNOUNCE_REWARD;
    public static boolean TVT_PRICE_NO_KILLS;
    public static boolean TVT_JOIN_CURSED;
    public static boolean TVT_COMMAND;
    public static long TVT_REVIVE_DELAY;
    public static boolean TVT_OPEN_FORT_DOORS;
    public static boolean TVT_CLOSE_FORT_DOORS;
    public static boolean TVT_OPEN_ADEN_COLOSSEUM_DOORS;
    public static boolean TVT_CLOSE_ADEN_COLOSSEUM_DOORS;
    public static int TVT_TOP_KILLER_REWARD;
    public static int TVT_TOP_KILLER_QTY;
    public static boolean TVT_AURA;
    public static boolean TVT_STATS_LOGGER;
    //============================================================
    public static int TW_TOWN_ID;
    public static boolean TW_ALL_TOWNS;
    public static int TW_ITEM_ID;
    public static int TW_ITEM_AMOUNT;
    public static boolean TW_ALLOW_KARMA;
    public static boolean TW_DISABLE_GK;
    public static boolean TW_RESS_ON_DIE;
    //============================================================
    public static boolean IRC_ENABLED;
    public static boolean IRC_LOG_CHAT;
    public static boolean IRC_SSL;
    public static String IRC_SERVER;
    public static int IRC_PORT;
    public static String IRC_PASS;
    public static String IRC_NICK;
    public static String IRC_USER;
    public static String IRC_NAME;
    public static boolean IRC_NICKSERV;
    public static String IRC_NICKSERV_NAME;
    public static String IRC_NICKSERV_COMMAND;
    public static String IRC_LOGIN_COMMAND;
    public static String IRC_CHANNEL;
    public static String IRC_FROM_GAME_TYPE;
    public static String IRC_TO_GAME_TYPE;
    public static String IRC_TO_GAME_SPECIAL_CHAR;
    public static String IRC_TO_GAME_DISPLAY;
    public static boolean IRC_ANNOUNCE;
    public static String IRC_NO_GM_MSG;
    public static String IRC_NO_PLAYER_ONLINE;
    public static String IRC_PLAYER_ONLINE;
    public static String IRC_MSG_START;
    //============================================================
    public static boolean REBIRTH_ENABLE;
    public static String[] REBIRTH_ITEM_PRICE;
    public static String[] REBIRTH_MAGE_SKILL;
    public static String[] REBIRTH_FIGHTER_SKILL;
    public static int REBIRTH_MIN_LEVEL;
    public static int REBIRTH_MAX;
    public static int REBIRTH_RETURN_TO_LEVEL;
    //============================================================
    public static boolean PCB_ENABLE;
    public static int PCB_MIN_LEVEL;
    public static int PCB_POINT_MIN;
    public static int PCB_POINT_MAX;
    public static int PCB_CHANCE_DUAL_POINT;
    public static int PCB_INTERVAL;
    //============================================================
    public static boolean ALT_DEV_NO_QUESTS;
    public static boolean ALT_DEV_NO_SPAWNS;
    public static boolean ALT_DEV_NO_SCRIPT;
    public static boolean ALT_DEV_NO_RB;
    //============================================================
    public static boolean ALT_DEV_NO_AI;
    //============================================================
    public static boolean SKILLSDEBUG;
    public static boolean DEBUG;
    public static boolean ASSERT;
    public static boolean DEVELOPER;
    public static boolean ZONE_DEBUG;
    public static boolean POSITION_DEBUG;
    public static boolean DOOR_DEBUG;
    public static boolean ITEMLIST_DEBUG;
    public static boolean AUTOHANDLER_DEBUG;
    public static boolean HANDLER_DEBUG;
    public static boolean BOSS_DEBUG;
    public static boolean SEVENSIGNFESTIVAL_DEBUG;
    public static boolean ENABLE_ALL_EXCEPTIONS = true;
    public static boolean SERVER_LIST_TESTSERVER;
    public static boolean SERVER_LIST_BRACKET;
    public static boolean SERVER_LIST_CLOCK;
    public static boolean SERVER_GMONLY;
    public static int REQUEST_ID;
    public static boolean ACCEPT_ALTERNATE_ID;
    public static int MAXIMUM_ONLINE_USERS;
    public static String CNAME_TEMPLATE;
    public static String PET_NAME_TEMPLATE;
    public static String CLAN_NAME_TEMPLATE;
    public static String ALLY_NAME_TEMPLATE;
    public static int MAX_CHARACTERS_NUMBER_PER_IP;
    public static int MAX_CHARACTERS_NUMBER_PER_ACCOUNT;
    public static int MIN_PROTOCOL_REVISION;
    public static int MAX_PROTOCOL_REVISION;
    public static boolean GMAUDIT;
    public static boolean LOG_CHAT;
    public static boolean LOG_ITEMS;
    public static boolean LOG_HIGH_DAMAGES;
    public static boolean GAMEGUARD_L2NET_CHECK;

    public static boolean LAZY_CACHE;
    public static boolean ENABLE_CACHE_INFO = false;
    //============================================================
    public static boolean IS_CRAFTING_ENABLED;
    public static int DWARF_RECIPE_LIMIT;
    public static int COMMON_RECIPE_LIMIT;
    public static boolean ALT_GAME_CREATION;
    public static double ALT_GAME_CREATION_SPEED;
    public static double ALT_GAME_CREATION_XP_RATE;
    public static double ALT_GAME_CREATION_SP_RATE;
    public static boolean ALT_BLACKSMITH_USE_RECIPES;
    //============================================================
    public static boolean ALLOW_AWAY_STATUS;
    public static int AWAY_TIMER;
    public static int BACK_TIMER;
    public static int AWAY_TITLE_COLOR;
    public static boolean AWAY_PLAYER_TAKE_AGGRO;
    public static boolean AWAY_PEACE_ZONE;
    //============================================================
    public static boolean BANKING_SYSTEM_ENABLED;
    public static int BANKING_SYSTEM_GOLDBARS;
    public static int BANKING_SYSTEM_ADENA;
    //============================================================
    public static boolean OFFLINE_TRADE_ENABLE;
    public static boolean OFFLINE_CRAFT_ENABLE;
    public static boolean OFFLINE_SET_NAME_COLOR;
    public static int OFFLINE_NAME_COLOR;
    public static boolean OFFLINE_COMMAND1;
    public static boolean OFFLINE_COMMAND2;
    public static boolean OFFLINE_LOGOUT;
    public static boolean OFFLINE_SLEEP_EFFECT;
    public static boolean RESTORE_OFFLINERS;
    public static int OFFLINE_MAX_DAYS;
    public static boolean OFFLINE_DISCONNECT_FINISHED;
    //============================================================
    public static boolean GM_TRADE_RESTRICTED_ITEMS;
    public static boolean GM_CRITANNOUNCER_NAME;
    public static boolean GM_RESTART_FIGHTING;
    public static boolean PM_MESSAGE_ON_START;
    public static boolean SERVER_TIME_ON_START;
    public static String PM_SERVER_NAME;
    public static String PM_TEXT1;
    public static String PM_TEXT2;
    public static boolean NEW_PLAYER_EFFECT;
    //============================================================
    public static boolean DM_ALLOW_INTERFERENCE;
    public static boolean DM_ALLOW_POTIONS;
    public static boolean DM_ALLOW_SUMMON;
    public static boolean DM_JOIN_CURSED;
    public static boolean DM_ON_START_REMOVE_ALL_EFFECTS;
    public static boolean DM_ON_START_UNSUMMON_PET;
    public static long DM_REVIVE_DELAY;
    public static boolean DM_COMMAND;
    public static boolean DM_ENABLE_KILL_REWARD;
    public static int DM_KILL_REWARD_ID;
    public static int DM_KILL_REWARD_AMOUNT;
    public static boolean DM_ANNOUNCE_REWARD;
    public static boolean DM_REVIVE_RECOVERY;
    public static int DM_SPAWN_OFFSET;
    public static boolean DM_STATS_LOGGER;
    public static boolean DM_ALLOW_HEALER_CLASSES;
    public static boolean DM_REMOVE_BUFFS_ON_DIE;
    //============================================================
    public static String CTF_EVEN_TEAMS;
    public static boolean CTF_ALLOW_INTERFERENCE;
    public static boolean CTF_ALLOW_POTIONS;
    public static boolean CTF_ALLOW_SUMMON;
    public static boolean CTF_ON_START_REMOVE_ALL_EFFECTS;
    public static boolean CTF_ON_START_UNSUMMON_PET;
    public static boolean CTF_ANNOUNCE_TEAM_STATS;
    public static boolean CTF_ANNOUNCE_REWARD;
    public static boolean CTF_JOIN_CURSED;
    public static boolean CTF_REVIVE_RECOVERY;
    public static boolean CTF_COMMAND;
    public static boolean CTF_AURA;
    public static boolean CTF_STATS_LOGGER;
    public static int CTF_SPAWN_OFFSET;
    //============================================================
    public static boolean ONLINE_PLAYERS_ON_LOGIN;
    public static boolean SHOW_SERVER_VERSION;
    public static boolean SUBSTUCK_SKILLS;
    public static boolean ALT_SERVER_NAME_ENABLED;
    public static boolean ANNOUNCE_TO_ALL_SPAWN_RB;
    public static boolean ANNOUNCE_TRY_BANNED_ACCOUNT;
    public static String ALT_Server_Name;
    public static boolean DONATOR_NAME_COLOR_ENABLED;
    public static int DONATOR_NAME_COLOR;
    public static int DONATOR_TITLE_COLOR;
    public static float DONATOR_XPSP_RATE;
    public static float DONATOR_ADENA_RATE;
    public static float DONATOR_DROP_RATE;
    public static float DONATOR_SPOIL_RATE;
    public static boolean CUSTOM_SPAWNLIST_TABLE;
    public static boolean SAVE_GMSPAWN_ON_CUSTOM;
    public static boolean DELETE_GMSPAWN_ON_CUSTOM;
    public static boolean CUSTOM_NPC_TABLE = true;
    public static boolean CUSTOM_ITEM_TABLES = true;
    public static boolean CUSTOM_ARMORSETS_TABLE = true;
    public static boolean CUSTOM_TELEPORT_TABLE = true;
    public static boolean CUSTOM_DROPLIST_TABLE = true;
    public static boolean CUSTOM_MERCHANT_TABLES = true;
    public static boolean ALLOW_SIMPLE_STATS_VIEW;
    public static boolean ALLOW_DETAILED_STATS_VIEW;
    public static boolean ALLOW_ONLINE_VIEW;
    public static boolean WELCOME_HTM;
    public static String ALLOWED_SKILLS;
    public static FastList<Integer> ALLOWED_SKILLS_LIST = new FastList<Integer>();
    public static boolean PROTECTOR_PLAYER_PK;
    public static boolean PROTECTOR_PLAYER_PVP;
    public static int PROTECTOR_RADIUS_ACTION;
    public static int PROTECTOR_SKILLID;
    public static int PROTECTOR_SKILLLEVEL;
    public static int PROTECTOR_SKILLTIME;
    public static String PROTECTOR_MESSAGE;
    public static boolean CASTLE_SHIELD;
    public static boolean CLANHALL_SHIELD;
    public static boolean APELLA_ARMORS;
    public static boolean OATH_ARMORS;
    public static boolean CASTLE_CROWN;
    public static boolean CASTLE_CIRCLETS;
    public static boolean KEEP_SUBCLASS_SKILLS;
    public static boolean CHAR_TITLE;
    public static String ADD_CHAR_TITLE;
    public static boolean NOBLE_CUSTOM_ITEMS;
    public static boolean HERO_CUSTOM_ITEMS;
    public static boolean ALLOW_CREATE_LVL;
    public static int CHAR_CREATE_LVL;
    public static boolean SPAWN_CHAR;
    /**
     * X Coordinate of the SPAWN_CHAR setting.
     */
    public static int SPAWN_X;
    /**
     * Y Coordinate of the SPAWN_CHAR setting.
     */
    public static int SPAWN_Y;
    /**
     * Z Coordinate of the SPAWN_CHAR setting.
     */
    public static int SPAWN_Z;
    public static boolean ALLOW_HERO_SUBSKILL;
    public static int HERO_COUNT;
    public static int CRUMA_TOWER_LEVEL_RESTRICT;
    /**
     * Allow RaidBoss Petrified if player have +9 lvl to RB
     */
    public static boolean ALLOW_RAID_BOSS_PETRIFIED;
    /**
     * Allow Players Level Difference Protection ?
     */
    public static int ALT_PLAYER_PROTECTION_LEVEL;
    public static boolean ALLOW_LOW_LEVEL_TRADE;
    /**
     * Chat filter
     */
    public static boolean USE_CHAT_FILTER;
    public static int MONSTER_RETURN_DELAY;
    public static boolean SCROLL_STACKABLE;
    public static boolean ALLOW_VERSION_COMMAND;
    public static boolean ALLOW_CHAR_KILL_PROTECT;
    public static int CLAN_LEADER_COLOR;
    public static int CLAN_LEADER_COLOR_CLAN_LEVEL;
    public static boolean CLAN_LEADER_COLOR_ENABLED;
    public static int CLAN_LEADER_COLORED;
    public static boolean SAVE_RAIDBOSS_STATUS_INTO_DB;
    public static boolean DISABLE_WEIGHT_PENALTY;
    public static int DIFFERENT_Z_CHANGE_OBJECT;
    public static int DIFFERENT_Z_NEW_MOVIE;
    public static int HERO_CUSTOM_ITEM_ID;
    public static int NOOBLE_CUSTOM_ITEM_ID;
    public static int HERO_CUSTOM_DAY;
    public static boolean ALLOW_FARM1_COMMAND;
    public static boolean ALLOW_FARM2_COMMAND;
    public static boolean ALLOW_PVP1_COMMAND;
    public static boolean ALLOW_PVP2_COMMAND;
    public static int FARM1_X;
    public static int FARM1_Y;
    public static int FARM1_Z;
    public static int PVP1_X;
    public static int PVP1_Y;
    public static int PVP1_Z;
    public static int FARM2_X;
    public static int FARM2_Y;
    public static int FARM2_Z;
    public static int PVP2_X;
    public static int PVP2_Y;
    public static int PVP2_Z;
    public static String FARM1_CUSTOM_MESSAGE;
    public static String FARM2_CUSTOM_MESSAGE;
    public static String PVP1_CUSTOM_MESSAGE;
    public static String PVP2_CUSTOM_MESSAGE;
    //============================================================
    public static int KARMA_MIN_KARMA;
    public static int KARMA_MAX_KARMA;
    public static int KARMA_XP_DIVIDER;
    public static int KARMA_LOST_BASE;
    public static boolean KARMA_DROP_GM;
    public static boolean KARMA_AWARD_PK_KILL;
    public static int KARMA_PK_LIMIT;
    public static String KARMA_NONDROPPABLE_PET_ITEMS;
    public static String KARMA_NONDROPPABLE_ITEMS;
    public static FastList<Integer> KARMA_LIST_NONDROPPABLE_PET_ITEMS = new FastList<Integer>();
    public static FastList<Integer> KARMA_LIST_NONDROPPABLE_ITEMS = new FastList<Integer>();
    public static int PVP_NORMAL_TIME;
    public static int PVP_PVP_TIME;
    public static boolean PVP_COLOR_SYSTEM_ENABLED;
    public static int PVP_AMOUNT1;
    public static int PVP_AMOUNT2;
    public static int PVP_AMOUNT3;
    public static int PVP_AMOUNT4;
    public static int PVP_AMOUNT5;
    public static int NAME_COLOR_FOR_PVP_AMOUNT1;
    public static int NAME_COLOR_FOR_PVP_AMOUNT2;
    public static int NAME_COLOR_FOR_PVP_AMOUNT3;
    public static int NAME_COLOR_FOR_PVP_AMOUNT4;
    public static int NAME_COLOR_FOR_PVP_AMOUNT5;
    public static boolean PK_COLOR_SYSTEM_ENABLED;
    public static int PK_AMOUNT1;
    public static int PK_AMOUNT2;
    public static int PK_AMOUNT3;
    public static int PK_AMOUNT4;
    public static int PK_AMOUNT5;
    public static int TITLE_COLOR_FOR_PK_AMOUNT1;
    public static int TITLE_COLOR_FOR_PK_AMOUNT2;
    public static int TITLE_COLOR_FOR_PK_AMOUNT3;
    public static int TITLE_COLOR_FOR_PK_AMOUNT4;
    public static int TITLE_COLOR_FOR_PK_AMOUNT5;
    public static boolean PVP_REWARD_ENABLED;
    public static int PVP_REWARD_ID;
    public static int PVP_REWARD_AMOUNT;
    public static boolean PK_REWARD_ENABLED;
    public static int PK_REWARD_ID;
    public static int PK_REWARD_AMOUNT;
    public static int REWARD_PROTECT;
    public static boolean ENABLE_PK_INFO;
    public static boolean FLAGED_PLAYER_USE_BUFFER;
    public static boolean FLAGED_PLAYER_CAN_USE_GK;
    public static boolean PVPEXPSP_SYSTEM;
    /**
     * Add Exp At Pvp!
     */
    public static int ADD_EXP;
    /**
     * Add Sp At Pvp!
     */
    public static int ADD_SP;
    public static boolean ALLOW_POTS_IN_PVP;
    public static boolean ALLOW_SOE_IN_PVP;
    /**
     * Announce PvP, PK, Kill
     */
    public static boolean ANNOUNCE_PVP_KILL;
    public static boolean ANNOUNCE_PK_KILL;
    public static boolean ANNOUNCE_ALL_KILL;
    public static int DUEL_SPAWN_X;
    public static int DUEL_SPAWN_Y;
    public static int DUEL_SPAWN_Z;
    public static boolean PVP_PK_TITLE;
    public static String PVP_TITLE_PREFIX;
    public static String PK_TITLE_PREFIX;
    public static boolean WAR_LEGEND_AURA;
    public static int KILLS_TO_GET_WAR_LEGEND_AURA;
    public static boolean ANTI_FARM_ENABLED;
    public static boolean ANTI_FARM_CLAN_ALLY_ENABLED;
    public static boolean ANTI_FARM_LVL_DIFF_ENABLED;
    public static int ANTI_FARM_MAX_LVL_DIFF;
    public static boolean ANTI_FARM_PDEF_DIFF_ENABLED;
    public static int ANTI_FARM_MAX_PDEF_DIFF;
    public static boolean ANTI_FARM_PATK_DIFF_ENABLED;
    public static int ANTI_FARM_MAX_PATK_DIFF;
    public static boolean ANTI_FARM_PARTY_ENABLED;
    public static boolean ANTI_FARM_IP_ENABLED;
    public static boolean ANTI_FARM_SUMMON;
    public static int ALT_OLY_NUMBER_HEROS_EACH_CLASS;
    public static boolean ALT_OLY_LOG_FIGHTS;
    public static boolean ALT_OLY_SHOW_MONTHLY_WINNERS;
    public static boolean ALT_OLY_ANNOUNCE_GAMES;
    public static List<Integer> LIST_OLY_RESTRICTED_SKILLS = new FastList<Integer>();
    public static boolean ALT_OLY_AUGMENT_ALLOW;
    public static int ALT_OLY_TELEPORT_COUNTDOWN;
    public static int ALT_OLY_START_TIME;
    public static int ALT_OLY_MIN;
    public static long ALT_OLY_CPERIOD;
    public static long ALT_OLY_BATTLE;
    public static long ALT_OLY_WPERIOD;
    public static long ALT_OLY_VPERIOD;
    public static int ALT_OLY_CLASSED;
    public static int ALT_OLY_NONCLASSED;
    public static int ALT_OLY_BATTLE_REWARD_ITEM;
    public static int ALT_OLY_CLASSED_RITEM_C;
    public static int ALT_OLY_NONCLASSED_RITEM_C;
    public static int ALT_OLY_GP_PER_POINT;
    public static int ALT_OLY_MIN_POINT_FOR_EXCH;
    public static int ALT_OLY_HERO_POINTS;
    public static String ALT_OLY_RESTRICTED_ITEMS;
    public static List<Integer> LIST_OLY_RESTRICTED_ITEMS = new FastList<Integer>();
    public static boolean ALLOW_EVENTS_DURING_OLY;
    public static boolean ALT_OLY_RECHARGE_SKILLS;
    public static int ALT_OLY_COMP_RITEM;
    public static boolean REMOVE_CUBIC_OLYMPIAD;
    public static boolean ALT_OLY_USE_CUSTOM_PERIOD_SETTINGS;
    public static OlympiadPeriod ALT_OLY_PERIOD;
    public static int ALT_OLY_PERIOD_MULTIPLIER;
    //============================================================
    //============================================================
    // Enchant map
    public static FastMap<Integer, Integer> NORMAL_WEAPON_ENCHANT_LEVEL = new FastMap<Integer, Integer>();
    public static FastMap<Integer, Integer> BLESS_WEAPON_ENCHANT_LEVEL = new FastMap<Integer, Integer>();
    public static FastMap<Integer, Integer> CRYSTAL_WEAPON_ENCHANT_LEVEL = new FastMap<Integer, Integer>();
    public static FastMap<Integer, Integer> NORMAL_ARMOR_ENCHANT_LEVEL = new FastMap<Integer, Integer>();
    public static FastMap<Integer, Integer> BLESS_ARMOR_ENCHANT_LEVEL = new FastMap<Integer, Integer>();
    public static FastMap<Integer, Integer> CRYSTAL_ARMOR_ENCHANT_LEVEL = new FastMap<Integer, Integer>();
    public static FastMap<Integer, Integer> NORMAL_JEWELRY_ENCHANT_LEVEL = new FastMap<Integer, Integer>();
    public static FastMap<Integer, Integer> BLESS_JEWELRY_ENCHANT_LEVEL = new FastMap<Integer, Integer>();
    public static FastMap<Integer, Integer> CRYSTAL_JEWELRY_ENCHANT_LEVEL = new FastMap<Integer, Integer>();
    public static int ENCHANT_SAFE_MAX;
    public static int ENCHANT_SAFE_MAX_FULL;
    public static int ENCHANT_WEAPON_MAX;
    public static int ENCHANT_ARMOR_MAX;
    public static int ENCHANT_JEWELRY_MAX;
    public static int CRYSTAL_ENCHANT_MAX;
    public static int CRYSTAL_ENCHANT_MIN;
    // Dwarf bonus
    public static boolean ENABLE_DWARF_ENCHANT_BONUS;
    public static int DWARF_ENCHANT_MIN_LEVEL;
    public static int DWARF_ENCHANT_BONUS;
    // Augment chance
    public static int AUGMENTATION_NG_SKILL_CHANCE;
    public static int AUGMENTATION_MID_SKILL_CHANCE;
    public static int AUGMENTATION_HIGH_SKILL_CHANCE;
    public static int AUGMENTATION_TOP_SKILL_CHANCE;
    public static int AUGMENTATION_BASESTAT_CHANCE;
    // Augment glow
    public static int AUGMENTATION_NG_GLOW_CHANCE;
    public static int AUGMENTATION_MID_GLOW_CHANCE;
    public static int AUGMENTATION_HIGH_GLOW_CHANCE;
    public static int AUGMENTATION_TOP_GLOW_CHANCE;
    public static boolean DELETE_AUGM_PASSIVE_ON_CHANGE;
    public static boolean DELETE_AUGM_ACTIVE_ON_CHANGE;
    //============================================================
    // Enchant hero weapon
    public static boolean ENCHANT_HERO_WEAPON;
    // Soul crystal
    public static int SOUL_CRYSTAL_BREAK_CHANCE;
    public static int SOUL_CRYSTAL_LEVEL_CHANCE;
    public static int SOUL_CRYSTAL_MAX_LEVEL;
    // Count enchant
    public static int CUSTOM_ENCHANT_VALUE;
    /**
     * Olympiad max enchant limitation
     */
    public static int ALT_OLY_ENCHANT_LIMIT;
    public static int BREAK_ENCHANT;
    public static int GM_OVER_ENCHANT;
    public static int MAX_ITEM_ENCHANT_KICK;

    //============================================================
    public static boolean ENABLE_UNK_PACKET_PROTECTION;
    public static int MAX_UNKNOWN_PACKETS;
    public static int UNKNOWN_PACKETS_PUNiSHMENT;
    public static boolean DEBUG_UNKNOWN_PACKETS;
    public static boolean DEBUG_PACKETS;
    //============================================================
    public static boolean CHECK_SKILLS_ON_ENTER;
    public static boolean CHECK_NAME_ON_LOGIN;
    public static boolean L2WALKER_PROTEC;
    public static boolean PROTECTED_ENCHANT;
    public static boolean ONLY_GM_ITEMS_FREE;
    public static boolean ONLY_GM_TELEPORT_FREE;
    public static boolean BOT_PROTECTOR;
    public static int BOT_PROTECTOR_FIRST_CHECK;
    public static int BOT_PROTECTOR_NEXT_CHECK;
    public static int BOT_PROTECTOR_WAIT_ANSVER;
    public static boolean ALLOW_DUALBOX;
    public static int ALLOWED_BOXES;
    public static boolean ALLOW_DUALBOX_OLY;
    public static boolean ALLOW_DUALBOX_EVENT;
    //============================================================
    public static String USER;
    public static int KEY;
    //============================================================
    public static int BLOW_ATTACK_FRONT;
    public static int BLOW_ATTACK_SIDE;
    public static int BLOW_ATTACK_BEHIND;
    public static int BACKSTAB_ATTACK_FRONT;
    public static int BACKSTAB_ATTACK_SIDE;
    public static int BACKSTAB_ATTACK_BEHIND;
    public static int MAX_PATK_SPEED;
    public static int MAX_MATK_SPEED;
    public static int MAX_PCRIT_RATE;
    public static int MAX_MCRIT_RATE;
    public static float MCRIT_RATE_MUL;
    public static int RUN_SPD_BOOST;
    public static int MAX_RUN_SPEED;
    public static float ALT_MAGES_PHYSICAL_DAMAGE_MULTI;
    public static float ALT_MAGES_MAGICAL_DAMAGE_MULTI;
    public static float ALT_FIGHTERS_PHYSICAL_DAMAGE_MULTI;
    public static float ALT_FIGHTERS_MAGICAL_DAMAGE_MULTI;
    public static float ALT_PETS_PHYSICAL_DAMAGE_MULTI;
    public static float ALT_PETS_MAGICAL_DAMAGE_MULTI;
    public static float ALT_NPC_PHYSICAL_DAMAGE_MULTI;
    public static float ALT_NPC_MAGICAL_DAMAGE_MULTI;
    // Alternative damage for dagger skills VS heavy
    public static float ALT_DAGGER_DMG_VS_HEAVY;
    // Alternative damage for dagger skills VS robe
    public static float ALT_DAGGER_DMG_VS_ROBE;
    // Alternative damage for dagger skills VS light
    public static float ALT_DAGGER_DMG_VS_LIGHT;
    public static boolean ALLOW_RAID_LETHAL,
            ALLOW_LETHAL_PROTECTION_MOBS;
    public static String LETHAL_PROTECTED_MOBS;
    public static FastList<Integer> LIST_LETHAL_PROTECTED_MOBS = new FastList<Integer>();
    public static float MAGIC_CRITICAL_POWER;
    public static float STUN_CHANCE_MODIFIER;
    public static float BLEED_CHANCE_MODIFIER;
    public static float POISON_CHANCE_MODIFIER;
    public static float PARALYZE_CHANCE_MODIFIER;
    public static float ROOT_CHANCE_MODIFIER;
    public static float SLEEP_CHANCE_MODIFIER;
    public static float FEAR_CHANCE_MODIFIER;
    public static float CONFUSION_CHANCE_MODIFIER;
    public static float DEBUFF_CHANCE_MODIFIER;
    public static float BUFF_CHANCE_MODIFIER;
    public static boolean SEND_SKILLS_CHANCE_TO_PLAYERS;
    /* Remove equip during subclass change */
    public static boolean REMOVE_WEAPON_SUBCLASS;
    public static boolean REMOVE_CHEST_SUBCLASS;
    public static boolean REMOVE_LEG_SUBCLASS;
    public static boolean ENABLE_CLASS_DAMAGES;
    public static boolean ENABLE_CLASS_DAMAGES_IN_OLY;
    public static boolean ENABLE_CLASS_DAMAGES_LOGGER;
    public static boolean LEAVE_BUFFS_ON_DIE;
    public static boolean ALT_RAIDS_STATS_BONUS;
    //============================================================
    public static int GEODATA;
    public static boolean GEODATA_CELLFINDING;
    public static boolean ALLOW_PLAYERS_PATHNODE;
    public static boolean FORCE_GEODATA;
    public static CorrectSpawnsZ GEO_CORRECT_Z;
    public static boolean ACCEPT_GEOEDITOR_CONN;
    public static int GEOEDITOR_PORT;
    public static int WORLD_SIZE_MIN_X;
    public static int WORLD_SIZE_MAX_X;
    public static int WORLD_SIZE_MIN_Y;
    public static int WORLD_SIZE_MAX_Y;
    public static int WORLD_SIZE_MIN_Z;
    public static int WORLD_SIZE_MAX_Z;
    public static int COORD_SYNCHRONIZE;
    public static boolean FALL_DAMAGE;
    public static boolean ALLOW_WATER;
    //============================================================
    public static int RBLOCKRAGE;
    public static boolean PLAYERS_CAN_HEAL_RB;
    public static HashMap<Integer, Integer> RBS_SPECIFIC_LOCK_RAGE;
    public static boolean ALLOW_DIRECT_TP_TO_BOSS_ROOM;
    public static boolean ANTHARAS_OLD;
    public static int ANTHARAS_CLOSE;
    public static int ANTHARAS_DESPAWN_TIME;
    public static int ANTHARAS_RESP_FIRST;
    public static int ANTHARAS_RESP_SECOND;
    public static int ANTHARAS_WAIT_TIME;
    public static float ANTHARAS_POWER_MULTIPLIER;
    public static int BAIUM_SLEEP;
    public static int BAIUM_RESP_FIRST;
    public static int BAIUM_RESP_SECOND;
    public static float BAIUM_POWER_MULTIPLIER;
    public static int CORE_RESP_MINION;
    public static int CORE_RESP_FIRST;
    public static int CORE_RESP_SECOND;
    public static int CORE_LEVEL;
    public static int CORE_RING_CHANCE;
    public static float CORE_POWER_MULTIPLIER;
    public static int QA_RESP_NURSE;
    public static int QA_RESP_ROYAL;
    public static int QA_RESP_FIRST;
    public static int QA_RESP_SECOND;
    public static int QA_LEVEL;
    public static int QA_RING_CHANCE;
    public static float QA_POWER_MULTIPLIER;
    public static float LEVEL_DIFF_MULTIPLIER_MINION;
    public static int HPH_FIXINTERVALOFHALTER;
    public static int HPH_RANDOMINTERVALOFHALTER;
    public static int HPH_APPTIMEOFHALTER;
    public static int HPH_ACTIVITYTIMEOFHALTER;
    public static int HPH_FIGHTTIMEOFHALTER;
    public static int HPH_CALLROYALGUARDHELPERCOUNT;
    public static int HPH_CALLROYALGUARDHELPERINTERVAL;
    public static int HPH_INTERVALOFDOOROFALTER;
    public static int HPH_TIMEOFLOCKUPDOOROFALTAR;
    public static int ZAKEN_RESP_FIRST;
    public static int ZAKEN_RESP_SECOND;
    public static int ZAKEN_LEVEL;
    public static int ZAKEN_EARRING_CHANCE;
    public static float ZAKEN_POWER_MULTIPLIER;
    public static int ORFEN_RESP_FIRST;
    public static int ORFEN_RESP_SECOND;
    public static int ORFEN_LEVEL;
    public static int ORFEN_EARRING_CHANCE;
    public static float ORFEN_POWER_MULTIPLIER;
    public static int VALAKAS_RESP_FIRST;
    public static int VALAKAS_RESP_SECOND;
    public static int VALAKAS_WAIT_TIME;
    public static int VALAKAS_DESPAWN_TIME;
    public static float VALAKAS_POWER_MULTIPLIER;
    public static int FRINTEZZA_RESP_FIRST;
    public static int FRINTEZZA_RESP_SECOND;
    public static float FRINTEZZA_POWER_MULTIPLIER;
    public static boolean BYPASS_FRINTEZZA_PARTIES_CHECK;
    public static int FRINTEZZA_MIN_PARTIES;
    public static int FRINTEZZA_MAX_PARTIES;
    public static String RAID_INFO_IDS;
    public static FastList<Integer> RAID_INFO_IDS_LIST = new FastList<Integer>();
    //============================================================
    public static boolean SCRIPT_DEBUG;
    public static boolean SCRIPT_ALLOW_COMPILATION;
    public static boolean SCRIPT_CACHE;
    public static boolean SCRIPT_ERROR_LOG;
    public static boolean PYTHON_SCRIPT_CACHE;
    public static String PYTHON_SCRIPT_CACHE_DIR;
    //============================================================
    public static boolean POWERPAK_ENABLED;
    //============================================================
    public static Map<String, List<String>> EXTENDERS;
    //============================================================
    public static long AUTOSAVE_INITIAL_TIME;
    public static long AUTOSAVE_DELAY_TIME;
    public static long CHECK_CONNECTION_INACTIVITY_TIME;
    public static long CHECK_CONNECTION_INITIAL_TIME;
    public static long CHECK_CONNECTION_DELAY_TIME;
    public static long CHECK_TELEPORT_ZOMBIE_DELAY_TIME;
    public static long DEADLOCKCHECK_INTIAL_TIME;
    public static long DEADLOCKCHECK_DELAY_TIME;
    //============================================================
    public static ArrayList<String> QUESTION_LIST = new ArrayList<String>();
    public static int SERVER_ID;
    public static byte[] HEX_ID;
    //============================================================
    public static int PORT_LOGIN;
    public static String LOGIN_BIND_ADDRESS;
    public static int LOGIN_TRY_BEFORE_BAN;
    public static int LOGIN_BLOCK_AFTER_BAN;
    public static File DATAPACK_ROOT;
    public static int GAME_SERVER_LOGIN_PORT;
    public static String GAME_SERVER_LOGIN_HOST;
    public static String INTERNAL_HOSTNAME;
    public static String EXTERNAL_HOSTNAME;
    public static int IP_UPDATE_TIME;
    public static boolean STORE_SKILL_COOLTIME;
    public static boolean FORCE_GGAUTH;
    public static boolean FLOOD_PROTECTION;
    public static int FAST_CONNECTION_LIMIT;
    public static int NORMAL_CONNECTION_TIME;
    public static int FAST_CONNECTION_TIME;
    public static int MAX_CONNECTION_PER_IP;
    public static boolean ACCEPT_NEW_GAMESERVER;
    public static boolean AUTO_CREATE_ACCOUNTS;
    public static String NETWORK_IP_LIST;
    public static long SESSION_TTL;
    public static int MAX_LOGINSESSIONS;
    @Autowired
    private ConfigManager configManager;

    //============================================================
    public void loadAccessConfig() {

        //============================================================
        EVERYBODY_HAS_ADMIN_RIGHTS = configManager.getBoolean("EverybodyHasAdminRights");
        GM_STARTUP_AUTO_LIST = configManager.getBoolean("GMStartupAutoList");
        GM_ADMIN_MENU_STYLE = configManager.getString("GMAdminMenuStyle");
        GM_HERO_AURA = configManager.getBoolean("GMHeroAura");
        GM_STARTUP_INVULNERABLE = configManager.getBoolean("GMStartupInvulnerable");
        GM_ANNOUNCER_NAME = configManager.getBoolean("AnnounceGmName");
        SHOW_GM_LOGIN = configManager.getBoolean("ShowGMLogin");
        GM_STARTUP_INVISIBLE = configManager.getBoolean("GMStartupInvisible");
        GM_SPECIAL_EFFECT = configManager.getBoolean("GmLoginSpecialEffect");
        GM_STARTUP_SILENCE = configManager.getBoolean("GMStartupSilence");
        MASTERACCESS_LEVEL = configManager.getInteger("MasterAccessLevel");
        MASTERACCESS_NAME_COLOR = configManager.getInteger("MasterNameColor");
        MASTERACCESS_TITLE_COLOR = configManager.getInteger("MasterTitleColor");
        USERACCESS_LEVEL = configManager.getInteger("UserAccessLevel");
    }

    //============================================================
    public void loadOptionsConfig() {

        AUTODESTROY_ITEM_AFTER = configManager.getInteger("AutoDestroyDroppedItemAfter");
        HERB_AUTO_DESTROY_TIME = configManager.getInteger("AutoDestroyHerbTime") * 1000;
        PROTECTED_ITEMS = configManager.getString("ListOfProtectedItems");
        LIST_PROTECTED_ITEMS = new FastList<>();
        LIST_PROTECTED_ITEMS.addAll(configManager.getArray("ListOfProtectedItems", ",", ConfigManager.PropertyType.INTEGER));

        DESTROY_DROPPED_PLAYER_ITEM = configManager.getBoolean("DestroyPlayerDroppedItem");
        DESTROY_EQUIPABLE_PLAYER_ITEM = configManager.getBoolean("DestroyEquipableItem");
        SAVE_DROPPED_ITEM = configManager.getBoolean("SaveDroppedItem");
        EMPTY_DROPPED_ITEM_TABLE_AFTER_LOAD = configManager.getBoolean("EmptyDroppedItemTableAfterLoad");
        SAVE_DROPPED_ITEM_INTERVAL = configManager.getInteger("SaveDroppedItemInterval") * 60000;
        CLEAR_DROPPED_ITEM_TABLE = configManager.getBoolean("ClearDroppedItemTable");

        PRECISE_DROP_CALCULATION = configManager.getBoolean("PreciseDropCalculation");
        MULTIPLE_ITEM_DROP = configManager.getBoolean("MultipleItemDrop");

        ALLOW_WAREHOUSE = configManager.getBoolean("AllowWarehouse");
        WAREHOUSE_CACHE = configManager.getBoolean("WarehouseCache");
        WAREHOUSE_CACHE_TIME = configManager.getInteger("WarehouseCacheTime");
        ALLOW_FREIGHT = configManager.getBoolean("AllowFreight");
        ALLOW_WEAR = configManager.getBoolean("AllowWear");
        WEAR_DELAY = configManager.getInteger("WearDelay");
        WEAR_PRICE = configManager.getInteger("WearPrice");
        ALLOW_LOTTERY = configManager.getBoolean("AllowLottery");
        ALLOW_RACE = configManager.getBoolean("AllowRace");
        ALLOW_RENTPET = configManager.getBoolean("AllowRentPet");
        ALLOW_DISCARDITEM = configManager.getBoolean("AllowDiscardItem");
        ALLOWFISHING = configManager.getBoolean("AllowFishing");
        ALLOW_MANOR = configManager.getBoolean("AllowManor");
        ALLOW_BOAT = configManager.getBoolean("AllowBoat");
        ALLOW_NPC_WALKERS = configManager.getBoolean("AllowNpcWalkers");
        ALLOW_CURSED_WEAPONS = configManager.getBoolean("AllowCursedWeapons");

        ALLOW_USE_CURSOR_FOR_WALK = configManager.getBoolean("AllowUseCursorForWalk");
        DEFAULT_GLOBAL_CHAT = configManager.getString("GlobalChat");
        DEFAULT_TRADE_CHAT = configManager.getString("TradeChat");
        MAX_CHAT_LENGTH = configManager.getInteger("MaxChatLength");

        TRADE_CHAT_IS_NOOBLE = configManager.getBoolean("TradeChatIsNooble");
        TRADE_CHAT_WITH_PVP = configManager.getBoolean("TradeChatWithPvP");
        TRADE_PVP_AMOUNT = configManager.getInteger("TradePvPAmount");
        GLOBAL_CHAT_WITH_PVP = configManager.getBoolean("GlobalChatWithPvP");
        GLOBAL_PVP_AMOUNT = configManager.getInteger("GlobalPvPAmount");


        COMMUNITY_TYPE = configManager.getString("CommunityType").toLowerCase();
        BBS_DEFAULT = configManager.getString("BBSDefault");
        SHOW_LEVEL_COMMUNITYBOARD = configManager.getBoolean("ShowLevelOnCommunityBoard");
        SHOW_STATUS_COMMUNITYBOARD = configManager.getBoolean("ShowStatusOnCommunityBoard");
        NAME_PAGE_SIZE_COMMUNITYBOARD = configManager.getInteger("NamePageSizeOnCommunityBoard");
        NAME_PER_ROW_COMMUNITYBOARD = configManager.getInteger("NamePerRowOnCommunityBoard");

        ZONE_TOWN = configManager.getInteger("ZoneTown");

        MAX_DRIFT_RANGE = configManager.getInteger("MaxDriftRange");

        MIN_NPC_ANIMATION = configManager.getInteger("MinNPCAnimation");
        MAX_NPC_ANIMATION = configManager.getInteger("MaxNPCAnimation");
        MIN_MONSTER_ANIMATION = configManager.getInteger("MinMonsterAnimation");
        MAX_MONSTER_ANIMATION = configManager.getInteger("MaxMonsterAnimation");

        SHOW_NPC_LVL = configManager.getBoolean("ShowNpcLevel");

        FORCE_INVENTORY_UPDATE = configManager.getBoolean("ForceInventoryUpdate");

        FORCE_COMPLETE_STATUS_UPDATE = configManager.getBoolean("ForceCompletePlayerStatusUpdate");

        AUTODELETE_INVALID_QUEST_DATA = configManager.getBoolean("AutoDeleteInvalidQuestData");

        DELETE_DAYS = configManager.getInteger("DeleteCharAfterDays");

        DEFAULT_PUNISH = configManager.getInteger("DefaultPunish");
        DEFAULT_PUNISH_PARAM = configManager.getInteger("DefaultPunishParam");

        GRIDS_ALWAYS_ON = configManager.getBoolean("GridsAlwaysOn");
        GRID_NEIGHBOR_TURNON_TIME = configManager.getInteger("GridNeighborTurnOnTime");
        GRID_NEIGHBOR_TURNOFF_TIME = configManager.getInteger("GridNeighborTurnOffTime");

        PATH_NODE_RADIUS = configManager.getInteger("PathNodeRadius");
        NEW_NODE_ID = configManager.getInteger("NewNodeId");
        NEW_NODE_TYPE = configManager.getString("NewNodeType");

        CHECK_KNOWN = configManager.getBoolean("CheckKnownList");

        HIGH_RATE_SERVER_DROPS = configManager.getBoolean("HighRateServerDrops");
    }

    //============================================================
    public void loadServerConfig() {

        GAMESERVER_HOSTNAME = configManager.getString("GameserverHostname");
        PORT_GAME = configManager.getInteger("GameserverPort");

        EXTERNAL_HOSTNAME = configManager.getString("GameExternalHostname");
        INTERNAL_HOSTNAME = configManager.getString("GameInternalHostname");

        GAME_SERVER_LOGIN_PORT = configManager.getInteger("GameConnectionLoginPort");
        GAME_SERVER_LOGIN_HOST = configManager.getString("GameConnectionLoginHost");

        try {
            DATAPACK_ROOT = new File(configManager.getString("DatapackRoot")).getCanonicalFile();
        } catch (IOException e) {
            LOGGER.error("Datapack path not found", e);
        }

        Random ppc = new Random();
        int z = ppc.nextInt(6);
        if (z == 0) {
            z += 2;
        }
        for (int x = 0; x < 8; x++) {
            if (x == 4) {
                RWHO_ARRAY[x] = 44;
            } else {
                RWHO_ARRAY[x] = 51 + ppc.nextInt(z);
            }
        }
        RWHO_ARRAY[11] = 37265 + ppc.nextInt(z * 2 + 3);
        RWHO_ARRAY[8] = 51 + ppc.nextInt(z);
        z = 36224 + ppc.nextInt(z * 2);
        RWHO_ARRAY[9] = z;
        RWHO_ARRAY[10] = z;
        RWHO_ARRAY[12] = 1;
        RWHO_LOG = configManager.getBoolean("RemoteWhoLog");
        RWHO_SEND_TRASH = configManager.getBoolean("RemoteWhoSendTrash");
        RWHO_MAX_ONLINE = configManager.getInteger("RemoteWhoMaxOnline");
        RWHO_KEEP_STAT = configManager.getInteger("RemoteOnlineKeepStat");
        RWHO_ONLINE_INCREMENT = configManager.getInteger("RemoteOnlineIncrement");
        RWHO_PRIV_STORE_FACTOR = configManager.getFloat("RemotePrivStoreFactor");
        RWHO_FORCE_INC = configManager.getInteger("RemoteWhoForceInc");
    }

    //============================================================
    public void loadServerVersionConfig() {
        SERVER_REVISION = configManager.getString("gameServerBuildRevision");
        SERVER_BUILD_DATE = configManager.getString("gameServerBuildDate");
    }

    //============================================================
    public void loadIdFactoryConfig() {
        MAP_TYPE = ObjectMapType.valueOf(configManager.getString("L2Map"));
        SET_TYPE = ObjectSetType.valueOf(configManager.getString("L2Set"));
        IDFACTORY_TYPE = IdFactoryType.valueOf(configManager.getString("IDFactory"));
        BAD_ID_CHECKING = configManager.getBoolean("BadIdChecking");
    }

    //============================================================
    public void loadOtherConfig() {


        DEEPBLUE_DROP_RULES = configManager.getBoolean("UseDeepBlueDropRules");
        ALLOW_GUARDS = configManager.getBoolean("AllowGuards");
        EFFECT_CANCELING = configManager.getBoolean("CancelLesserEffect");
        WYVERN_SPEED = configManager.getInteger("WyvernSpeed");
        STRIDER_SPEED = configManager.getInteger("StriderSpeed");
        ALLOW_WYVERN_UPGRADER = configManager.getBoolean("AllowWyvernUpgrader");

			/* Select hit task */
        CLICK_TASK = configManager.getInteger("ClickTask");

        GM_CRITANNOUNCER_NAME = configManager.getBoolean("GMShowCritAnnouncerName");

			/* Inventory slots limits */
        INVENTORY_MAXIMUM_NO_DWARF = configManager.getInteger("MaximumSlotsForNoDwarf");
        INVENTORY_MAXIMUM_DWARF = configManager.getInteger("MaximumSlotsForDwarf");
        INVENTORY_MAXIMUM_GM = configManager.getInteger("MaximumSlotsForGMPlayer");
        MAX_ITEM_IN_PACKET = Math.max(INVENTORY_MAXIMUM_NO_DWARF, Math.max(INVENTORY_MAXIMUM_DWARF, INVENTORY_MAXIMUM_GM));

			/* Inventory slots limits */
        WAREHOUSE_SLOTS_NO_DWARF = configManager.getInteger("MaximumWarehouseSlotsForNoDwarf");
        WAREHOUSE_SLOTS_DWARF = configManager.getInteger("MaximumWarehouseSlotsForDwarf");
        WAREHOUSE_SLOTS_CLAN = configManager.getInteger("MaximumWarehouseSlotsForClan");
        FREIGHT_SLOTS = configManager.getInteger("MaximumFreightSlots");

			/* If different from 100 (ie 100%) heal rate is modified acordingly */
        HP_REGEN_MULTIPLIER = configManager.getDouble("HpRegenMultiplier") / 100;
        MP_REGEN_MULTIPLIER = configManager.getDouble("MpRegenMultiplier") / 100;
        CP_REGEN_MULTIPLIER = configManager.getDouble("CpRegenMultiplier") / 100;

        RAID_HP_REGEN_MULTIPLIER = configManager.getDouble("RaidHpRegenMultiplier") / 100;
        RAID_MP_REGEN_MULTIPLIER = configManager.getDouble("RaidMpRegenMultiplier") / 100;
        RAID_P_DEFENCE_MULTIPLIER = configManager.getDouble("RaidPhysicalDefenceMultiplier") / 100;
        RAID_M_DEFENCE_MULTIPLIER = configManager.getDouble("RaidMagicalDefenceMultiplier") / 100;
        RAID_MINION_RESPAWN_TIMER = configManager.getDouble("RaidMinionRespawnTime");
        RAID_MIN_RESPAWN_MULTIPLIER = configManager.getFloat("RaidMinRespawnMultiplier");
        RAID_MAX_RESPAWN_MULTIPLIER = configManager.getFloat("RaidMaxRespawnMultiplier");
        ENABLE_AIO_SYSTEM = configManager.getBoolean("EnableAioSystem");
        ALLOW_AIO_NCOLOR = configManager.getBoolean("AllowAioNameColor");
        AIO_NCOLOR = configManager.getInteger("AioNameColor");
        ALLOW_AIO_TCOLOR = configManager.getBoolean("AllowAioTitleColor");
        AIO_TCOLOR = configManager.getInteger("AioTitleColor");
        ALLOW_AIO_USE_GK = configManager.getBoolean("AllowAioUseGk");
        ALLOW_AIO_USE_CM = configManager.getBoolean("AllowAioUseClassMaster");
        ANNOUNCE_CASTLE_LORDS = configManager.getBoolean("AnnounceCastleLords");
        if (ENABLE_AIO_SYSTEM) //create map if system is enabled
        {
            AIO_SKILLS = configManager.getHashMap("AioSkills");
        }
        STARTING_ADENA = configManager.getInteger("StartingAdena");
        STARTING_AA = configManager.getInteger("StartingAncientAdena");

        CUSTOM_STARTER_ITEMS_ENABLED = configManager.getBoolean("CustomStarterItemsEnabled");
        if (GameServerConfig.CUSTOM_STARTER_ITEMS_ENABLED) {
            STARTING_CUSTOM_ITEMS_M.putAll(configManager.getHashMap("StartingCustomItemsMage"));
            STARTING_CUSTOM_ITEMS_F.putAll(configManager.getHashMap("StartingCustomItemsFighter"));
        }

        UNSTUCK_INTERVAL = configManager.getInteger("UnstuckInterval");

			/* Player protection after teleport or login */
        PLAYER_SPAWN_PROTECTION = configManager.getInteger("PlayerSpawnProtection");
        PLAYER_TELEPORT_PROTECTION = configManager.getInteger("PlayerTeleportProtection");
        EFFECT_TELEPORT_PROTECTION = configManager.getBoolean("EffectTeleportProtection");

			/* Player protection after recovering from fake death (works against mobs only) */
        PLAYER_FAKEDEATH_UP_PROTECTION = configManager.getInteger("PlayerFakeDeathUpProtection");

			/* Defines some Party XP related values */
        PARTY_XP_CUTOFF_METHOD = configManager.getString("PartyXpCutoffMethod");
        PARTY_XP_CUTOFF_PERCENT = configManager.getDouble("PartyXpCutoffPercent");
        PARTY_XP_CUTOFF_LEVEL = configManager.getInteger("PartyXpCutoffLevel");

			/* Amount of HP, MP, and CP is restored */
        RESPAWN_RESTORE_CP = configManager.getDouble("RespawnRestoreCP") / 100;
        RESPAWN_RESTORE_HP = configManager.getDouble("RespawnRestoreHP") / 100;
        RESPAWN_RESTORE_MP = configManager.getDouble("RespawnRestoreMP") / 100;

        RESPAWN_RANDOM_ENABLED = configManager.getBoolean("RespawnRandomInTown");
        RESPAWN_RANDOM_MAX_OFFSET = configManager.getInteger("RespawnRandomMaxOffset");

			/* Maximum number of available slots for pvt stores */
        MAX_PVTSTORE_SLOTS_DWARF = configManager.getInteger("MaxPvtStoreSlotsDwarf");
        MAX_PVTSTORE_SLOTS_OTHER = configManager.getInteger("MaxPvtStoreSlotsOther");

        STORE_SKILL_COOLTIME = configManager.getBoolean("StoreSkillCooltime");

        PET_RENT_NPC = configManager.getString("ListPetRentNpc");
        LIST_PET_RENT_NPC = new FastList<>();

        LIST_PET_RENT_NPC.addAll(configManager.getArray("ListPetRentNpc", ",", ConfigManager.PropertyType.INTEGER));
        LIST_NONDROPPABLE_ITEMS.addAll(configManager.getArray("ListOfNonDroppableItems", ",", ConfigManager.PropertyType.INTEGER));

        ANNOUNCE_MAMMON_SPAWN = configManager.getBoolean("AnnounceMammonSpawn");
        PETITIONING_ALLOWED = configManager.getBoolean("PetitioningAllowed");
        MAX_PETITIONS_PER_PLAYER = configManager.getInteger("MaxPetitionsPerPlayer");
        MAX_PETITIONS_PENDING = configManager.getInteger("MaxPetitionsPending");
        JAIL_IS_PVP = configManager.getBoolean("JailIsPvp");
        JAIL_DISABLE_CHAT = configManager.getBoolean("JailDisableChat");
        DEATH_PENALTY_CHANCE = configManager.getInteger("DeathPenaltyChance");
        //////////////
        ENABLE_MODIFY_SKILL_DURATION = configManager.getBoolean("EnableModifySkillDuration");
        if (ENABLE_MODIFY_SKILL_DURATION) {
            SKILL_DURATION_LIST = new FastMap<>();
            SKILL_DURATION_LIST.putAll(configManager.getHashMap("SkillDurationList"));

        }

        USE_SAY_FILTER = configManager.getBoolean("UseChatFilter");
        CHAT_FILTER_CHARS = configManager.getString("ChatFilterChars");
        CHAT_FILTER_PUNISHMENT = configManager.getString("ChatFilterPunishment");
        CHAT_FILTER_PUNISHMENT_PARAM1 = configManager.getInteger("ChatFilterPunishmentParam1");
        CHAT_FILTER_PUNISHMENT_PARAM2 = configManager.getInteger("ChatFilterPunishmentParam2");

        FS_TIME_ATTACK = configManager.getInteger("TimeOfAttack");
        FS_TIME_COOLDOWN = configManager.getInteger("TimeOfCoolDown");
        FS_TIME_ENTRY = configManager.getInteger("TimeOfEntry");
        FS_TIME_WARMUP = configManager.getInteger("TimeOfWarmUp");
        FS_PARTY_MEMBER_COUNT = configManager.getInteger("NumberOfNecessaryPartyMembers");

        if (FS_TIME_ATTACK <= 0) {
            FS_TIME_ATTACK = 50;
        }
        if (FS_TIME_COOLDOWN <= 0) {
            FS_TIME_COOLDOWN = 5;
        }
        if (FS_TIME_ENTRY <= 0) {
            FS_TIME_ENTRY = 3;
        }
        if (FS_TIME_WARMUP <= 0) {
            FS_TIME_WARMUP = 2;
        }
        if (FS_PARTY_MEMBER_COUNT <= 0) {
            FS_PARTY_MEMBER_COUNT = 4;
        }

        ALLOW_QUAKE_SYSTEM = configManager.getBoolean("AllowQuakeSystem");
        ENABLE_ANTI_PVP_FARM_MSG = configManager.getBoolean("EnableAntiPvpFarmMsg");
    }

    //============================================================
    public void loadRatesConfig() {

        RATE_XP = configManager.getFloat("RateXp");
        RATE_SP = configManager.getFloat("RateSp");
        RATE_PARTY_XP = configManager.getFloat("RatePartyXp");
        RATE_PARTY_SP = configManager.getFloat("RatePartySp");
        RATE_QUESTS_REWARD = configManager.getFloat("RateQuestsReward");
        RATE_DROP_ADENA = configManager.getFloat("RateDropAdena");
        RATE_CONSUMABLE_COST = configManager.getFloat("RateConsumableCost");
        RATE_DROP_ITEMS = configManager.getFloat("RateDropItems");
        RATE_DROP_SEAL_STONES = configManager.getFloat("RateDropSealStones");
        RATE_DROP_SPOIL = configManager.getFloat("RateDropSpoil");
        RATE_DROP_MANOR = configManager.getInteger("RateDropManor");
        RATE_DROP_QUEST = configManager.getFloat("RateDropQuest");
        RATE_KARMA_EXP_LOST = configManager.getFloat("RateKarmaExpLost");
        RATE_SIEGE_GUARDS_PRICE = configManager.getFloat("RateSiegeGuardsPrice");
        RATE_DROP_COMMON_HERBS = configManager.getFloat("RateCommonHerbs");
        RATE_DROP_MP_HP_HERBS = configManager.getFloat("RateHpMpHerbs");
        RATE_DROP_GREATER_HERBS = configManager.getFloat("RateGreaterHerbs");
        RATE_DROP_SUPERIOR_HERBS = configManager.getFloat("RateSuperiorHerbs") * 10;
        RATE_DROP_SPECIAL_HERBS = configManager.getFloat("RateSpecialHerbs") * 10;

        PLAYER_DROP_LIMIT = configManager.getInteger("PlayerDropLimit");
        PLAYER_RATE_DROP = configManager.getInteger("PlayerRateDrop");
        PLAYER_RATE_DROP_ITEM = configManager.getInteger("PlayerRateDropItem");
        PLAYER_RATE_DROP_EQUIP = configManager.getInteger("PlayerRateDropEquip");
        PLAYER_RATE_DROP_EQUIP_WEAPON = configManager.getInteger("PlayerRateDropEquipWeapon");

        PET_XP_RATE = configManager.getFloat("PetXpRate");
        PET_FOOD_RATE = configManager.getInteger("PetFoodRate");
        SINEATER_XP_RATE = configManager.getFloat("SinEaterXpRate");

        KARMA_DROP_LIMIT = configManager.getInteger("KarmaDropLimit");
        KARMA_RATE_DROP = configManager.getInteger("KarmaRateDrop");
        KARMA_RATE_DROP_ITEM = configManager.getInteger("KarmaRateDropItem");
        KARMA_RATE_DROP_EQUIP = configManager.getInteger("KarmaRateDropEquip");
        KARMA_RATE_DROP_EQUIP_WEAPON = configManager.getInteger("KarmaRateDropEquipWeapon");

        /** RB rate **/
        ADENA_BOSS = configManager.getFloat("AdenaBoss");
        ADENA_RAID = configManager.getFloat("AdenaRaid");
        ADENA_MINON = configManager.getFloat("AdenaMinion");
        ITEMS_BOSS = configManager.getFloat("ItemsBoss");
        ITEMS_RAID = configManager.getFloat("ItemsRaid");
        ITEMS_MINON = configManager.getFloat("ItemsMinion");
        SPOIL_BOSS = configManager.getFloat("SpoilBoss");
        SPOIL_RAID = configManager.getFloat("SpoilRaid");
        SPOIL_MINON = configManager.getFloat("SpoilMinion");
    }

    //============================================================
    public void loadAltConfig() {

        ALT_GAME_TIREDNESS = configManager.getBoolean("AltGameTiredness");
        ALT_WEIGHT_LIMIT = configManager.getDouble("AltWeightLimit");
        ALT_GAME_SKILL_LEARN = configManager.getBoolean("AltGameSkillLearn");
        AUTO_LEARN_SKILLS = configManager.getBoolean("AutoLearnSkills");
        ALT_GAME_CANCEL_BOW = configManager.getString("AltGameCancelByHit").equalsIgnoreCase("bow") || configManager.getString("AltGameCancelByHit").equalsIgnoreCase("all");
        ALT_GAME_CANCEL_CAST = configManager.getString("AltGameCancelByHit").equalsIgnoreCase("cast") || configManager.getString("AltGameCancelByHit").equalsIgnoreCase("all");
        ALT_GAME_SHIELD_BLOCKS = configManager.getBoolean("AltShieldBlocks");
        ALT_PERFECT_SHLD_BLOCK = configManager.getInteger("AltPerfectShieldBlockRate");
        ALT_GAME_DELEVEL = configManager.getBoolean("Delevel");
        ALT_GAME_MAGICFAILURES = configManager.getBoolean("MagicFailures");
        ALT_GAME_MOB_ATTACK_AI = configManager.getBoolean("AltGameMobAttackAI");
        ALT_MOB_AGRO_IN_PEACEZONE = configManager.getBoolean("AltMobAgroInPeaceZone");
        ALT_GAME_EXPONENT_XP = configManager.getFloat("AltGameExponentXp");
        ALT_GAME_EXPONENT_SP = configManager.getFloat("AltGameExponentSp");
        AUTO_LEARN_DIVINE_INSPIRATION = configManager.getBoolean("AutoLearnDivineInspiration");
        DIVINE_SP_BOOK_NEEDED = configManager.getBoolean("DivineInspirationSpBookNeeded");
        ALLOW_CLASS_MASTERS = configManager.getBoolean("AllowClassMasters");
        CLASS_MASTER_STRIDER_UPDATE = configManager.getBoolean("AllowClassMastersStriderUpdate");
        CLASS_MASTER_SETTINGS = new ClassMasterSettings(configManager.getString("ConfigClassMaster"));
        ALLOW_REMOTE_CLASS_MASTERS = configManager.getBoolean("AllowRemoteClassMasters");
        MAX_LEVEL_DONT_LOSE_EXP = configManager.getBoolean("MaxLevelDontLoseExp");

        ALLOW_CLASS_MASTERS_FIRST_CLASS = configManager.getBoolean("AllowClassMastersFirstClass");
        ALLOW_CLASS_MASTERS_SECOND_CLASS = configManager.getBoolean("AllowClassMastersSecondClass");
        ALLOW_CLASS_MASTERS_THIRD_CLASS = configManager.getBoolean("AllowClassMastersThirdClass");

        ALT_GAME_FREIGHTS = configManager.getBoolean("AltGameFreights");
        ALT_GAME_FREIGHT_PRICE = configManager.getInteger("AltGameFreightPrice");
        ALT_PARTY_RANGE = configManager.getInteger("AltPartyRange");
        ALT_PARTY_RANGE2 = configManager.getInteger("AltPartyRange2");
        REMOVE_CASTLE_CIRCLETS = configManager.getBoolean("RemoveCastleCirclets");
        LIFE_CRYSTAL_NEEDED = configManager.getBoolean("LifeCrystalNeeded");
        SP_BOOK_NEEDED = configManager.getBoolean("SpBookNeeded");
        ES_SP_BOOK_NEEDED = configManager.getBoolean("EnchantSkillSpBookNeeded");
        AUTO_LOOT = configManager.getBoolean("AutoLoot");
        AUTO_LOOT_BOSS = configManager.getBoolean("AutoLootBoss");
        AUTO_LOOT_HERBS = configManager.getBoolean("AutoLootHerbs");
        ALT_GAME_FREE_TELEPORT = configManager.getBoolean("AltFreeTeleporting");
        ALT_RECOMMEND = configManager.getBoolean("AltRecommend");
        ALT_GAME_SUBCLASS_WITHOUT_QUESTS = configManager.getBoolean("AltSubClassWithoutQuests");
        ALT_RESTORE_EFFECTS_ON_SUBCLASS_CHANGE = configManager.getBoolean("AltRestoreEffectOnSub");
        ALT_GAME_VIEWNPC = configManager.getBoolean("AltGameViewNpc");
        ALT_GAME_NEW_CHAR_ALWAYS_IS_NEWBIE = configManager.getBoolean("AltNewCharAlwaysIsNewbie");
        ALT_MEMBERS_CAN_WITHDRAW_FROM_CLANWH = configManager.getBoolean("AltMembersCanWithdrawFromClanWH");
        ALT_MAX_NUM_OF_CLANS_IN_ALLY = configManager.getInteger("AltMaxNumOfClansInAlly");

        ALT_CLAN_MEMBERS_FOR_WAR = configManager.getInteger("AltClanMembersForWar");
        ALT_CLAN_JOIN_DAYS = configManager.getInteger("DaysBeforeJoinAClan");
        ALT_CLAN_CREATE_DAYS = configManager.getInteger("DaysBeforeCreateAClan");
        ALT_CLAN_DISSOLVE_DAYS = configManager.getInteger("DaysToPassToDissolveAClan");
        ALT_ALLY_JOIN_DAYS_WHEN_LEAVED = configManager.getInteger("DaysBeforeJoinAllyWhenLeaved");
        ALT_ALLY_JOIN_DAYS_WHEN_DISMISSED = configManager.getInteger("DaysBeforeJoinAllyWhenDismissed");
        ALT_ACCEPT_CLAN_DAYS_WHEN_DISMISSED = configManager.getInteger("DaysBeforeAcceptNewClanWhenDismissed");
        ALT_CREATE_ALLY_DAYS_WHEN_DISSOLVED = configManager.getInteger("DaysBeforeCreateNewAllyWhenDissolved");

        ALT_MANOR_REFRESH_TIME = configManager.getInteger("AltManorRefreshTime");
        ALT_MANOR_REFRESH_MIN = configManager.getInteger("AltManorRefreshMin");
        ALT_MANOR_APPROVE_TIME = configManager.getInteger("AltManorApproveTime");
        ALT_MANOR_APPROVE_MIN = configManager.getInteger("AltManorApproveMin");
        ALT_MANOR_MAINTENANCE_PERIOD = configManager.getInteger("AltManorMaintenancePeriod");
        ALT_MANOR_SAVE_ALL_ACTIONS = configManager.getBoolean("AltManorSaveAllActions");
        ALT_MANOR_SAVE_PERIOD_RATE = configManager.getInteger("AltManorSavePeriodRate");

        ALT_LOTTERY_PRIZE = configManager.getInteger("AltLotteryPrize");
        ALT_LOTTERY_TICKET_PRICE = configManager.getInteger("AltLotteryTicketPrice");
        ALT_LOTTERY_5_NUMBER_RATE = configManager.getFloat("AltLottery5NumberRate");
        ALT_LOTTERY_4_NUMBER_RATE = configManager.getFloat("AltLottery4NumberRate");
        ALT_LOTTERY_3_NUMBER_RATE = configManager.getFloat("AltLottery3NumberRate");
        ALT_LOTTERY_2_AND_1_NUMBER_PRIZE = configManager.getInteger("AltLottery2and1NumberPrize");
        BUFFS_MAX_AMOUNT = configManager.getByte("MaxBuffAmount");
        DEBUFFS_MAX_AMOUNT = configManager.getByte("MaxDebuffAmount");

        // Dimensional Rift Config
        RIFT_MIN_PARTY_SIZE = configManager.getInteger("RiftMinPartySize");
        RIFT_MAX_JUMPS = configManager.getInteger("MaxRiftJumps");
        RIFT_SPAWN_DELAY = configManager.getInteger("RiftSpawnDelay");
        RIFT_AUTO_JUMPS_TIME_MIN = configManager.getInteger("AutoJumpsDelayMin");
        RIFT_AUTO_JUMPS_TIME_MAX = configManager.getInteger("AutoJumpsDelayMax");
        RIFT_ENTER_COST_RECRUIT = configManager.getInteger("RecruitCost");
        RIFT_ENTER_COST_SOLDIER = configManager.getInteger("SoldierCost");
        RIFT_ENTER_COST_OFFICER = configManager.getInteger("OfficerCost");
        RIFT_ENTER_COST_CAPTAIN = configManager.getInteger("CaptainCost");
        RIFT_ENTER_COST_COMMANDER = configManager.getInteger("CommanderCost");
        RIFT_ENTER_COST_HERO = configManager.getInteger("HeroCost");
        RIFT_BOSS_ROOM_TIME_MUTIPLY = configManager.getFloat("BossRoomTimeMultiply");

        // Destroy ss
        DONT_DESTROY_SS = configManager.getBoolean("DontDestroySS");

        // Max level newbie
        MAX_LEVEL_NEWBIE = configManager.getInteger("MaxLevelNewbie");
        // Level when Char lost Newbie status
        MAX_LEVEL_NEWBIE_STATUS = configManager.getInteger("MaxLevelNewbieStatus");


        STANDARD_RESPAWN_DELAY = configManager.getInteger("StandardRespawnDelay");
        ALT_RECOMMENDATIONS_NUMBER = configManager.getInteger("AltMaxRecommendationNumber");

        RAID_RANKING_1ST = configManager.getInteger("1stRaidRankingPoints");
        RAID_RANKING_2ND = configManager.getInteger("2ndRaidRankingPoints");
        RAID_RANKING_3RD = configManager.getInteger("3rdRaidRankingPoints");
        RAID_RANKING_4TH = configManager.getInteger("4thRaidRankingPoints");
        RAID_RANKING_5TH = configManager.getInteger("5thRaidRankingPoints");
        RAID_RANKING_6TH = configManager.getInteger("6thRaidRankingPoints");
        RAID_RANKING_7TH = configManager.getInteger("7thRaidRankingPoints");
        RAID_RANKING_8TH = configManager.getInteger("8thRaidRankingPoints");
        RAID_RANKING_9TH = configManager.getInteger("9thRaidRankingPoints");
        RAID_RANKING_10TH = configManager.getInteger("10thRaidRankingPoints");
        RAID_RANKING_UP_TO_50TH = configManager.getInteger("UpTo50thRaidRankingPoints");
        RAID_RANKING_UP_TO_100TH = configManager.getInteger("UpTo100thRaidRankingPoints");

        EXPERTISE_PENALTY = configManager.getBoolean("ExpertisePenality");
        MASTERY_PENALTY = configManager.getBoolean("MasteryPenality");
        LEVEL_TO_GET_PENALITY = configManager.getInteger("LevelToGetPenalty");

        MASTERY_WEAPON_PENALTY = configManager.getBoolean("MasteryWeaponPenality");
        LEVEL_TO_GET_WEAPON_PENALITY = configManager.getInteger("LevelToGetWeaponPenalty");

        /** augmentation start reuse time **/
        ACTIVE_AUGMENTS_START_REUSE_TIME = configManager.getInteger("AugmStartReuseTime");

        INVUL_NPC_LIST = new FastList<>();
        String t = configManager.getString("InvulNpcList");
        String as[];
        int k = (as = t.split(",")).length;
        for (int j = 0; j < k; j++) {
            String t2 = as[j];
            if (t2.contains("-")) {
                int a1 = Integer.parseInt(t2.split("-")[0]);
                int a2 = Integer.parseInt(t2.split("-")[1]);
                for (int i = a1; i <= a2; i++)
                    INVUL_NPC_LIST.add(Integer.valueOf(i));
            } else
                INVUL_NPC_LIST.add(Integer.valueOf(Integer.parseInt(t2)));
        }
        DISABLE_ATTACK_NPC_TYPE = configManager.getBoolean("DisableAttackToNpcs");
        LIST_ALLOWED_NPC_TYPES = new FastList<>();
        LIST_ALLOWED_NPC_TYPES.addAll(configManager.getArray("AllowedNPCTypes", ",", ConfigManager.PropertyType.STRING));
        NPC_ATTACKABLE = configManager.getBoolean("NpcAttackable");

        SELL_BY_ITEM = configManager.getBoolean("SellByItem");
        SELL_ITEM = configManager.getInteger("SellItem");

        ALLOWED_SUBCLASS = configManager.getInteger("AllowedSubclass");
        BASE_SUBCLASS_LEVEL = configManager.getByte("BaseSubclassLevel");
        MAX_SUBCLASS_LEVEL = configManager.getByte("MaxSubclassLevel");

        ALT_MOBS_STATS_BONUS = configManager.getBoolean("AltMobsStatsBonus");
        ALT_PETS_STATS_BONUS = configManager.getBoolean("AltPetsStatsBonus");
    }

    //============================================================
    public void load7sConfig() {

        ALT_GAME_REQUIRE_CASTLE_DAWN = configManager.getBoolean("AltRequireCastleForDawn");
        ALT_GAME_REQUIRE_CLAN_CASTLE = configManager.getBoolean("AltRequireClanCastle");
        ALT_REQUIRE_WIN_7S = configManager.getBoolean("AltRequireWin7s");
        ALT_FESTIVAL_MIN_PLAYER = configManager.getInteger("AltFestivalMinPlayer");
        ALT_MAXIMUM_PLAYER_CONTRIB = configManager.getInteger("AltMaxPlayerContrib");
        ALT_FESTIVAL_MANAGER_START = configManager.getLong("AltFestivalManagerStart");
        ALT_FESTIVAL_LENGTH = configManager.getLong("AltFestivalLength");
        ALT_FESTIVAL_CYCLE_LENGTH = configManager.getLong("AltFestivalCycleLength");
        ALT_FESTIVAL_FIRST_SPAWN = configManager.getLong("AltFestivalFirstSpawn");
        ALT_FESTIVAL_FIRST_SWARM = configManager.getLong("AltFestivalFirstSwarm");
        ALT_FESTIVAL_SECOND_SPAWN = configManager.getLong("AltFestivalSecondSpawn");
        ALT_FESTIVAL_SECOND_SWARM = configManager.getLong("AltFestivalSecondSwarm");
        ALT_FESTIVAL_CHEST_SPAWN = configManager.getLong("AltFestivalChestSpawn");

    }

    //============================================================
    public void loadCHConfig() {

        CH_TELE_FEE_RATIO = configManager.getLong("ClanHallTeleportFunctionFeeRation");
        CH_TELE1_FEE = configManager.getInteger("ClanHallTeleportFunctionFeeLvl1");
        CH_TELE2_FEE = configManager.getInteger("ClanHallTeleportFunctionFeeLvl2");
        CH_SUPPORT_FEE_RATIO = configManager.getLong("ClanHallSupportFunctionFeeRation");
        CH_SUPPORT1_FEE = configManager.getInteger("ClanHallSupportFeeLvl1");
        CH_SUPPORT2_FEE = configManager.getInteger("ClanHallSupportFeeLvl2");
        CH_SUPPORT3_FEE = configManager.getInteger("ClanHallSupportFeeLvl3");
        CH_SUPPORT4_FEE = configManager.getInteger("ClanHallSupportFeeLvl4");
        CH_SUPPORT5_FEE = configManager.getInteger("ClanHallSupportFeeLvl5");
        CH_SUPPORT6_FEE = configManager.getInteger("ClanHallSupportFeeLvl6");
        CH_SUPPORT7_FEE = configManager.getInteger("ClanHallSupportFeeLvl7");
        CH_SUPPORT8_FEE = configManager.getInteger("ClanHallSupportFeeLvl8");
        CH_MPREG_FEE_RATIO = configManager.getLong("ClanHallMpRegenerationFunctionFeeRation");
        CH_MPREG1_FEE = configManager.getInteger("ClanHallMpRegenerationFeeLvl1");
        CH_MPREG2_FEE = configManager.getInteger("ClanHallMpRegenerationFeeLvl2");
        CH_MPREG3_FEE = configManager.getInteger("ClanHallMpRegenerationFeeLvl3");
        CH_MPREG4_FEE = configManager.getInteger("ClanHallMpRegenerationFeeLvl4");
        CH_MPREG5_FEE = configManager.getInteger("ClanHallMpRegenerationFeeLvl5");
        CH_HPREG_FEE_RATIO = configManager.getLong("ClanHallHpRegenerationFunctionFeeRation");
        CH_HPREG1_FEE = configManager.getInteger("ClanHallHpRegenerationFeeLvl1");
        CH_HPREG2_FEE = configManager.getInteger("ClanHallHpRegenerationFeeLvl2");
        CH_HPREG3_FEE = configManager.getInteger("ClanHallHpRegenerationFeeLvl3");
        CH_HPREG4_FEE = configManager.getInteger("ClanHallHpRegenerationFeeLvl4");
        CH_HPREG5_FEE = configManager.getInteger("ClanHallHpRegenerationFeeLvl5");
        CH_HPREG6_FEE = configManager.getInteger("ClanHallHpRegenerationFeeLvl6");
        CH_HPREG7_FEE = configManager.getInteger("ClanHallHpRegenerationFeeLvl7");
        CH_HPREG8_FEE = configManager.getInteger("ClanHallHpRegenerationFeeLvl8");
        CH_HPREG9_FEE = configManager.getInteger("ClanHallHpRegenerationFeeLvl9");
        CH_HPREG10_FEE = configManager.getInteger("ClanHallHpRegenerationFeeLvl10");
        CH_HPREG11_FEE = configManager.getInteger("ClanHallHpRegenerationFeeLvl11");
        CH_HPREG12_FEE = configManager.getInteger("ClanHallHpRegenerationFeeLvl12");
        CH_HPREG13_FEE = configManager.getInteger("ClanHallHpRegenerationFeeLvl13");
        CH_EXPREG_FEE_RATIO = configManager.getLong("ClanHallExpRegenerationFunctionFeeRation");
        CH_EXPREG1_FEE = configManager.getInteger("ClanHallExpRegenerationFeeLvl1");
        CH_EXPREG2_FEE = configManager.getInteger("ClanHallExpRegenerationFeeLvl2");
        CH_EXPREG3_FEE = configManager.getInteger("ClanHallExpRegenerationFeeLvl3");
        CH_EXPREG4_FEE = configManager.getInteger("ClanHallExpRegenerationFeeLvl4");
        CH_EXPREG5_FEE = configManager.getInteger("ClanHallExpRegenerationFeeLvl5");
        CH_EXPREG6_FEE = configManager.getInteger("ClanHallExpRegenerationFeeLvl6");
        CH_EXPREG7_FEE = configManager.getInteger("ClanHallExpRegenerationFeeLvl7");
        CH_ITEM_FEE_RATIO = configManager.getLong("ClanHallItemCreationFunctionFeeRation");
        CH_ITEM1_FEE = configManager.getInteger("ClanHallItemCreationFunctionFeeLvl1");
        CH_ITEM2_FEE = configManager.getInteger("ClanHallItemCreationFunctionFeeLvl2");
        CH_ITEM3_FEE = configManager.getInteger("ClanHallItemCreationFunctionFeeLvl3");
        CH_CURTAIN_FEE_RATIO = configManager.getLong("ClanHallCurtainFunctionFeeRation");
        CH_CURTAIN1_FEE = configManager.getInteger("ClanHallCurtainFunctionFeeLvl1");
        CH_CURTAIN2_FEE = configManager.getInteger("ClanHallCurtainFunctionFeeLvl2");
        CH_FRONT_FEE_RATIO = configManager.getLong("ClanHallFrontPlatformFunctionFeeRation");
        CH_FRONT1_FEE = configManager.getInteger("ClanHallFrontPlatformFunctionFeeLvl1");
        CH_FRONT2_FEE = configManager.getInteger("ClanHallFrontPlatformFunctionFeeLvl2");
    }

    //============================================================
    public void loadElitCHConfig() {
        DEVASTATED_DAY = configManager.getInteger("DevastatedDay");
        DEVASTATED_HOUR = configManager.getInteger("DevastatedHour");
        DEVASTATED_MINUTES = configManager.getInteger("DevastatedMinutes");
        PARTISAN_DAY = configManager.getInteger("PartisanDay");
        PARTISAN_HOUR = configManager.getInteger("PartisanHour");
        PARTISAN_MINUTES = configManager.getInteger("PartisanMinutes");
    }

    //============================================================
    public void loadChampionConfig() {

        L2JMOD_CHAMPION_ENABLE = configManager.getBoolean("ChampionEnable");
        L2JMOD_CHAMPION_FREQUENCY = configManager.getInteger("ChampionFrequency");
        L2JMOD_CHAMP_MIN_LVL = configManager.getInteger("ChampionMinLevel");
        L2JMOD_CHAMP_MAX_LVL = configManager.getInteger("ChampionMaxLevel");
        L2JMOD_CHAMPION_HP = configManager.getInteger("ChampionHp");
        L2JMOD_CHAMPION_HP_REGEN = configManager.getFloat("ChampionHpRegen");
        L2JMOD_CHAMPION_REWARDS = configManager.getInteger("ChampionRewards");
        L2JMOD_CHAMPION_ADENAS_REWARDS = configManager.getInteger("ChampionAdenasRewards");
        L2JMOD_CHAMPION_ATK = configManager.getFloat("ChampionAtk");
        L2JMOD_CHAMPION_SPD_ATK = configManager.getFloat("ChampionSpdAtk");
        L2JMOD_CHAMPION_REWARD = configManager.getInteger("ChampionRewardItem");
        L2JMOD_CHAMPION_REWARD_ID = configManager.getInteger("ChampionRewardItemID");
        L2JMOD_CHAMPION_REWARD_QTY = configManager.getInteger("ChampionRewardItemQty");
        L2JMOD_CHAMP_TITLE = configManager.getString("ChampionTitle");
    }

    //============================================================
    public void loadWeddingConfig() {
        L2JMOD_ALLOW_WEDDING = configManager.getBoolean("AllowWedding");
        L2JMOD_WEDDING_PRICE = configManager.getInteger("WeddingPrice");
        L2JMOD_WEDDING_PUNISH_INFIDELITY = configManager.getBoolean("WeddingPunishInfidelity");
        L2JMOD_WEDDING_TELEPORT = configManager.getBoolean("WeddingTeleport");
        L2JMOD_WEDDING_TELEPORT_PRICE = configManager.getInteger("WeddingTeleportPrice");
        L2JMOD_WEDDING_TELEPORT_DURATION = configManager.getInteger("WeddingTeleportDuration");
        L2JMOD_WEDDING_NAME_COLOR_NORMAL = configManager.getInteger("WeddingNameCollorN");
        L2JMOD_WEDDING_NAME_COLOR_GEY = configManager.getInteger("WeddingNameCollorB");
        L2JMOD_WEDDING_NAME_COLOR_LESBO = configManager.getInteger("WeddingNameCollorL");
        L2JMOD_WEDDING_SAMESEX = configManager.getBoolean("WeddingAllowSameSex");
        L2JMOD_WEDDING_FORMALWEAR = configManager.getBoolean("WeddingFormalWear");
        L2JMOD_WEDDING_DIVORCE_COSTS = configManager.getInteger("WeddingDivorceCosts");
        WEDDING_GIVE_CUPID_BOW = configManager.getBoolean("WeddingGiveBow");
        ANNOUNCE_WEDDING = configManager.getBoolean("AnnounceWedding");
    }

    //============================================================
    public void loadTVTConfig() {

        TVT_EVEN_TEAMS = configManager.getString("TvTEvenTeams");
        TVT_ALLOW_INTERFERENCE = configManager.getBoolean("TvTAllowInterference");
        TVT_ALLOW_POTIONS = configManager.getBoolean("TvTAllowPotions");
        TVT_ALLOW_SUMMON = configManager.getBoolean("TvTAllowSummon");
        TVT_ON_START_REMOVE_ALL_EFFECTS = configManager.getBoolean("TvTOnStartRemoveAllEffects");
        TVT_ON_START_UNSUMMON_PET = configManager.getBoolean("TvTOnStartUnsummonPet");
        TVT_REVIVE_RECOVERY = configManager.getBoolean("TvTReviveRecovery");
        TVT_ANNOUNCE_TEAM_STATS = configManager.getBoolean("TvTAnnounceTeamStats");
        TVT_ANNOUNCE_REWARD = configManager.getBoolean("TvTAnnounceReward");
        TVT_PRICE_NO_KILLS = configManager.getBoolean("TvTPriceNoKills");
        TVT_JOIN_CURSED = configManager.getBoolean("TvTJoinWithCursedWeapon");
        TVT_COMMAND = configManager.getBoolean("TvTCommand");
        TVT_REVIVE_DELAY = configManager.getLong("TvTReviveDelay");
        if (TVT_REVIVE_DELAY < 1000)
            TVT_REVIVE_DELAY = 1000; //can't be set less then 1 second
        TVT_OPEN_FORT_DOORS = configManager.getBoolean("TvTOpenFortDoors");
        TVT_CLOSE_FORT_DOORS = configManager.getBoolean("TvTCloseFortDoors");
        TVT_OPEN_ADEN_COLOSSEUM_DOORS = configManager.getBoolean("TvTOpenAdenColosseumDoors");
        TVT_CLOSE_ADEN_COLOSSEUM_DOORS = configManager.getBoolean("TvTCloseAdenColosseumDoors");
        TVT_TOP_KILLER_REWARD = configManager.getInteger("TvTTopKillerRewardId");
        TVT_TOP_KILLER_QTY = configManager.getInteger("TvTTopKillerRewardQty");
        TVT_AURA = configManager.getBoolean("TvTAura");
        TVT_STATS_LOGGER = configManager.getBoolean("TvTStatsLogger");
    }

    //============================================================
    public void loadTWConfig() {
        TW_TOWN_ID = configManager.getInteger("TWTownId");
        TW_ALL_TOWNS = configManager.getBoolean("TWAllTowns");
        TW_ITEM_ID = configManager.getInteger("TownWarItemId");
        TW_ITEM_AMOUNT = configManager.getInteger("TownWarItemAmount");
        TW_ALLOW_KARMA = configManager.getBoolean("AllowKarma");
        TW_DISABLE_GK = configManager.getBoolean("DisableGK");
        TW_RESS_ON_DIE = configManager.getBoolean("SendRessOnDeath");
    }

    public void loadIRCConfig() {

        IRC_ENABLED = configManager.getBoolean("Enable");
        IRC_LOG_CHAT = configManager.getBoolean("IRCLogChat");
        IRC_SSL = configManager.getBoolean("SSL");
        IRC_SERVER = configManager.getString("Server");
        IRC_PORT = configManager.getInteger("Port");
        IRC_PASS = configManager.getString("IrcPassword");
        IRC_NICK = configManager.getString("Nick");
        IRC_USER = configManager.getString("User");
        IRC_NAME = configManager.getString("Name");
        IRC_NICKSERV = configManager.getBoolean("NickServ");
        IRC_NICKSERV_NAME = configManager.getString("NickservName");
        IRC_NICKSERV_COMMAND = configManager.getString("NickservCommand");
        IRC_LOGIN_COMMAND = configManager.getString("LoginCommand");
        IRC_CHANNEL = configManager.getString("Channel");
        IRC_ANNOUNCE = configManager.getBoolean("IrcAnnounces");
        IRC_FROM_GAME_TYPE = configManager.getString("GameToIrcType");
        IRC_TO_GAME_TYPE = configManager.getString("IrcToGameType");
        IRC_TO_GAME_SPECIAL_CHAR = configManager.getString("IrcToGameSpecialChar");
        IRC_TO_GAME_DISPLAY = configManager.getString("IrcToGameDisplay");
        IRC_NO_GM_MSG = configManager.getString("IrcNoGmMsg");
        IRC_NO_PLAYER_ONLINE = configManager.getString("IrcNoPlayerOnlineMsg");
        IRC_PLAYER_ONLINE = configManager.getString("IrcPlayerOnlineMsg");
        IRC_MSG_START = configManager.getString("IrcMsgStart");
    }

    //============================================================
    public void loadREBIRTHConfig() {

        REBIRTH_ENABLE = configManager.getBoolean("REBIRTH_ENABLE");
        REBIRTH_MIN_LEVEL = configManager.getInteger("REBIRTH_MIN_LEVEL");
        REBIRTH_MAX = configManager.getInteger("REBIRTH_MAX");
        REBIRTH_RETURN_TO_LEVEL = configManager.getInteger("REBIRTH_RETURN_TO_LEVEL");

        REBIRTH_ITEM_PRICE = configManager.getString("REBIRTH_ITEM_PRICE").split(";");
        REBIRTH_MAGE_SKILL = configManager.getString("REBIRTH_MAGE_SKILL").split(";");
        REBIRTH_FIGHTER_SKILL = configManager.getString("REBIRTH_FIGHTER_SKILL").split(";");
    }

    //==============================================================

    //============================================================
    public void loadPCBPointConfig() {

        PCB_ENABLE = configManager.getBoolean("PcBangPointEnable");
        PCB_MIN_LEVEL = configManager.getInteger("PcBangPointMinLevel");
        PCB_POINT_MIN = configManager.getInteger("PcBangPointMinCount");
        PCB_POINT_MAX = configManager.getInteger("PcBangPointMaxCount");

        if (PCB_POINT_MAX < 1) {
            PCB_POINT_MAX = Integer.MAX_VALUE;
        }

        PCB_CHANCE_DUAL_POINT = configManager.getInteger("PcBangPointDualChance");
        PCB_INTERVAL = configManager.getInteger("PcBangPointTimeStamp");

    }

    //============================================================
    public void loadDevConfig() {
        SKILLSDEBUG = configManager.getBoolean("SkillsDebug");
        DEBUG = configManager.getBoolean("GameDebug");
        ASSERT = configManager.getBoolean("Assert");
        DEVELOPER = configManager.getBoolean("DeveloperDebug");
        POSITION_DEBUG = configManager.getBoolean("PositionDebug");
        ZONE_DEBUG = configManager.getBoolean("ZoneDebug");
        DOOR_DEBUG = configManager.getBoolean("DoorDebug");
        ITEMLIST_DEBUG = configManager.getBoolean("ItemListDebug");
        AUTOHANDLER_DEBUG = configManager.getBoolean("AutoHandlerDebug");
        HANDLER_DEBUG = configManager.getBoolean("HandlerDebug");
        BOSS_DEBUG = configManager.getBoolean("BossDebug");
        SEVENSIGNFESTIVAL_DEBUG = configManager.getBoolean("SevenSignFestivalDebug");
        ENABLE_ALL_EXCEPTIONS = configManager.getBoolean("EnableAllExceptionsLog");
        SERVER_LIST_TESTSERVER = configManager.getBoolean("TestServer");
        SERVER_LIST_BRACKET = configManager.getBoolean("ServerListBrackets");
        SERVER_LIST_CLOCK = configManager.getBoolean("ServerListClock");
        SERVER_GMONLY = configManager.getBoolean("ServerGMOnly");
        ALT_DEV_NO_QUESTS = configManager.getBoolean("AltDevNoQuests");
        ALT_DEV_NO_SPAWNS = configManager.getBoolean("AltDevNoSpawns");
        ALT_DEV_NO_SCRIPT = configManager.getBoolean("AltDevNoScript");
        ALT_DEV_NO_AI = configManager.getBoolean("AltDevNoAI");
        ALT_DEV_NO_RB = configManager.getBoolean("AltDevNoRB");
        ENABLE_OLYMPIAD_DISCONNECTION_DEBUG = configManager.getBoolean("EnableOlympiadDisconnectionDebug");

        REQUEST_ID = configManager.getInteger("RequestServerID");
        ACCEPT_ALTERNATE_ID = configManager.getBoolean("AcceptAlternateID");

        CNAME_TEMPLATE = configManager.getString("CnameTemplate");
        PET_NAME_TEMPLATE = configManager.getString("PetNameTemplate");
        CLAN_NAME_TEMPLATE = configManager.getString("ClanNameTemplate");
        ALLY_NAME_TEMPLATE = configManager.getString("AllyNameTemplate");
        MAX_CHARACTERS_NUMBER_PER_ACCOUNT = configManager.getInteger("CharMaxNumber");

        MAX_CHARACTERS_NUMBER_PER_IP = configManager.getInteger("CharMaxNumberPerIP");

        MAXIMUM_ONLINE_USERS = configManager.getInteger("MaximumOnlineUsers");

        MIN_PROTOCOL_REVISION = configManager.getInteger("MinProtocolRevision");
        MAX_PROTOCOL_REVISION = configManager.getInteger("MaxProtocolRevision");
        if (MIN_PROTOCOL_REVISION > MAX_PROTOCOL_REVISION) {
            throw new Error("MinProtocolRevision is bigger than MaxProtocolRevision in server configuration file.");
        }

        GMAUDIT = configManager.getBoolean("GMAudit");
        LOG_CHAT = configManager.getBoolean("DevLogChat");
        LOG_ITEMS = configManager.getBoolean("LogItems");
        LOG_HIGH_DAMAGES = configManager.getBoolean("LogHighDamages");

        GAMEGUARD_L2NET_CHECK = configManager.getBoolean("GameGuardL2NetCheck");


        LAZY_CACHE = configManager.getBoolean("LazyCache");

    }

    //============================================================
    public void loadCraftConfig() {
        DWARF_RECIPE_LIMIT = configManager.getInteger("DwarfRecipeLimit");
        COMMON_RECIPE_LIMIT = configManager.getInteger("CommonRecipeLimit");
        IS_CRAFTING_ENABLED = configManager.getBoolean("CraftingEnabled");
        ALT_GAME_CREATION = configManager.getBoolean("AltGameCreation");
        ALT_GAME_CREATION_SPEED = configManager.getDouble("AltGameCreationSpeed");
        ALT_GAME_CREATION_XP_RATE = configManager.getDouble("AltGameCreationRateXp");
        ALT_GAME_CREATION_SP_RATE = configManager.getDouble("AltGameCreationRateSp");
        ALT_BLACKSMITH_USE_RECIPES = configManager.getBoolean("AltBlacksmithUseRecipes");

    }

    //============================================================
    public void loadAWAYConfig() {

        /** Away System **/
        ALLOW_AWAY_STATUS = configManager.getBoolean("AllowAwayStatus");
        AWAY_PLAYER_TAKE_AGGRO = configManager.getBoolean("AwayPlayerTakeAggro");
        AWAY_TITLE_COLOR = configManager.getInteger("AwayTitleColor");
        AWAY_TIMER = configManager.getInteger("AwayTimer");
        BACK_TIMER = configManager.getInteger("BackTimer");
        AWAY_PEACE_ZONE = configManager.getBoolean("AwayOnlyInPeaceZone");
    }

    //============================================================
    public void loadBankingConfig() {
        BANKING_SYSTEM_ENABLED = configManager.getBoolean("BankingEnabled");
        BANKING_SYSTEM_GOLDBARS = configManager.getInteger("BankingGoldbarCount");
        BANKING_SYSTEM_ADENA = configManager.getInteger("BankingAdenaCount");
    }

    //============================================================
    public void loadOfflineConfig() {
        OFFLINE_TRADE_ENABLE = configManager.getBoolean("OfflineTradeEnable");
        OFFLINE_CRAFT_ENABLE = configManager.getBoolean("OfflineCraftEnable");
        OFFLINE_SET_NAME_COLOR = configManager.getBoolean("OfflineNameColorEnable");
        OFFLINE_NAME_COLOR = configManager.getInteger("OfflineNameColor");

        OFFLINE_COMMAND1 = configManager.getBoolean("OfflineCommand1");
        OFFLINE_COMMAND2 = configManager.getBoolean("OfflineCommand2");
        OFFLINE_LOGOUT = configManager.getBoolean("OfflineLogout");
        OFFLINE_SLEEP_EFFECT = configManager.getBoolean("OfflineSleepEffect");

        RESTORE_OFFLINERS = configManager.getBoolean("RestoreOffliners");
        OFFLINE_MAX_DAYS = configManager.getInteger("OfflineMaxDays");
        OFFLINE_DISCONNECT_FINISHED = configManager.getBoolean("OfflineDisconnectFinished");
    }

    //============================================================
    public void loadFrozenConfig() {
        GM_TRADE_RESTRICTED_ITEMS = configManager.getBoolean("GMTradeRestrictedItems");
        GM_RESTART_FIGHTING = configManager.getBoolean("GMRestartFighting");
        PM_MESSAGE_ON_START = configManager.getBoolean("PMWelcomeShow");
        SERVER_TIME_ON_START = configManager.getBoolean("ShowServerTimeOnStart");
        PM_SERVER_NAME = configManager.getString("PMServerName");
        PM_TEXT1 = configManager.getString("PMText1");
        PM_TEXT2 = configManager.getString("PMText2");
        NEW_PLAYER_EFFECT = configManager.getBoolean("NewPlayerEffect");
    }

    //============================================================
    public void loadDMConfig() {
        DM_ALLOW_INTERFERENCE = configManager.getBoolean("DMAllowInterference");
        DM_ALLOW_POTIONS = configManager.getBoolean("DMAllowPotions");
        DM_ALLOW_SUMMON = configManager.getBoolean("DMAllowSummon");
        DM_JOIN_CURSED = configManager.getBoolean("DMJoinWithCursedWeapon");
        DM_ON_START_REMOVE_ALL_EFFECTS = configManager.getBoolean("DMOnStartRemoveAllEffects");
        DM_ON_START_UNSUMMON_PET = configManager.getBoolean("DMOnStartUnsummonPet");
        DM_REVIVE_DELAY = configManager.getLong("DMReviveDelay");
        if (DM_REVIVE_DELAY < 1000) {
            DM_REVIVE_DELAY = 1000; //can't be set less then 1 second
        }

        DM_REVIVE_RECOVERY = configManager.getBoolean("DMReviveRecovery");

        DM_COMMAND = configManager.getBoolean("DMCommand");
        DM_ENABLE_KILL_REWARD = configManager.getBoolean("DMEnableKillReward");
        DM_KILL_REWARD_ID = configManager.getInteger("DMKillRewardID");
        DM_KILL_REWARD_AMOUNT = configManager.getInteger("DMKillRewardAmount");

        DM_ANNOUNCE_REWARD = configManager.getBoolean("DMAnnounceReward");
        DM_SPAWN_OFFSET = configManager.getInteger("DMSpawnOffset");

        DM_STATS_LOGGER = configManager.getBoolean("DMStatsLogger");

        DM_ALLOW_HEALER_CLASSES = configManager.getBoolean("DMAllowedHealerClasses");

        DM_REMOVE_BUFFS_ON_DIE = configManager.getBoolean("DMRemoveBuffsOnPlayerDie");
    }

    //============================================================
    public void loadCTFConfig() {
        CTF_EVEN_TEAMS = configManager.getString("CTFEvenTeams");
        CTF_ALLOW_INTERFERENCE = configManager.getBoolean("CTFAllowInterference");
        CTF_ALLOW_POTIONS = configManager.getBoolean("CTFAllowPotions");
        CTF_ALLOW_SUMMON = configManager.getBoolean("CTFAllowSummon");
        CTF_ON_START_REMOVE_ALL_EFFECTS = configManager.getBoolean("CTFOnStartRemoveAllEffects");
        CTF_ON_START_UNSUMMON_PET = configManager.getBoolean("CTFOnStartUnsummonPet");
        CTF_ANNOUNCE_TEAM_STATS = configManager.getBoolean("CTFAnnounceTeamStats");
        CTF_ANNOUNCE_REWARD = configManager.getBoolean("CTFAnnounceReward");
        CTF_JOIN_CURSED = configManager.getBoolean("CTFJoinWithCursedWeapon");
        CTF_REVIVE_RECOVERY = configManager.getBoolean("CTFReviveRecovery");
        CTF_COMMAND = configManager.getBoolean("CTFCommand");
        CTF_AURA = configManager.getBoolean("CTFAura");

        CTF_STATS_LOGGER = configManager.getBoolean("CTFStatsLogger");

        CTF_SPAWN_OFFSET = configManager.getInteger("CTFSpawnOffset");
    }

    //============================================================
    public void loadL2JFrozenConfig() {
        /** Custom Tables **/
        CUSTOM_SPAWNLIST_TABLE = configManager.getBoolean("CustomSpawnlistTable");
        SAVE_GMSPAWN_ON_CUSTOM = configManager.getBoolean("SaveGmSpawnOnCustom");
        DELETE_GMSPAWN_ON_CUSTOM = configManager.getBoolean("DeleteGmSpawnOnCustom");

        ONLINE_PLAYERS_ON_LOGIN = configManager.getBoolean("OnlineOnLogin");
        SHOW_SERVER_VERSION = configManager.getBoolean("ShowServerVersion");

        /** Protector **/
        PROTECTOR_PLAYER_PK = configManager.getBoolean("ProtectorPlayerPK");
        PROTECTOR_PLAYER_PVP = configManager.getBoolean("ProtectorPlayerPVP");
        PROTECTOR_RADIUS_ACTION = configManager.getInteger("ProtectorRadiusAction");
        PROTECTOR_SKILLID = configManager.getInteger("ProtectorSkillId");
        PROTECTOR_SKILLLEVEL = configManager.getInteger("ProtectorSkillLevel");
        PROTECTOR_SKILLTIME = configManager.getInteger("ProtectorSkillTime");
        PROTECTOR_MESSAGE = configManager.getString("ProtectorMessage");

        /** Donator color name **/
        DONATOR_NAME_COLOR_ENABLED = configManager.getBoolean("DonatorNameColorEnabled");
        DONATOR_NAME_COLOR = configManager.getInteger("DonatorColorName");
        DONATOR_TITLE_COLOR = configManager.getInteger("DonatorTitleColor");
        DONATOR_XPSP_RATE = configManager.getFloat("DonatorXpSpRate");
        DONATOR_ADENA_RATE = configManager.getFloat("DonatorAdenaRate");
        DONATOR_DROP_RATE = configManager.getFloat("DonatorDropRate");
        DONATOR_SPOIL_RATE = configManager.getFloat("DonatorSpoilRate");

        /** Welcome Htm **/
        WELCOME_HTM = configManager.getBoolean("WelcomeHtm");

        /** Server Name **/
        ALT_SERVER_NAME_ENABLED = configManager.getBoolean("ServerNameEnabled");
        ANNOUNCE_TO_ALL_SPAWN_RB = configManager.getBoolean("AnnounceToAllSpawnRb");
        ANNOUNCE_TRY_BANNED_ACCOUNT = configManager.getBoolean("AnnounceTryBannedAccount");
        ALT_Server_Name = String.valueOf(configManager.getString("ServerName"));
        DIFFERENT_Z_CHANGE_OBJECT = configManager.getInteger("DifferentZchangeObject");
        DIFFERENT_Z_NEW_MOVIE = configManager.getInteger("DifferentZnewmovie");

        ALLOW_SIMPLE_STATS_VIEW = configManager.getBoolean("AllowSimpleStatsView");
        ALLOW_DETAILED_STATS_VIEW = configManager.getBoolean("AllowDetailedStatsView");
        ALLOW_ONLINE_VIEW = configManager.getBoolean("AllowOnlineView");

        KEEP_SUBCLASS_SKILLS = configManager.getBoolean("KeepSubClassSkills");
        ALLOWED_SKILLS_LIST.addAll(configManager.getArray("AllowedSkills", ",", ConfigManager.PropertyType.INTEGER));

        CASTLE_SHIELD = configManager.getBoolean("CastleShieldRestriction");
        CLANHALL_SHIELD = configManager.getBoolean("ClanHallShieldRestriction");
        APELLA_ARMORS = configManager.getBoolean("ApellaArmorsRestriction");
        OATH_ARMORS = configManager.getBoolean("OathArmorsRestriction");
        CASTLE_CROWN = configManager.getBoolean("CastleLordsCrownRestriction");
        CASTLE_CIRCLETS = configManager.getBoolean("CastleCircletsRestriction");
        CHAR_TITLE = configManager.getBoolean("CharTitle");
        ADD_CHAR_TITLE = configManager.getString("CharAddTitle");

        NOBLE_CUSTOM_ITEMS = configManager.getBoolean("EnableNobleCustomItem");
        NOOBLE_CUSTOM_ITEM_ID = configManager.getInteger("NoobleCustomItemId");
        HERO_CUSTOM_ITEMS = configManager.getBoolean("EnableHeroCustomItem");
        HERO_CUSTOM_ITEM_ID = configManager.getInteger("HeroCustomItemId");
        HERO_CUSTOM_DAY = configManager.getInteger("HeroCustomDay");

        ALLOW_CREATE_LVL = configManager.getBoolean("CustomStartingLvl");
        CHAR_CREATE_LVL = configManager.getInteger("CharLvl");
        SPAWN_CHAR = configManager.getBoolean("CustomSpawn");
        SPAWN_X = configManager.getInteger("SpawnX");
        SPAWN_Y = configManager.getInteger("SpawnY");
        SPAWN_Z = configManager.getInteger("SpawnZ");
        ALLOW_LOW_LEVEL_TRADE = configManager.getBoolean("AllowLowLevelTrade");
        ALLOW_HERO_SUBSKILL = configManager.getBoolean("CustomHeroSubSkill");
        HERO_COUNT = configManager.getInteger("HeroCount");
        CRUMA_TOWER_LEVEL_RESTRICT = configManager.getInteger("CrumaTowerLevelRestrict");
        ALLOW_RAID_BOSS_PETRIFIED = configManager.getBoolean("AllowRaidBossPetrified");
        ALT_PLAYER_PROTECTION_LEVEL = configManager.getInteger("AltPlayerProtectionLevel");
        MONSTER_RETURN_DELAY = configManager.getInteger("MonsterReturnDelay");
        SCROLL_STACKABLE = configManager.getBoolean("ScrollStackable");
        ALLOW_CHAR_KILL_PROTECT = configManager.getBoolean("AllowLowLvlProtect");
        CLAN_LEADER_COLOR_ENABLED = configManager.getBoolean("ClanLeaderNameColorEnabled");
        CLAN_LEADER_COLORED = configManager.getInteger("ClanLeaderColored");
        CLAN_LEADER_COLOR = configManager.getInteger("ClanLeaderColor");
        CLAN_LEADER_COLOR_CLAN_LEVEL = configManager.getInteger("ClanLeaderColorAtClanLevel");
        ALLOW_VERSION_COMMAND = configManager.getBoolean("AllowVersionCommand");
        SAVE_RAIDBOSS_STATUS_INTO_DB = configManager.getBoolean("SaveRBStatusIntoDB");
        DISABLE_WEIGHT_PENALTY = configManager.getBoolean("DisableWeightPenalty");
        ALLOW_FARM1_COMMAND = configManager.getBoolean("AllowFarm1Command");
        ALLOW_FARM2_COMMAND = configManager.getBoolean("AllowFarm2Command");
        ALLOW_PVP1_COMMAND = configManager.getBoolean("AllowPvP1Command");
        ALLOW_PVP2_COMMAND = configManager.getBoolean("AllowPvP2Command");
        FARM1_X = configManager.getInteger("farm1_X");
        FARM1_Y = configManager.getInteger("farm1_Y");
        FARM1_Z = configManager.getInteger("farm1_Z");
        PVP1_X = configManager.getInteger("pvp1_X");
        PVP1_Y = configManager.getInteger("pvp1_Y");
        PVP1_Z = configManager.getInteger("pvp1_Z");
        FARM2_X = configManager.getInteger("farm2_X");
        FARM2_Y = configManager.getInteger("farm2_Y");
        FARM2_Z = configManager.getInteger("farm2_Z");
        PVP2_X = configManager.getInteger("pvp2_X");
        PVP2_Y = configManager.getInteger("pvp2_Y");
        PVP2_Z = configManager.getInteger("pvp2_Z");
        FARM1_CUSTOM_MESSAGE = configManager.getString("Farm1CustomMeesage");
        FARM2_CUSTOM_MESSAGE = configManager.getString("Farm2CustomMeesage");
        PVP1_CUSTOM_MESSAGE = configManager.getString("PvP1CustomMeesage");
        PVP2_CUSTOM_MESSAGE = configManager.getString("PvP2CustomMeesage");
    }

    //============================================================
    public void loadPvpConfig() {

			/* KARMA SYSTEM */
        KARMA_MIN_KARMA = configManager.getInteger("MinKarma");
        KARMA_MAX_KARMA = configManager.getInteger("MaxKarma");
        KARMA_XP_DIVIDER = configManager.getInteger("XPDivider");
        KARMA_LOST_BASE = configManager.getInteger("BaseKarmaLost");

        KARMA_DROP_GM = configManager.getBoolean("CanGMDropEquipment");
        KARMA_AWARD_PK_KILL = configManager.getBoolean("AwardPKKillPVPPoint");

        KARMA_PK_LIMIT = configManager.getInteger("MinimumPKRequiredToDrop");
        KARMA_LIST_NONDROPPABLE_PET_ITEMS = new FastList<>();
        KARMA_LIST_NONDROPPABLE_ITEMS = new FastList<>();
        KARMA_LIST_NONDROPPABLE_PET_ITEMS.addAll(configManager.getArray("ListOfPetItems", ",", ConfigManager.PropertyType.INTEGER));
        KARMA_LIST_NONDROPPABLE_ITEMS.addAll(configManager.getArray("ListOfNonDroppableItems", ",", ConfigManager.PropertyType.INTEGER));

        PVP_NORMAL_TIME = configManager.getInteger("PvPVsNormalTime");
        PVP_PVP_TIME = configManager.getInteger("PvPVsPvPTime");
        ALT_GAME_KARMA_PLAYER_CAN_BE_KILLED_IN_PEACEZONE = configManager.getBoolean("AltKarmaPlayerCanBeKilledInPeaceZone");
        ALT_GAME_KARMA_PLAYER_CAN_SHOP = configManager.getBoolean("AltKarmaPlayerCanShop");
        ALT_GAME_KARMA_PLAYER_CAN_USE_GK = configManager.getBoolean("AltKarmaPlayerCanUseGK");
        ALT_GAME_KARMA_PLAYER_CAN_TELEPORT = configManager.getBoolean("AltKarmaPlayerCanTeleport");
        ALT_GAME_KARMA_PLAYER_CAN_TRADE = configManager.getBoolean("AltKarmaPlayerCanTrade");
        ALT_GAME_KARMA_PLAYER_CAN_USE_WAREHOUSE = configManager.getBoolean("AltKarmaPlayerCanUseWareHouse");
        ALT_KARMA_TELEPORT_TO_FLORAN = configManager.getBoolean("AltKarmaTeleportToFloran");
        /** Custom Reword **/
        PVP_REWARD_ENABLED = configManager.getBoolean("PvpRewardEnabled");
        PVP_REWARD_ID = configManager.getInteger("PvpRewardItemId");
        PVP_REWARD_AMOUNT = configManager.getInteger("PvpRewardAmmount");

        PK_REWARD_ENABLED = configManager.getBoolean("PKRewardEnabled");
        PK_REWARD_ID = configManager.getInteger("PKRewardItemId");
        PK_REWARD_AMOUNT = configManager.getInteger("PKRewardAmmount");

        REWARD_PROTECT = configManager.getInteger("RewardProtect");

        // PVP Name Color System configs - Start
        PVP_COLOR_SYSTEM_ENABLED = configManager.getBoolean("EnablePvPColorSystem");
        PVP_AMOUNT1 = configManager.getInteger("PvpAmount1");
        PVP_AMOUNT2 = configManager.getInteger("PvpAmount2");
        PVP_AMOUNT3 = configManager.getInteger("PvpAmount3");
        PVP_AMOUNT4 = configManager.getInteger("PvpAmount4");
        PVP_AMOUNT5 = configManager.getInteger("PvpAmount5");
        NAME_COLOR_FOR_PVP_AMOUNT1 = configManager.getInteger("ColorForAmount1");
        NAME_COLOR_FOR_PVP_AMOUNT2 = configManager.getInteger("ColorForAmount2");
        NAME_COLOR_FOR_PVP_AMOUNT3 = configManager.getInteger("ColorForAmount3");
        NAME_COLOR_FOR_PVP_AMOUNT4 = configManager.getInteger("ColorForAmount4");
        NAME_COLOR_FOR_PVP_AMOUNT5 = configManager.getInteger("ColorForAmount5");

        // PK Title Color System configs - Start
        PK_COLOR_SYSTEM_ENABLED = configManager.getBoolean("EnablePkColorSystem");
        PK_AMOUNT1 = configManager.getInteger("PkAmount1");
        PK_AMOUNT2 = configManager.getInteger("PkAmount2");
        PK_AMOUNT3 = configManager.getInteger("PkAmount3");
        PK_AMOUNT4 = configManager.getInteger("PkAmount4");
        PK_AMOUNT5 = configManager.getInteger("PkAmount5");
        TITLE_COLOR_FOR_PK_AMOUNT1 = configManager.getInteger("TitleForAmount1");
        TITLE_COLOR_FOR_PK_AMOUNT2 = configManager.getInteger("TitleForAmount2");
        TITLE_COLOR_FOR_PK_AMOUNT3 = configManager.getInteger("TitleForAmount3");
        TITLE_COLOR_FOR_PK_AMOUNT4 = configManager.getInteger("TitleForAmount4");
        TITLE_COLOR_FOR_PK_AMOUNT5 = configManager.getInteger("TitleForAmount5");

        FLAGED_PLAYER_USE_BUFFER = configManager.getBoolean("AltKarmaFlagPlayerCanUseBuffer");

        FLAGED_PLAYER_CAN_USE_GK = configManager.getBoolean("FlaggedPlayerCanUseGK");
        PVPEXPSP_SYSTEM = configManager.getBoolean("AllowAddExpSpAtPvP");
        ADD_EXP = configManager.getInteger("AddExpAtPvp");
        ADD_SP = configManager.getInteger("AddSpAtPvp");
        ALLOW_SOE_IN_PVP = configManager.getBoolean("AllowSoEInPvP");
        ALLOW_POTS_IN_PVP = configManager.getBoolean("AllowPotsInPvP");
        /** Enable Pk Info mod. Displays number of times player has killed other */
        ENABLE_PK_INFO = configManager.getBoolean("EnablePkInfo");
        // Get the AnnounceAllKill, AnnouncePvpKill and AnnouncePkKill values
        ANNOUNCE_ALL_KILL = configManager.getBoolean("AnnounceAllKill");
        ANNOUNCE_PVP_KILL = configManager.getBoolean("AnnouncePvPKill");
        ANNOUNCE_PK_KILL = configManager.getBoolean("AnnouncePkKill");

        DUEL_SPAWN_X = configManager.getInteger("DuelSpawnX");
        DUEL_SPAWN_Y = configManager.getInteger("DuelSpawnY");
        DUEL_SPAWN_Z = configManager.getInteger("DuelSpawnZ");
        PVP_PK_TITLE = configManager.getBoolean("PvpPkTitle");
        PVP_TITLE_PREFIX = configManager.getString("PvPTitlePrefix");
        PK_TITLE_PREFIX = configManager.getString("PkTitlePrefix");

        WAR_LEGEND_AURA = configManager.getBoolean("WarLegendAura");
        KILLS_TO_GET_WAR_LEGEND_AURA = configManager.getInteger("KillsToGetWarLegendAura");

        ANTI_FARM_ENABLED = configManager.getBoolean("AntiFarmEnabled");
        ANTI_FARM_CLAN_ALLY_ENABLED = configManager.getBoolean("AntiFarmClanAlly");
        ANTI_FARM_LVL_DIFF_ENABLED = configManager.getBoolean("AntiFarmLvlDiff");
        ANTI_FARM_MAX_LVL_DIFF = configManager.getInteger("AntiFarmMaxLvlDiff");
        ANTI_FARM_PDEF_DIFF_ENABLED = configManager.getBoolean("AntiFarmPdefDiff");
        ANTI_FARM_MAX_PDEF_DIFF = configManager.getInteger("AntiFarmMaxPdefDiff");
        ANTI_FARM_PATK_DIFF_ENABLED = configManager.getBoolean("AntiFarmPatkDiff");
        ANTI_FARM_MAX_PATK_DIFF = configManager.getInteger("AntiFarmMaxPatkDiff");
        ANTI_FARM_PARTY_ENABLED = configManager.getBoolean("AntiFarmParty");
        ANTI_FARM_IP_ENABLED = configManager.getBoolean("AntiFarmIP");
        ANTI_FARM_SUMMON = configManager.getBoolean("AntiFarmSummon");
    }

    public void loadOlympConfig() {
        ALT_OLY_START_TIME = configManager.getInteger("AltOlyStartTime");
        ALT_OLY_MIN = configManager.getInteger("AltOlyMin");
        ALT_OLY_CPERIOD = configManager.getLong("AltOlyCPeriod");
        ALT_OLY_BATTLE = configManager.getLong("AltOlyBattle");
        ALT_OLY_WPERIOD = configManager.getLong("AltOlyWPeriod");
        ALT_OLY_VPERIOD = configManager.getLong("AltOlyVPeriod");
        ALT_OLY_CLASSED = configManager.getInteger("AltOlyClassedParticipants");
        ALT_OLY_NONCLASSED = configManager.getInteger("AltOlyNonClassedParticipants");
        ALT_OLY_BATTLE_REWARD_ITEM = configManager.getInteger("AltOlyBattleRewItem");
        ALT_OLY_CLASSED_RITEM_C = configManager.getInteger("AltOlyClassedRewItemCount");
        ALT_OLY_NONCLASSED_RITEM_C = configManager.getInteger("AltOlyNonClassedRewItemCount");
        ALT_OLY_COMP_RITEM = configManager.getInteger("AltOlyCompRewItem");
        ALT_OLY_GP_PER_POINT = configManager.getInteger("AltOlyGPPerPoint");
        ALT_OLY_MIN_POINT_FOR_EXCH = configManager.getInteger("AltOlyMinPointForExchange");
        ALT_OLY_HERO_POINTS = configManager.getInteger("AltOlyHeroPoints");
        LIST_OLY_RESTRICTED_ITEMS = new FastList<>();
        LIST_OLY_RESTRICTED_ITEMS.addAll(configManager.getArray("AltOlyRestrictedItems", ",", ConfigManager.PropertyType.INTEGER));

        ALLOW_EVENTS_DURING_OLY = configManager.getBoolean("AllowEventsDuringOly");

        ALT_OLY_RECHARGE_SKILLS = configManager.getBoolean("AltOlyRechargeSkills");
            
			/* Remove cubic at the enter of olympiad */
        REMOVE_CUBIC_OLYMPIAD = configManager.getBoolean("RemoveCubicOlympiad");

        ALT_OLY_NUMBER_HEROS_EACH_CLASS = configManager.getInteger("AltOlyNumberHerosEachClass");
        ALT_OLY_LOG_FIGHTS = configManager.getBoolean("AlyOlyLogFights");
        ALT_OLY_SHOW_MONTHLY_WINNERS = configManager.getBoolean("AltOlyShowMonthlyWinners");
        ALT_OLY_ANNOUNCE_GAMES = configManager.getBoolean("AltOlyAnnounceGames");
        LIST_OLY_RESTRICTED_SKILLS = new FastList<>();
        LIST_OLY_RESTRICTED_SKILLS.addAll(configManager.getArray("AltOlyRestrictedSkills", ",", ConfigManager.PropertyType.INTEGER));

        ALT_OLY_AUGMENT_ALLOW = configManager.getBoolean("AltOlyAugmentAllow");
        ALT_OLY_TELEPORT_COUNTDOWN = configManager.getInteger("AltOlyTeleportCountDown");

        ALT_OLY_USE_CUSTOM_PERIOD_SETTINGS = configManager.getBoolean("AltOlyUseCustomPeriodSettings");
        ALT_OLY_PERIOD = OlympiadPeriod.valueOf(configManager.getString("AltOlyPeriod"));
        ALT_OLY_PERIOD_MULTIPLIER = configManager.getInteger("AltOlyPeriodMultiplier");
    }

    //============================================================
    public void loadEnchantConfig() {

        NORMAL_WEAPON_ENCHANT_LEVEL.putAll(configManager.getHashMap("NormalWeaponEnchantLevel"));
        BLESS_WEAPON_ENCHANT_LEVEL.putAll(configManager.getHashMap("BlessWeaponEnchantLevel"));

        CRYSTAL_WEAPON_ENCHANT_LEVEL.putAll(configManager.getHashMap("CrystalWeaponEnchantLevel"));
        NORMAL_ARMOR_ENCHANT_LEVEL.putAll(configManager.getHashMap("NormalArmorEnchantLevel"));
        BLESS_ARMOR_ENCHANT_LEVEL.putAll(configManager.getHashMap("BlessArmorEnchantLevel"));
        CRYSTAL_ARMOR_ENCHANT_LEVEL.putAll(configManager.getHashMap("CrystalArmorEnchantLevel"));
        NORMAL_JEWELRY_ENCHANT_LEVEL.putAll(configManager.getHashMap("NormalJewelryEnchantLevel"));
        BLESS_JEWELRY_ENCHANT_LEVEL.putAll(configManager.getHashMap("BlessJewelryEnchantLevel"));
        CRYSTAL_JEWELRY_ENCHANT_LEVEL.putAll(configManager.getHashMap("CrystalJewelryEnchantLevel"));

        /** limit of safe enchant normal **/
        ENCHANT_SAFE_MAX = configManager.getInteger("EnchantSafeMax");

        /** limit of safe enchant full **/
        ENCHANT_SAFE_MAX_FULL = configManager.getInteger("EnchantSafeMaxFull");

        /** limit of max enchant **/
        ENCHANT_WEAPON_MAX = configManager.getInteger("EnchantWeaponMax");
        ENCHANT_ARMOR_MAX = configManager.getInteger("EnchantArmorMax");
        ENCHANT_JEWELRY_MAX = configManager.getInteger("EnchantJewelryMax");


        /** CRYSTAL SCROLL enchant limits **/
        CRYSTAL_ENCHANT_MIN = configManager.getInteger("CrystalEnchantMin");
        CRYSTAL_ENCHANT_MAX = configManager.getInteger("CrystalEnchantMax");

        /** bonus for dwarf **/
        ENABLE_DWARF_ENCHANT_BONUS = configManager.getBoolean("EnableDwarfEnchantBonus");
        DWARF_ENCHANT_MIN_LEVEL = configManager.getInteger("DwarfEnchantMinLevel");
        DWARF_ENCHANT_BONUS = configManager.getInteger("DwarfEnchantBonus");

        /** augmentation chance **/
        AUGMENTATION_NG_SKILL_CHANCE = configManager.getInteger("AugmentationNGSkillChance");
        AUGMENTATION_MID_SKILL_CHANCE = configManager.getInteger("AugmentationMidSkillChance");
        AUGMENTATION_HIGH_SKILL_CHANCE = configManager.getInteger("AugmentationHighSkillChance");
        AUGMENTATION_TOP_SKILL_CHANCE = configManager.getInteger("AugmentationTopSkillChance");
        AUGMENTATION_BASESTAT_CHANCE = configManager.getInteger("AugmentationBaseStatChance");

        /** augmentation glow **/
        AUGMENTATION_NG_GLOW_CHANCE = configManager.getInteger("AugmentationNGGlowChance");
        AUGMENTATION_MID_GLOW_CHANCE = configManager.getInteger("AugmentationMidGlowChance");
        AUGMENTATION_HIGH_GLOW_CHANCE = configManager.getInteger("AugmentationHighGlowChance");
        AUGMENTATION_TOP_GLOW_CHANCE = configManager.getInteger("AugmentationTopGlowChance");

        /** augmentation configs **/
        DELETE_AUGM_PASSIVE_ON_CHANGE = configManager.getBoolean("DeleteAgmentPassiveEffectOnChangeWep");
        DELETE_AUGM_ACTIVE_ON_CHANGE = configManager.getBoolean("DeleteAgmentActiveEffectOnChangeWep");

        /** enchant hero weapon **/
        ENCHANT_HERO_WEAPON = configManager.getBoolean("EnableEnchantHeroWeapons");

        /** soul crystal **/
        SOUL_CRYSTAL_BREAK_CHANCE = configManager.getInteger("SoulCrystalBreakChance");
        SOUL_CRYSTAL_LEVEL_CHANCE = configManager.getInteger("SoulCrystalLevelChance");
        SOUL_CRYSTAL_MAX_LEVEL = configManager.getInteger("SoulCrystalMaxLevel");

        /** count enchant **/
        CUSTOM_ENCHANT_VALUE = configManager.getInteger("CustomEnchantValue");
        ALT_OLY_ENCHANT_LIMIT = configManager.getInteger("AltOlyMaxEnchant");
        BREAK_ENCHANT = configManager.getInteger("BreakEnchant");

        MAX_ITEM_ENCHANT_KICK = configManager.getInteger("EnchantKick");
        GM_OVER_ENCHANT = configManager.getInteger("GMOverEnchant");
    }

    //============================================================
    public void loadPacketConfig() {

        ENABLE_UNK_PACKET_PROTECTION = configManager.getBoolean("UnknownPacketProtection");
        MAX_UNKNOWN_PACKETS = configManager.getInteger("UnknownPacketsBeforeBan");
        UNKNOWN_PACKETS_PUNiSHMENT = configManager.getInteger("UnknownPacketsPunishment");
        DEBUG_PACKETS = configManager.getBoolean("GameDebugPackets");
        DEBUG_UNKNOWN_PACKETS = configManager.getBoolean("UnknownDebugPackets");

    }

    //============================================================
    public void loadPOtherConfig() {

        CHECK_NAME_ON_LOGIN = configManager.getBoolean("CheckNameOnEnter");
        CHECK_SKILLS_ON_ENTER = configManager.getBoolean("CheckSkillsOnEnter");

        /** l2walker protection **/
        L2WALKER_PROTEC = configManager.getBoolean("L2WalkerProtection");

        /** enchant protected **/
        PROTECTED_ENCHANT = configManager.getBoolean("ProtectorEnchant");

        ONLY_GM_TELEPORT_FREE = configManager.getBoolean("OnlyGMTeleportFree");
        ONLY_GM_ITEMS_FREE = configManager.getBoolean("OnlyGMItemsFree");

        BYPASS_VALIDATION = configManager.getBoolean("BypassValidation");

        ALLOW_DUALBOX_OLY = configManager.getBoolean("AllowDualBoxInOly");
        ALLOW_DUALBOX_EVENT = configManager.getBoolean("AllowDualBoxInEvent");
        ALLOWED_BOXES = configManager.getInteger("AllowedBoxes");
        ALLOW_DUALBOX = configManager.getBoolean("AllowDualBox");

        BOT_PROTECTOR = configManager.getBoolean("BotProtect");
        BOT_PROTECTOR_FIRST_CHECK = configManager.getInteger("BotProtectFirstCheck");
        BOT_PROTECTOR_NEXT_CHECK = configManager.getInteger("BotProtectNextCheck");
        BOT_PROTECTOR_WAIT_ANSVER = configManager.getInteger("BotProtectAnsver");

    }

    //============================================================
    public void loadPHYSICSConfig() {

        ENABLE_CLASS_DAMAGES = configManager.getBoolean("EnableClassDamagesSettings");
        ENABLE_CLASS_DAMAGES_IN_OLY = configManager.getBoolean("EnableClassDamagesSettingsInOly");
        ENABLE_CLASS_DAMAGES_LOGGER = configManager.getBoolean("EnableClassDamagesLogger");

        BLOW_ATTACK_FRONT = configManager.getInteger("BlowAttackFront");
        BLOW_ATTACK_SIDE = configManager.getInteger("BlowAttackSide");
        BLOW_ATTACK_BEHIND = configManager.getInteger("BlowAttackBehind");

        BACKSTAB_ATTACK_FRONT = configManager.getInteger("BackstabAttackFront");
        BACKSTAB_ATTACK_SIDE = configManager.getInteger("BackstabAttackSide");
        BACKSTAB_ATTACK_BEHIND = configManager.getInteger("BackstabAttackBehind");

        // Max patk speed and matk speed
        MAX_PATK_SPEED = configManager.getInteger("MaxPAtkSpeed");
        MAX_MATK_SPEED = configManager.getInteger("MaxMAtkSpeed");

        if (MAX_PATK_SPEED < 1) {
            MAX_PATK_SPEED = Integer.MAX_VALUE;
        }

        if (MAX_MATK_SPEED < 1) {
            MAX_MATK_SPEED = Integer.MAX_VALUE;
        }

        MAX_PCRIT_RATE = configManager.getInteger("MaxPCritRate");
        MAX_MCRIT_RATE = configManager.getInteger("MaxMCritRate");
        MCRIT_RATE_MUL = configManager.getFloat("McritMulDif");

        MAGIC_CRITICAL_POWER = configManager.getFloat("MagicCriticalPower");

        STUN_CHANCE_MODIFIER = configManager.getFloat("StunChanceModifier");
        BLEED_CHANCE_MODIFIER = configManager.getFloat("BleedChanceModifier");
        POISON_CHANCE_MODIFIER = configManager.getFloat("PoisonChanceModifier");
        PARALYZE_CHANCE_MODIFIER = configManager.getFloat("ParalyzeChanceModifier");
        ROOT_CHANCE_MODIFIER = configManager.getFloat("RootChanceModifier");
        SLEEP_CHANCE_MODIFIER = configManager.getFloat("SleepChanceModifier");
        FEAR_CHANCE_MODIFIER = configManager.getFloat("FearChanceModifier");
        CONFUSION_CHANCE_MODIFIER = configManager.getFloat("ConfusionChanceModifier");
        DEBUFF_CHANCE_MODIFIER = configManager.getFloat("DebuffChanceModifier");
        BUFF_CHANCE_MODIFIER = configManager.getFloat("BuffChanceModifier");

        ALT_MAGES_PHYSICAL_DAMAGE_MULTI = configManager.getFloat("AltPDamageMages");
        ALT_MAGES_MAGICAL_DAMAGE_MULTI = configManager.getFloat("AltMDamageMages");
        ALT_FIGHTERS_PHYSICAL_DAMAGE_MULTI = configManager.getFloat("AltPDamageFighters");
        ALT_FIGHTERS_MAGICAL_DAMAGE_MULTI = configManager.getFloat("AltMDamageFighters");
        ALT_PETS_PHYSICAL_DAMAGE_MULTI = configManager.getFloat("AltPDamagePets");
        ALT_PETS_MAGICAL_DAMAGE_MULTI = configManager.getFloat("AltMDamagePets");
        ALT_NPC_PHYSICAL_DAMAGE_MULTI = configManager.getFloat("AltPDamageNpc");
        ALT_NPC_MAGICAL_DAMAGE_MULTI = configManager.getFloat("AltMDamageNpc");
        ALT_DAGGER_DMG_VS_HEAVY = configManager.getFloat("DaggerVSHeavy");
        ALT_DAGGER_DMG_VS_ROBE = configManager.getFloat("DaggerVSRobe");
        ALT_DAGGER_DMG_VS_LIGHT = configManager.getFloat("DaggerVSLight");
        RUN_SPD_BOOST = configManager.getInteger("RunSpeedBoost");
        MAX_RUN_SPEED = configManager.getInteger("MaxRunSpeed");

        ALLOW_RAID_LETHAL = configManager.getBoolean("AllowLethalOnRaids");

        ALLOW_LETHAL_PROTECTION_MOBS = configManager.getBoolean("AllowLethalProtectionMobs");

        LETHAL_PROTECTED_MOBS = configManager.getString("LethalProtectedMobs");

        LIST_LETHAL_PROTECTED_MOBS = new FastList<>();
        for (String id : LETHAL_PROTECTED_MOBS.split(",")) {
            LIST_LETHAL_PROTECTED_MOBS.add(Integer.parseInt(id));
        }

        SEND_SKILLS_CHANCE_TO_PLAYERS = configManager.getBoolean("SendSkillsChanceToPlayers");

			/* Remove equip during subclass change */
        REMOVE_WEAPON_SUBCLASS = configManager.getBoolean("RemoveWeaponSubclass");
        REMOVE_CHEST_SUBCLASS = configManager.getBoolean("RemoveChestSubclass");
        REMOVE_LEG_SUBCLASS = configManager.getBoolean("RemoveLegSubclass");

        DISABLE_BOW_CLASSES.addAll(configManager.getArray("DisableBowForClasses", ",", ConfigManager.PropertyType.INTEGER));


        LEAVE_BUFFS_ON_DIE = configManager.getBoolean("LeaveBuffsOnDie");
    }

    //============================================================
    public void loadgeodataConfig() {

        GEODATA = configManager.getInteger("GeoData");
        GEODATA_CELLFINDING = configManager.getBoolean("CellPathFinding");

        ALLOW_PLAYERS_PATHNODE = configManager.getBoolean("AllowPlayersPathnode");

        FORCE_GEODATA = configManager.getBoolean("ForceGeoData");
        String correctZ = configManager.getString("GeoCorrectZ");
        GEO_CORRECT_Z = CorrectSpawnsZ.valueOf(correctZ.toUpperCase());

        ACCEPT_GEOEDITOR_CONN = configManager.getBoolean("AcceptGeoeditorConn");
        GEOEDITOR_PORT = configManager.getInteger("GeoEditorPort");

        WORLD_SIZE_MIN_X = configManager.getInteger("WorldSizeMinX");
        WORLD_SIZE_MAX_X = configManager.getInteger("WorldSizeMaxX");
        WORLD_SIZE_MIN_Y = configManager.getInteger("WorldSizeMinY");
        WORLD_SIZE_MAX_Y = configManager.getInteger("WorldSizeMaxY");
        WORLD_SIZE_MIN_Z = configManager.getInteger("WorldSizeMinZ");
        WORLD_SIZE_MAX_Z = configManager.getInteger("WorldSizeMaxZ");

        COORD_SYNCHRONIZE = configManager.getInteger("CoordSynchronize");

        FALL_DAMAGE = configManager.getBoolean("FallDamage");
        ALLOW_WATER = configManager.getBoolean("AllowWater");
    }

    //============================================================
    public void loadBossConfig() {
        ALT_RAIDS_STATS_BONUS = configManager.getBoolean("AltRaidsStatsBonus");

        RBLOCKRAGE = configManager.getInteger("RBlockRage");

        if (RBLOCKRAGE > 0 && RBLOCKRAGE < 100) {
            LOGGER.info("ATTENTION: RBlockRage, if enabled (>0), must be >=100");
            LOGGER.info("	-- RBlockRage setted to 100 by default");
            RBLOCKRAGE = 100;
        }

        RBS_SPECIFIC_LOCK_RAGE = new HashMap<>();

        RBS_SPECIFIC_LOCK_RAGE.putAll(configManager.getHashMap("RaidBossesSpecificLockRage"));


        PLAYERS_CAN_HEAL_RB = configManager.getBoolean("PlayersCanHealRb");

        //============================================================
        ALLOW_DIRECT_TP_TO_BOSS_ROOM = configManager.getBoolean("AllowDirectTeleportToBossRoom");
        //Antharas
        ANTHARAS_OLD = configManager.getBoolean("AntharasOldScript");
        ANTHARAS_CLOSE = configManager.getInteger("AntharasClose");
        ANTHARAS_DESPAWN_TIME = configManager.getInteger("AntharasDespawnTime");
        ANTHARAS_RESP_FIRST = configManager.getInteger("AntharasRespFirst");
        ANTHARAS_RESP_SECOND = configManager.getInteger("AntharasRespSecond");
        ANTHARAS_WAIT_TIME = configManager.getInteger("AntharasWaitTime");
        ANTHARAS_POWER_MULTIPLIER = configManager.getFloat("AntharasPowerMultiplier");
        //============================================================
        //Baium
        BAIUM_SLEEP = configManager.getInteger("BaiumSleep");
        BAIUM_RESP_FIRST = configManager.getInteger("BaiumRespFirst");
        BAIUM_RESP_SECOND = configManager.getInteger("BaiumRespSecond");
        BAIUM_POWER_MULTIPLIER = configManager.getFloat("BaiumPowerMultiplier");
        //============================================================
        //Core
        CORE_RESP_MINION = configManager.getInteger("CoreRespMinion");
        CORE_RESP_FIRST = configManager.getInteger("CoreRespFirst");
        CORE_RESP_SECOND = configManager.getInteger("CoreRespSecond");
        CORE_LEVEL = configManager.getInteger("CoreLevel");
        CORE_RING_CHANCE = configManager.getInteger("CoreRingChance");
        CORE_POWER_MULTIPLIER = configManager.getFloat("CorePowerMultiplier");
        //============================================================
        //Queen Ant
        QA_RESP_NURSE = configManager.getInteger("QueenAntRespNurse");
        QA_RESP_ROYAL = configManager.getInteger("QueenAntRespRoyal");
        QA_RESP_FIRST = configManager.getInteger("QueenAntRespFirst");
        QA_RESP_SECOND = configManager.getInteger("QueenAntRespSecond");
        QA_LEVEL = configManager.getInteger("QALevel");
        QA_RING_CHANCE = configManager.getInteger("QARingChance");
        QA_POWER_MULTIPLIER = configManager.getFloat("QueenAntPowerMultiplier");
        //============================================================
        //ZAKEN
        ZAKEN_RESP_FIRST = configManager.getInteger("ZakenRespFirst");
        ZAKEN_RESP_SECOND = configManager.getInteger("ZakenRespSecond");
        ZAKEN_LEVEL = configManager.getInteger("ZakenLevel");
        ZAKEN_EARRING_CHANCE = configManager.getInteger("ZakenEarringChance");
        ZAKEN_POWER_MULTIPLIER = configManager.getFloat("ZakenPowerMultiplier");
        //============================================================
        //ORFEN
        ORFEN_RESP_FIRST = configManager.getInteger("OrfenRespFirst");
        ORFEN_RESP_SECOND = configManager.getInteger("OrfenRespSecond");
        ORFEN_LEVEL = configManager.getInteger("OrfenLevel");
        ORFEN_EARRING_CHANCE = configManager.getInteger("OrfenEarringChance");
        ORFEN_POWER_MULTIPLIER = configManager.getFloat("OrfenPowerMultiplier");
        //============================================================
        //VALAKAS
        VALAKAS_RESP_FIRST = configManager.getInteger("ValakasRespFirst");
        VALAKAS_RESP_SECOND = configManager.getInteger("ValakasRespSecond");
        VALAKAS_WAIT_TIME = configManager.getInteger("ValakasWaitTime");
        VALAKAS_POWER_MULTIPLIER = configManager.getFloat("ValakasPowerMultiplier");
        VALAKAS_DESPAWN_TIME = configManager.getInteger("ValakasDespawnTime");
        //============================================================
        //FRINTEZZA
        FRINTEZZA_RESP_FIRST = configManager.getInteger("FrintezzaRespFirst");
        FRINTEZZA_RESP_SECOND = configManager.getInteger("FrintezzaRespSecond");
        FRINTEZZA_POWER_MULTIPLIER = configManager.getFloat("FrintezzaPowerMultiplier");

        BYPASS_FRINTEZZA_PARTIES_CHECK = configManager.getBoolean("BypassPartiesCheck");
        FRINTEZZA_MIN_PARTIES = configManager.getInteger("FrintezzaMinParties");
        FRINTEZZA_MAX_PARTIES = configManager.getInteger("FrintezzaMaxParties");
        //============================================================

        LEVEL_DIFF_MULTIPLIER_MINION = configManager.getFloat("LevelDiffMultiplierMinion");
        RAID_INFO_IDS_LIST.addAll(configManager.getArray("RaidInfoIDs", ",", ConfigManager.PropertyType.INTEGER));

        //High Priestess van Halter
        HPH_FIXINTERVALOFHALTER = configManager.getInteger("FixIntervalOfHalter");
        if (HPH_FIXINTERVALOFHALTER < 300 || HPH_FIXINTERVALOFHALTER > 864000) {
            HPH_FIXINTERVALOFHALTER = 172800;
        }
        HPH_FIXINTERVALOFHALTER *= 6000;

        HPH_RANDOMINTERVALOFHALTER = configManager.getInteger("RandomIntervalOfHalter");
        if (HPH_RANDOMINTERVALOFHALTER < 300 || HPH_RANDOMINTERVALOFHALTER > 864000) {
            HPH_RANDOMINTERVALOFHALTER = 86400;
        }
        HPH_RANDOMINTERVALOFHALTER *= 6000;

        HPH_APPTIMEOFHALTER = configManager.getInteger("AppTimeOfHalter");
        if (HPH_APPTIMEOFHALTER < 5 || HPH_APPTIMEOFHALTER > 60) {
            HPH_APPTIMEOFHALTER = 20;
        }
        HPH_APPTIMEOFHALTER *= 6000;

        HPH_ACTIVITYTIMEOFHALTER = configManager.getInteger("ActivityTimeOfHalter");
        if (HPH_ACTIVITYTIMEOFHALTER < 7200 || HPH_ACTIVITYTIMEOFHALTER > 86400) {
            HPH_ACTIVITYTIMEOFHALTER = 21600;
        }
        HPH_ACTIVITYTIMEOFHALTER *= 1000;

        HPH_FIGHTTIMEOFHALTER = configManager.getInteger("FightTimeOfHalter");
        if (HPH_FIGHTTIMEOFHALTER < 7200 || HPH_FIGHTTIMEOFHALTER > 21600) {
            HPH_FIGHTTIMEOFHALTER = 7200;
        }
        HPH_FIGHTTIMEOFHALTER *= 6000;

        HPH_CALLROYALGUARDHELPERCOUNT = configManager.getInteger("CallRoyalGuardHelperCount");
        if (HPH_CALLROYALGUARDHELPERCOUNT < 1 || HPH_CALLROYALGUARDHELPERCOUNT > 6) {
            HPH_CALLROYALGUARDHELPERCOUNT = 6;
        }

        HPH_CALLROYALGUARDHELPERINTERVAL = configManager.getInteger("CallRoyalGuardHelperInterval");
        if (HPH_CALLROYALGUARDHELPERINTERVAL < 1 || HPH_CALLROYALGUARDHELPERINTERVAL > 60) {
            HPH_CALLROYALGUARDHELPERINTERVAL = 10;
        }
        HPH_CALLROYALGUARDHELPERINTERVAL *= 6000;

        HPH_INTERVALOFDOOROFALTER = configManager.getInteger("IntervalOfDoorOfAlter");
        if (HPH_INTERVALOFDOOROFALTER < 60 || HPH_INTERVALOFDOOROFALTER > 5400) {
            HPH_INTERVALOFDOOROFALTER = 5400;
        }
        HPH_INTERVALOFDOOROFALTER *= 6000;

        HPH_TIMEOFLOCKUPDOOROFALTAR = configManager.getInteger("TimeOfLockUpDoorOfAltar");
        if (HPH_TIMEOFLOCKUPDOOROFALTAR < 60 || HPH_TIMEOFLOCKUPDOOROFALTAR > 600) {
            HPH_TIMEOFLOCKUPDOOROFALTAR = 180;
        }
        HPH_TIMEOFLOCKUPDOOROFALTAR *= 6000;
    }

    //============================================================
    public void loadScriptConfig() {

        SCRIPT_DEBUG = configManager.getBoolean("EnableScriptDebug");
        SCRIPT_ALLOW_COMPILATION = configManager.getBoolean("AllowCompilation");
        SCRIPT_CACHE = configManager.getBoolean("UseCache");
        SCRIPT_ERROR_LOG = configManager.getBoolean("EnableScriptErrorLog");
        PYTHON_SCRIPT_CACHE = configManager.getBoolean("EnablePythonCacheDir");
        PYTHON_SCRIPT_CACHE_DIR = configManager.getString("PythonCacheDir");

        if (PYTHON_SCRIPT_CACHE) {
            System.setProperty("python.cachedir", PYTHON_SCRIPT_CACHE_DIR);
        } else {
            System.setProperty("python.cachedir.skip", "true");

        }

    }

    //============================================================
    public void loadPowerPak() {

        POWERPAK_ENABLED = configManager.getBoolean("PowerPakEnabled");
    }

    //============================================================
    public void loadExtendersConfig() {

    }

    //============================================================
    public void loadDaemonsConf() {


        AUTOSAVE_INITIAL_TIME = configManager.getLong("AutoSaveInitial");
        AUTOSAVE_DELAY_TIME = configManager.getLong("AutoSaveDelay");
        CHECK_CONNECTION_INITIAL_TIME = configManager.getLong("CheckConnectionInitial");
        CHECK_CONNECTION_DELAY_TIME = configManager.getLong("CheckConnectionDelay");
        CHECK_CONNECTION_INACTIVITY_TIME = configManager.getLong("CheckConnectionInactivityTime");
        CHECK_TELEPORT_ZOMBIE_DELAY_TIME = configManager.getLong("CheckTeleportZombiesDelay");
        DEADLOCKCHECK_INTIAL_TIME = configManager.getLong("DeadLockCheck");
        DEADLOCKCHECK_DELAY_TIME = configManager.getLong("DeadLockDelay");

    }

    /**
     * Loads all Filter Words
     */
    //==============================================================
    public static void loadFilter() {
        final String FILTER_FILE = FService.FILTER_FILE;
        LineNumberReader lnr = null;
        try {
            File filter_file = new File(FILTER_FILE);
            if (!filter_file.exists()) {
                return;
            }

            lnr = new LineNumberReader(new BufferedReader(new FileReader(filter_file)));
            String line = null;
            while ((line = lnr.readLine()) != null) {
                if (line.trim().length() == 0 || line.startsWith("#")) {
                    continue;
                }
                FILTER_LIST.add(line.trim());
            }
            LOGGER.info("Loaded " + FILTER_LIST.size() + " Filter Words.");
        } catch (Exception e) {
            e.printStackTrace();
            throw new Error("Failed to Load " + FILTER_FILE + " File.");
        } finally {
            if (lnr != null) {
                try {
                    lnr.close();
                } catch (IOException e) {
                    LOGGER.error("", e);
                }
            }
        }
    }

    public static void loadQuestion() {
        final String QUESTION_FILE = FService.QUESTION_FILE;
        LineNumberReader lnr = null;
        try {
            lnr = new LineNumberReader(new BufferedReader(new FileReader(new File(QUESTION_FILE))));
            String line = null;
            while ((line = lnr.readLine()) != null) {
                if (line.trim().length() < 6 || line.trim().length() > 15 || line.startsWith("#")) {
                    continue;
                }
                QUESTION_LIST.add(line.trim());
            }
            LOGGER.info("Loaded " + QUESTION_LIST.size() + " Question Words.");
        } catch (Exception e) {
            e.printStackTrace();
            throw new Error("Failed to Load " + QUESTION_FILE + " File.");
        } finally {
            if (lnr != null) {
                try {
                    lnr.close();
                } catch (IOException e) {
                    LOGGER.error("", e);
                }
            }
        }
    }

    //============================================================
    public void loadHexed() {
        try {

            SERVER_ID = configManager.getInteger("register.server.id");

            String hexid = configManager.getString("register.server.hex.id");

            if (hexid == null
                    || hexid.isEmpty()) {
                LOGGER.error("ATTENTION: HexID is NULL or Empty.. Remember to Register Gameserver..");
                System.exit(1);
            }

            HEX_ID = new BigInteger(configManager.getString("register.server.hex.id"), 16).toByteArray();

        } catch (Exception e) {
            LOGGER.error("", e);
        }
    }

    public static boolean setParameterValue(String pName, String pValue) {
        if (pName.equalsIgnoreCase("GmLoginSpecialEffect")) {
            GM_SPECIAL_EFFECT = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("RateXp")) {
            RATE_XP = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("RateSp")) {
            RATE_SP = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("RatePartyXp")) {
            RATE_PARTY_XP = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("RatePartySp")) {
            RATE_PARTY_SP = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("RateQuestsReward")) {
            RATE_QUESTS_REWARD = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("RateDropAdena")) {
            RATE_DROP_ADENA = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("RateConsumableCost")) {
            RATE_CONSUMABLE_COST = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("RateDropItems")) {
            RATE_DROP_ITEMS = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("RateDropSealStones")) {
            RATE_DROP_SEAL_STONES = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("RateDropSpoil")) {
            RATE_DROP_SPOIL = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("RateDropManor")) {
            RATE_DROP_MANOR = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("RateDropQuest")) {
            RATE_DROP_QUEST = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("RateKarmaExpLost")) {
            RATE_KARMA_EXP_LOST = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("RateSiegeGuardsPrice")) {
            RATE_SIEGE_GUARDS_PRICE = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("PlayerDropLimit")) {
            PLAYER_DROP_LIMIT = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("PlayerRateDrop")) {
            PLAYER_RATE_DROP = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("PlayerRateDropItem")) {
            PLAYER_RATE_DROP_ITEM = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("PlayerRateDropEquip")) {
            PLAYER_RATE_DROP_EQUIP = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("PlayerRateDropEquipWeapon")) {
            PLAYER_RATE_DROP_EQUIP_WEAPON = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("KarmaDropLimit")) {
            KARMA_DROP_LIMIT = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("KarmaRateDrop")) {
            KARMA_RATE_DROP = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("KarmaRateDropItem")) {
            KARMA_RATE_DROP_ITEM = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("KarmaRateDropEquip")) {
            KARMA_RATE_DROP_EQUIP = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("KarmaRateDropEquipWeapon")) {
            KARMA_RATE_DROP_EQUIP_WEAPON = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AutoDestroyDroppedItemAfter")) {
            AUTODESTROY_ITEM_AFTER = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("DestroyPlayerDroppedItem")) {
            DESTROY_DROPPED_PLAYER_ITEM = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("DestroyEquipableItem")) {
            DESTROY_EQUIPABLE_PLAYER_ITEM = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("SaveDroppedItem")) {
            SAVE_DROPPED_ITEM = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("EmptyDroppedItemTableAfterLoad")) {
            EMPTY_DROPPED_ITEM_TABLE_AFTER_LOAD = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("SaveDroppedItemInterval")) {
            SAVE_DROPPED_ITEM_INTERVAL = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("ClearDroppedItemTable")) {
            CLEAR_DROPPED_ITEM_TABLE = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("PreciseDropCalculation")) {
            PRECISE_DROP_CALCULATION = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("MultipleItemDrop")) {
            MULTIPLE_ITEM_DROP = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("CoordSynchronize")) {
            COORD_SYNCHRONIZE = Integer.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("DeleteCharAfterDays")) {
            DELETE_DAYS = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AllowDiscardItem")) {
            ALLOW_DISCARDITEM = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AllowFreight")) {
            ALLOW_FREIGHT = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AllowWarehouse")) {
            ALLOW_WAREHOUSE = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AllowWear")) {
            ALLOW_WEAR = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("WearDelay")) {
            WEAR_DELAY = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("WearPrice")) {
            WEAR_PRICE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AllowWater")) {
            ALLOW_WATER = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AllowRentPet")) {
            ALLOW_RENTPET = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AllowBoat")) {
            ALLOW_BOAT = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AllowCursedWeapons")) {
            ALLOW_CURSED_WEAPONS = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AllowManor")) {
            ALLOW_MANOR = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("BypassValidation")) {
            BYPASS_VALIDATION = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("CommunityType")) {
            COMMUNITY_TYPE = pValue.toLowerCase();
        } else if (pName.equalsIgnoreCase("BBSDefault")) {
            BBS_DEFAULT = pValue;
        } else if (pName.equalsIgnoreCase("ShowLevelOnCommunityBoard")) {
            SHOW_LEVEL_COMMUNITYBOARD = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("ShowStatusOnCommunityBoard")) {
            SHOW_STATUS_COMMUNITYBOARD = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("NamePageSizeOnCommunityBoard")) {
            NAME_PAGE_SIZE_COMMUNITYBOARD = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("NamePerRowOnCommunityBoard")) {
            NAME_PER_ROW_COMMUNITYBOARD = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("ShowNpcLevel")) {
            SHOW_NPC_LVL = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("ForceInventoryUpdate")) {
            FORCE_INVENTORY_UPDATE = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AutoDeleteInvalidQuestData")) {
            AUTODELETE_INVALID_QUEST_DATA = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("MaximumOnlineUsers")) {
            MAXIMUM_ONLINE_USERS = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("UnknownPacketProtection")) {
            ENABLE_UNK_PACKET_PROTECTION = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("UnknownPacketsBeforeBan")) {
            MAX_UNKNOWN_PACKETS = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("UnknownPacketsPunishment")) {
            UNKNOWN_PACKETS_PUNiSHMENT = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("ZoneTown")) {
            ZONE_TOWN = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("CheckKnownList")) {
            CHECK_KNOWN = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("UseDeepBlueDropRules")) {
            DEEPBLUE_DROP_RULES = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AllowGuards")) {
            ALLOW_GUARDS = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("CancelLesserEffect")) {
            EFFECT_CANCELING = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("WyvernSpeed")) {
            WYVERN_SPEED = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("StriderSpeed")) {
            STRIDER_SPEED = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("MaximumSlotsForNoDwarf")) {
            INVENTORY_MAXIMUM_NO_DWARF = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("MaximumSlotsForDwarf")) {
            INVENTORY_MAXIMUM_DWARF = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("MaximumSlotsForGMPlayer")) {
            INVENTORY_MAXIMUM_GM = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("MaximumWarehouseSlotsForNoDwarf")) {
            WAREHOUSE_SLOTS_NO_DWARF = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("MaximumWarehouseSlotsForDwarf")) {
            WAREHOUSE_SLOTS_DWARF = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("MaximumWarehouseSlotsForClan")) {
            WAREHOUSE_SLOTS_CLAN = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("MaximumFreightSlots")) {
            FREIGHT_SLOTS = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AugmentationNGSkillChance")) {
            AUGMENTATION_NG_SKILL_CHANCE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AugmentationMidSkillChance")) {
            AUGMENTATION_MID_SKILL_CHANCE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AugmentationHighSkillChance")) {
            AUGMENTATION_HIGH_SKILL_CHANCE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AugmentationTopSkillChance")) {
            AUGMENTATION_TOP_SKILL_CHANCE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AugmentationBaseStatChance")) {
            AUGMENTATION_BASESTAT_CHANCE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AugmentationNGGlowChance")) {
            AUGMENTATION_NG_GLOW_CHANCE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AugmentationMidGlowChance")) {
            AUGMENTATION_MID_GLOW_CHANCE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AugmentationHighGlowChance")) {
            AUGMENTATION_HIGH_GLOW_CHANCE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AugmentationTopGlowChance")) {
            AUGMENTATION_TOP_GLOW_CHANCE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("EnchantSafeMax")) {
            ENCHANT_SAFE_MAX = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("EnchantSafeMaxFull")) {
            ENCHANT_SAFE_MAX_FULL = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("GMOverEnchant")) {
            GM_OVER_ENCHANT = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("HpRegenMultiplier")) {
            HP_REGEN_MULTIPLIER = Double.parseDouble(pValue);
        } else if (pName.equalsIgnoreCase("MpRegenMultiplier")) {
            MP_REGEN_MULTIPLIER = Double.parseDouble(pValue);
        } else if (pName.equalsIgnoreCase("CpRegenMultiplier")) {
            CP_REGEN_MULTIPLIER = Double.parseDouble(pValue);
        } else if (pName.equalsIgnoreCase("RaidHpRegenMultiplier")) {
            RAID_HP_REGEN_MULTIPLIER = Double.parseDouble(pValue);
        } else if (pName.equalsIgnoreCase("RaidMpRegenMultiplier")) {
            RAID_MP_REGEN_MULTIPLIER = Double.parseDouble(pValue);
        } else if (pName.equalsIgnoreCase("RaidPhysicalDefenceMultiplier")) {
            RAID_P_DEFENCE_MULTIPLIER = Double.parseDouble(pValue) / 100;
        } else if (pName.equalsIgnoreCase("RaidMagicalDefenceMultiplier")) {
            RAID_M_DEFENCE_MULTIPLIER = Double.parseDouble(pValue) / 100;
        } else if (pName.equalsIgnoreCase("RaidMinionRespawnTime")) {
            RAID_MINION_RESPAWN_TIMER = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("StartingAdena")) {
            STARTING_ADENA = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("UnstuckInterval")) {
            UNSTUCK_INTERVAL = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("PlayerSpawnProtection")) {
            PLAYER_SPAWN_PROTECTION = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("PlayerFakeDeathUpProtection")) {
            PLAYER_FAKEDEATH_UP_PROTECTION = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("PartyXpCutoffMethod")) {
            PARTY_XP_CUTOFF_METHOD = pValue;
        } else if (pName.equalsIgnoreCase("PartyXpCutoffPercent")) {
            PARTY_XP_CUTOFF_PERCENT = Double.parseDouble(pValue);
        } else if (pName.equalsIgnoreCase("PartyXpCutoffLevel")) {
            PARTY_XP_CUTOFF_LEVEL = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("RespawnRestoreCP")) {
            RESPAWN_RESTORE_CP = Double.parseDouble(pValue) / 100;
        } else if (pName.equalsIgnoreCase("RespawnRestoreHP")) {
            RESPAWN_RESTORE_HP = Double.parseDouble(pValue) / 100;
        } else if (pName.equalsIgnoreCase("RespawnRestoreMP")) {
            RESPAWN_RESTORE_MP = Double.parseDouble(pValue) / 100;
        } else if (pName.equalsIgnoreCase("MaxPvtStoreSlotsDwarf")) {
            MAX_PVTSTORE_SLOTS_DWARF = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("MaxPvtStoreSlotsOther")) {
            MAX_PVTSTORE_SLOTS_OTHER = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("StoreSkillCooltime")) {
            STORE_SKILL_COOLTIME = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AnnounceMammonSpawn")) {
            ANNOUNCE_MAMMON_SPAWN = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltGameTiredness")) {
            ALT_GAME_TIREDNESS = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltGameCreation")) {
            ALT_GAME_CREATION = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltGameCreationSpeed")) {
            ALT_GAME_CREATION_SPEED = Double.parseDouble(pValue);
        } else if (pName.equalsIgnoreCase("AltGameCreationXpRate")) {
            ALT_GAME_CREATION_XP_RATE = Double.parseDouble(pValue);
        } else if (pName.equalsIgnoreCase("AltGameCreationSpRate")) {
            ALT_GAME_CREATION_SP_RATE = Double.parseDouble(pValue);
        } else if (pName.equalsIgnoreCase("AltWeightLimit")) {
            ALT_WEIGHT_LIMIT = Double.parseDouble(pValue);
        } else if (pName.equalsIgnoreCase("AltBlacksmithUseRecipes")) {
            ALT_BLACKSMITH_USE_RECIPES = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltGameSkillLearn")) {
            ALT_GAME_SKILL_LEARN = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("RemoveCastleCirclets")) {
            REMOVE_CASTLE_CIRCLETS = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltGameCancelByHit")) {
            ALT_GAME_CANCEL_BOW = pValue.equalsIgnoreCase("bow") || pValue.equalsIgnoreCase("all");
            ALT_GAME_CANCEL_CAST = pValue.equalsIgnoreCase("cast") || pValue.equalsIgnoreCase("all");
        } else if (pName.equalsIgnoreCase("AltShieldBlocks")) {
            ALT_GAME_SHIELD_BLOCKS = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltPerfectShieldBlockRate")) {
            ALT_PERFECT_SHLD_BLOCK = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("Delevel")) {
            ALT_GAME_DELEVEL = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("MagicFailures")) {
            ALT_GAME_MAGICFAILURES = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltGameMobAttackAI")) {
            ALT_GAME_MOB_ATTACK_AI = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltMobAgroInPeaceZone")) {
            ALT_MOB_AGRO_IN_PEACEZONE = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltGameExponentXp")) {
            ALT_GAME_EXPONENT_XP = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("AltGameExponentSp")) {
            ALT_GAME_EXPONENT_SP = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("AllowClassMasters")) {
            ALLOW_CLASS_MASTERS = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltGameFreights")) {
            ALT_GAME_FREIGHTS = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltGameFreightPrice")) {
            ALT_GAME_FREIGHT_PRICE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AltPartyRange")) {
            ALT_PARTY_RANGE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AltPartyRange2")) {
            ALT_PARTY_RANGE2 = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("CraftingEnabled")) {
            IS_CRAFTING_ENABLED = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("LifeCrystalNeeded")) {
            LIFE_CRYSTAL_NEEDED = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("SpBookNeeded")) {
            SP_BOOK_NEEDED = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AutoLoot")) {
            AUTO_LOOT = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AutoLootHerbs")) {
            AUTO_LOOT_HERBS = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltKarmaPlayerCanBeKilledInPeaceZone")) {
            ALT_GAME_KARMA_PLAYER_CAN_BE_KILLED_IN_PEACEZONE = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltKarmaPlayerCanShop")) {
            ALT_GAME_KARMA_PLAYER_CAN_SHOP = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltKarmaPlayerCanUseGK")) {
            ALT_GAME_KARMA_PLAYER_CAN_USE_GK = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltKarmaFlagPlayerCanUseBuffer")) {
            FLAGED_PLAYER_USE_BUFFER = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltKarmaPlayerCanTeleport")) {
            ALT_GAME_KARMA_PLAYER_CAN_TELEPORT = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltKarmaPlayerCanTrade")) {
            ALT_GAME_KARMA_PLAYER_CAN_TRADE = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltKarmaPlayerCanUseWareHouse")) {
            ALT_GAME_KARMA_PLAYER_CAN_USE_WAREHOUSE = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltRequireCastleForDawn")) {
            ALT_GAME_REQUIRE_CASTLE_DAWN = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltRequireClanCastle")) {
            ALT_GAME_REQUIRE_CLAN_CASTLE = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltFreeTeleporting")) {
            ALT_GAME_FREE_TELEPORT = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltSubClassWithoutQuests")) {
            ALT_GAME_SUBCLASS_WITHOUT_QUESTS = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltRestoreEffectOnSub")) {
            ALT_RESTORE_EFFECTS_ON_SUBCLASS_CHANGE = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltNewCharAlwaysIsNewbie")) {
            ALT_GAME_NEW_CHAR_ALWAYS_IS_NEWBIE = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AltMembersCanWithdrawFromClanWH")) {
            ALT_MEMBERS_CAN_WITHDRAW_FROM_CLANWH = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("DwarfRecipeLimit")) {
            DWARF_RECIPE_LIMIT = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("CommonRecipeLimit")) {
            COMMON_RECIPE_LIMIT = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("ChampionEnable")) {
            L2JMOD_CHAMPION_ENABLE = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("ChampionFrequency")) {
            L2JMOD_CHAMPION_FREQUENCY = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("ChampionMinLevel")) {
            L2JMOD_CHAMP_MIN_LVL = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("ChampionMaxLevel")) {
            L2JMOD_CHAMP_MAX_LVL = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("ChampionHp")) {
            L2JMOD_CHAMPION_HP = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("ChampionHpRegen")) {
            L2JMOD_CHAMPION_HP_REGEN = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("ChampionRewards")) {
            L2JMOD_CHAMPION_REWARDS = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("ChampionAdenasRewards")) {
            L2JMOD_CHAMPION_ADENAS_REWARDS = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("ChampionAtk")) {
            L2JMOD_CHAMPION_ATK = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("ChampionSpdAtk")) {
            L2JMOD_CHAMPION_SPD_ATK = Float.parseFloat(pValue);
        } else if (pName.equalsIgnoreCase("ChampionRewardItem")) {
            L2JMOD_CHAMPION_REWARD = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("ChampionRewardItemID")) {
            L2JMOD_CHAMPION_REWARD_ID = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("ChampionRewardItemQty")) {
            L2JMOD_CHAMPION_REWARD_QTY = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AllowWedding")) {
            L2JMOD_ALLOW_WEDDING = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("WeddingPrice")) {
            L2JMOD_WEDDING_PRICE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("WeddingPunishInfidelity")) {
            L2JMOD_WEDDING_PUNISH_INFIDELITY = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("WeddingTeleport")) {
            L2JMOD_WEDDING_TELEPORT = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("WeddingTeleportPrice")) {
            L2JMOD_WEDDING_TELEPORT_PRICE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("WeddingTeleportDuration")) {
            L2JMOD_WEDDING_TELEPORT_DURATION = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("WeddingAllowSameSex")) {
            L2JMOD_WEDDING_SAMESEX = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("WeddingFormalWear")) {
            L2JMOD_WEDDING_FORMALWEAR = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("WeddingDivorceCosts")) {
            L2JMOD_WEDDING_DIVORCE_COSTS = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("TvTEvenTeams")) {
            TVT_EVEN_TEAMS = pValue;
        } else if (pName.equalsIgnoreCase("TvTAllowInterference")) {
            TVT_ALLOW_INTERFERENCE = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("TvTAllowPotions")) {
            TVT_ALLOW_POTIONS = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("TvTAllowSummon")) {
            TVT_ALLOW_SUMMON = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("TvTOnStartRemoveAllEffects")) {
            TVT_ON_START_REMOVE_ALL_EFFECTS = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("TvTOnStartUnsummonPet")) {
            TVT_ON_START_UNSUMMON_PET = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("TVTReviveDelay")) {
            TVT_REVIVE_DELAY = Long.parseLong(pValue);
        } else if (pName.equalsIgnoreCase("MinKarma")) {
            KARMA_MIN_KARMA = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("MaxKarma")) {
            KARMA_MAX_KARMA = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("XPDivider")) {
            KARMA_XP_DIVIDER = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("BaseKarmaLost")) {
            KARMA_LOST_BASE = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("CanGMDropEquipment")) {
            KARMA_DROP_GM = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AwardPKKillPVPPoint")) {
            KARMA_AWARD_PK_KILL = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("MinimumPKRequiredToDrop")) {
            KARMA_PK_LIMIT = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("PvPVsNormalTime")) {
            PVP_NORMAL_TIME = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("PvPVsPvPTime")) {
            PVP_PVP_TIME = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("GlobalChat")) {
            DEFAULT_GLOBAL_CHAT = pValue;
        } else if (pName.equalsIgnoreCase("TradeChat")) {
            DEFAULT_TRADE_CHAT = pValue;
        } else if (pName.equalsIgnoreCase("MenuStyle")) {
            GM_ADMIN_MENU_STYLE = pValue;
        } else if (pName.equalsIgnoreCase("AllowVersionCommand")) {
            ALLOW_VERSION_COMMAND = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("MaxPAtkSpeed")) {
            MAX_PATK_SPEED = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("MaxMAtkSpeed")) {
            MAX_MATK_SPEED = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("ServerNameEnabled")) {
            ALT_SERVER_NAME_ENABLED = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("ServerName")) {
            ALT_Server_Name = String.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("FlagedPlayerCanUseGK")) {
            FLAGED_PLAYER_CAN_USE_GK = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("AddExpAtPvp")) {
            ADD_EXP = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AddSpAtPvp")) {
            ADD_SP = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("CastleShieldRestriction")) {
            CASTLE_SHIELD = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("ClanHallShieldRestriction")) {
            CLANHALL_SHIELD = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("ApellaArmorsRestriction")) {
            APELLA_ARMORS = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("OathArmorsRestriction")) {
            OATH_ARMORS = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("CastleLordsCrownRestriction")) {
            CASTLE_CROWN = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("CastleCircletsRestriction")) {
            CASTLE_CIRCLETS = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AllowRaidBossPetrified")) {
            ALLOW_RAID_BOSS_PETRIFIED = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AllowLowLevelTrade")) {
            ALLOW_LOW_LEVEL_TRADE = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("AllowPotsInPvP")) {
            ALLOW_POTS_IN_PVP = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("StartingAncientAdena")) {
            STARTING_AA = Integer.parseInt(pValue);
        } else if (pName.equalsIgnoreCase("AnnouncePvPKill") && !ANNOUNCE_ALL_KILL) {
            ANNOUNCE_PVP_KILL = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AnnouncePkKill") && !ANNOUNCE_ALL_KILL) {
            ANNOUNCE_PK_KILL = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("AnnounceAllKill") && !ANNOUNCE_PVP_KILL && !ANNOUNCE_PK_KILL) {
            ANNOUNCE_ALL_KILL = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("DisableWeightPenalty")) {
            DISABLE_WEIGHT_PENALTY = Boolean.valueOf(pValue);
        } else if (pName.equalsIgnoreCase("CTFEvenTeams")) {
            CTF_EVEN_TEAMS = pValue;
        } else if (pName.equalsIgnoreCase("CTFAllowInterference")) {
            CTF_ALLOW_INTERFERENCE = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("CTFAllowPotions")) {
            CTF_ALLOW_POTIONS = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("CTFAllowSummon")) {
            CTF_ALLOW_SUMMON = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("CTFOnStartRemoveAllEffects")) {
            CTF_ON_START_REMOVE_ALL_EFFECTS = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("CTFOnStartUnsummonPet")) {
            CTF_ON_START_UNSUMMON_PET = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("DMAllowInterference")) {
            DM_ALLOW_INTERFERENCE = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("DMAllowPotions")) {
            DM_ALLOW_POTIONS = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("DMAllowSummon")) {
            DM_ALLOW_SUMMON = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("DMJoinWithCursedWeapon")) {
            DM_JOIN_CURSED = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("DMOnStartRemoveAllEffects")) {
            DM_ON_START_REMOVE_ALL_EFFECTS = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("DMOnStartUnsummonPet")) {
            DM_ON_START_UNSUMMON_PET = Boolean.parseBoolean(pValue);
        } else if (pName.equalsIgnoreCase("DMReviveDelay")) {
            DM_REVIVE_DELAY = Long.parseLong(pValue);
        } else
            return false;
        return true;
    }

    public static void unallocateFilterBuffer() {
        LOGGER.info("Cleaning Chat Filter..");
        FILTER_LIST.clear();
    }

    @PostConstruct
    public void load() {

        loadHexed();

        // Load network
        loadServerConfig();

        // Load system
        loadIdFactoryConfig();

        // Load developer parameters
        loadDevConfig();

        // Head
        loadOptionsConfig();
        loadOtherConfig();
        loadRatesConfig();
        loadAltConfig();
        load7sConfig();
        loadCHConfig();
        loadElitCHConfig();
        loadOlympConfig();
        loadEnchantConfig();
        loadBossConfig();

        // Head functions
        loadL2JFrozenConfig();
        loadPHYSICSConfig();
        loadAccessConfig();
        loadPvpConfig();
        loadCraftConfig();

        // Frozen config
        loadCTFConfig();
        loadDMConfig();
        loadFrozenConfig();
        loadTVTConfig();
        loadTWConfig();
        loadIRCConfig();

        loadPacketConfig();
        loadPOtherConfig();

        // Geo&path
        loadgeodataConfig();

        // Fun
        loadChampionConfig();
        loadWeddingConfig();
        loadREBIRTHConfig();
        loadAWAYConfig();
        loadBankingConfig();
        loadPCBPointConfig();
        loadOfflineConfig();
        loadPowerPak();

        loadServerVersionConfig();
        loadExtendersConfig();
        loadDaemonsConf();
        loadScriptConfig();

        if (GameServerConfig.USE_SAY_FILTER) {
            loadFilter();
        }
        if (GameServerConfig.BOT_PROTECTOR) {
            loadQuestion();
        }

    }

    public static enum CorrectSpawnsZ {
        TOWN, MONSTER, ALL, NONE
    }

    /**
     * Enumeration for type of ID Factory
     */
    public static enum IdFactoryType {
        Compaction,
        BitSet,
        Stack
    }

    /**
     * Enumeration for type of maps object
     */
    public static enum ObjectMapType {
        WorldObjectTree,
        WorldObjectMap
    }

    /**
     * Enumeration for type of set object
     */
    public static enum ObjectSetType {
        L2ObjectHashSet,
        WorldObjectSet
    }
}
