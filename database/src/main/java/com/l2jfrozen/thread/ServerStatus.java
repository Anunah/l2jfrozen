package com.l2jfrozen.thread;

/**
 * vdidenko 04.12.13
 */
public enum ServerStatus
{
	STATUS_AUTO(0x00), STATUS_GOOD(0x01), STATUS_NORMAL(0x02), STATUS_FULL(0x03), STATUS_DOWN(0x04), STATUS_GM_ONLY(0x05);
	int statusId;

	ServerStatus(int statusId)
	{
		this.statusId = statusId;
	}

	public static ServerStatus get(int id)
	{
		for (ServerStatus status : ServerStatus.values())
		{
			if (status.statusId == id)
			{
				return status;
			}
		}
		return null;
	}
}
