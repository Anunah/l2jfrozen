package com.l2jfrozen.database.dal.game.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.l2jfrozen.database.dal.common.GenericManager;
import com.l2jfrozen.database.dal.game.CharacterManagerDAO;
import com.l2jfrozen.database.dal.impl.ManagerImpl;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.database.model.game.character.CharacterFriendEntity;

/**
 * author vadim.didenko 1/10/14.
 */
@Repository
public class CharacterManagerDAOImpl extends ManagerImpl implements CharacterManagerDAO
{
	@Autowired
	private GenericManager genericManager;

	private Map<Long, CharacterEntity> characters = new HashMap<>();

	@Override
	@Transactional
	public CharacterEntity create(CharacterEntity character)
	{
		try
		{
			if (character.getId() != null)
			{
				update(character);

				return character;
			}
			getCurrentSession().persist(character);
			characters.put(character.getId(), character);
			return character;
		}
		catch (Exception ex)
		{
			LOGGER.error("Got hibernate exception.", ex);
		}
		return null;
	}

	@Override
	@Transactional
	public void delete(CharacterEntity character)
	{
		try
		{
			getCurrentSession().delete(character);
			characters.remove(character.getId());
			getCurrentSession().flush();
			getCurrentSession().clear();
		}
		catch (HibernateException e)
		{
			LOGGER.error(String.format("Cannot deleteEntity entity! %s", e.toString()));
		}
	}

	/**
	 * Loading character from the database
	 * 
	 * @param accountName
	 *            - account name
	 * @return list of characters for account
	 */

	@Override
	@Transactional(readOnly = true)
	public List<CharacterEntity> get(String accountName)
	{
		try
		{
			List<CharacterEntity> result = new ArrayList<>();
			List<CharacterEntity> entities = getCurrentSession().createCriteria(CharacterEntity.class).add(Restrictions.eq("accountName", accountName)).list();
			for (CharacterEntity entity : entities)
			{
				if (characters.get(entity.getId()) != null)
				{
					result.add(characters.get(entity.getId()));
					LOGGER.debug("Add cache data");
				}
				else
				{
					result.add(entity);
					initFields(entity);
					characters.put(entity.getId(), entity);
				}
			}
			return result;
		}
		catch (Exception e)
		{
			LOGGER.error("Got hibernate exception.", e);
		}

		return new ArrayList<>();
	}

	@Override
	@Transactional(readOnly = true)
	public CharacterEntity get(Long id)
	{
		try
		{
			if (characters.get(id) != null)
			{
				LOGGER.debug("Get cache data");
				return characters.get(id);
			}
			CharacterEntity characterEntity = (CharacterEntity) getCurrentSession().get(CharacterEntity.class, id);
			initFields(characterEntity);
			characters.put(characterEntity.getId(), characterEntity);
			return characterEntity;
		}
		catch (Exception e)
		{
			LOGGER.error("Got hibernate exception.", e);
		}
		return null;
	}

	// TODO this method must return list of character
	@Override
	@Transactional(readOnly = true)
	public List<CharacterEntity> getByClanId(int clanId)
	{
		try
		{

			return getCurrentSession().createCriteria(CharacterEntity.class).add(Restrictions.eq("clanId", clanId)).list();
		}
		catch (HibernateException e)
		{
			LOGGER.error("Got hibernate exception.", e);
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public CharacterEntity getByName(String name)
	{
		try
		{
			for (CharacterEntity entity : characters.values())
			{
				if (entity.getCharName().equals(name))
				{
					return entity;
				}
			}
			CharacterEntity characterEntity = (CharacterEntity) getCurrentSession().createCriteria(CharacterEntity.class).add(Restrictions.eq("charName", name)).uniqueResult();
			if (characterEntity != null)
			{
				initFields(characterEntity);
				characters.put(characterEntity.getId(), characterEntity);
			}
			return characterEntity;
		}
		catch (HibernateException e)
		{
			LOGGER.error("Got hibernate exception.", e);
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<CharacterEntity> getSponsor(long sponsorId)
	{
		try
		{
			return getCurrentSession().createCriteria(CharacterEntity.class).add(Restrictions.eq("sponsor", sponsorId)).list();
		}
		catch (HibernateException e)
		{
			LOGGER.error("Got hibernate exception.", e);
		}
		return new ArrayList<>();
	}

	@Override
	@Transactional
	public CharacterEntity update(CharacterEntity character)
	{
		try
		{
			characters.put(character.getId(), character);
			getCurrentSession().merge(character);
			return character;
		}
		catch (HibernateException e)
		{
			LOGGER.error("Got hibernate exception.", e);
		}
		return null;
	}

	@Override
	@Transactional
	public CharacterFriendEntity createFriend(CharacterFriendEntity friend)
	{
		try
		{
			if (friend.getId() == null)
			{
				getCurrentSession().persist(friend);
				return friend;
			}
			getCurrentSession().saveOrUpdate(friend);
			return friend;
		}
		catch (Exception ex)
		{
			LOGGER.error("Got hibernate exception.", ex);
		}
		return null;
	}

	@Override
	@Transactional
	public boolean deleteFriend(CharacterEntity character, CharacterEntity friendCharacter)
	{

		CharacterFriendEntity friendItem = null;
		CharacterFriendEntity selfItem = null;
		for (CharacterFriendEntity friend : character.getFriends().values())
		{
			if (friend.getFriendName().equals(friendCharacter.getCharName()))
			{
				friendItem = friend;
			}
		}
		for (CharacterFriendEntity friend : friendCharacter.getFriends().values())
		{
			if (friend.getFriendName().equals(character.getCharName()))
			{
				selfItem = friend;
			}
		}
		if (friendItem != null && selfItem != null)
		{
			character.getFriends().remove(friendItem.getId());
			friendCharacter.getFriends().remove(selfItem.getId());
		}

		getCurrentSession().delete(friendItem);
		getCurrentSession().delete(selfItem);

		return true;
	}

	@Override
	@Transactional
	public void deleteAllShortcuts(CharacterEntity character)
	{
		getCurrentSession().createQuery("DELETE FROM CharacterShortcut WHERE character=:character").setEntity("character", character);
	}

	@Override
	public void deleteShortcutByClass(CharacterEntity character, int classIndex)
	{
		getCurrentSession().createQuery("DELETE FROM CharacterShortcut WHERE character=:character AND classIndex=:classIndex").setEntity("character", character).setInteger("classIndex", classIndex);
	}

	private void initFields(CharacterEntity characterEntity)
	{
		genericManager.initialize(characterEntity, characterEntity.getFriends());
		genericManager.initialize(characterEntity, characterEntity.getSkills());
		genericManager.initialize(characterEntity, characterEntity.getItems());
		genericManager.initialize(characterEntity, characterEntity.getMacrosList());
		genericManager.initialize(characterEntity, characterEntity.getShortcuts());
		genericManager.initialize(characterEntity, characterEntity.getQuests());
		genericManager.initialize(characterEntity, characterEntity.getHennas());
		genericManager.initialize(characterEntity, characterEntity.getRecipeBooks());
		genericManager.initialize(characterEntity, characterEntity.getRecommends());
		genericManager.initialize(characterEntity, characterEntity.getRecommended());
		genericManager.initialize(characterEntity, characterEntity.getSubclasses());
		genericManager.initialize(characterEntity, characterEntity.getSkillsSave());
	}

	@Override
	@Transactional
	public void clearCache()
	{
		LOGGER.info("Clear character cache: delete {} records", characters.size());
		for (CharacterEntity entity : characters.values())
		{
			update(entity);
		}
		characters.clear();
	}

}
