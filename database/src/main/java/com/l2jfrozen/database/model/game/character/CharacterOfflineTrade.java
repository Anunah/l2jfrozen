package com.l2jfrozen.database.model.game.character;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * author vadim.didenko 1/12/14.
 */
@Entity
@Table(name = "character_offline_trade")
public class CharacterOfflineTrade implements Serializable
{

	@Id
	private int charId;

	private long time;

	private byte type;

	private String title;

	@Column(name = "charId")
	public int getCharId()
	{
		return charId;
	}

	public void setCharId(int charId)
	{
		this.charId = charId;
	}

	@Basic
	@Column(name = "time")
	public long getTime()
	{
		return time;
	}

	public void setTime(long time)
	{
		this.time = time;
	}

	@Basic
	@Column(name = "title")
	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	@Basic
	@Column(name = "type")
	public byte getType()
	{
		return type;
	}

	public void setType(byte type)
	{
		this.type = type;
	}
}
