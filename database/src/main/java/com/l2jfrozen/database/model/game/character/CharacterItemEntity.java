package com.l2jfrozen.database.model.game.character;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.l2jfrozen.database.model.AbstractIdentifiable;
import com.l2jfrozen.database.model.game.ItemLocation;

/**
 * author vadim.didenko 1/11/14.
 */
@Entity
@Table(name = "character_item")
public class CharacterItemEntity extends AbstractIdentifiable
{

	private int itemId;

	private int count;

	private int enchantLevel;

	private ItemLocation loc;

	private int locData;

	private int priceSell;

	private int priceBuy;

	private int timeOfUse;

	private int customType1;

	private int customType2;

	private int manaLeft;
	private CharacterEntity character;

	@Column(name = "count")
	public int getCount()
	{
		return count;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	@Column(name = "custom_type1")
	public int getCustomType1()
	{
		return customType1;
	}

	public void setCustomType1(int customType1)
	{
		this.customType1 = customType1;
	}

	@Column(name = "custom_type2")
	public int getCustomType2()
	{
		return customType2;
	}

	public void setCustomType2(int customType2)
	{
		this.customType2 = customType2;
	}

	@Column(name = "enchant_level")
	public int getEnchantLevel()
	{
		return enchantLevel;
	}

	public void setEnchantLevel(int enchantLevel)
	{
		this.enchantLevel = enchantLevel;
	}

	@Column(name = "item_id")
	public int getItemId()
	{
		return itemId;
	}

	public void setItemId(int itemId)
	{
		this.itemId = itemId;
	}

	@Column(name = "loc")
	@Enumerated(EnumType.STRING)
	public ItemLocation getLoc()
	{
		return loc;
	}

	public void setLoc(ItemLocation loc)
	{
		this.loc = loc;
	}

	@Column(name = "loc_data")
	public int getLocData()
	{
		return locData;
	}

	public void setLocData(int locData)
	{
		this.locData = locData;
	}

	@Column(name = "mana_left")
	public int getManaLeft()
	{
		return manaLeft;
	}

	public void setManaLeft(int manaLeft)
	{
		this.manaLeft = manaLeft;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_id", nullable = false)
	public CharacterEntity getCharacter()
	{
		return character;
	}

	public void setCharacter(CharacterEntity character)
	{
		this.character = character;
	}

	@Column(name = "price_buy")
	public int getPriceBuy()
	{
		return priceBuy;
	}

	public void setPriceBuy(int priceBuy)
	{
		this.priceBuy = priceBuy;
	}

	@Column(name = "price_sell")
	public int getPriceSell()
	{
		return priceSell;
	}

	public void setPriceSell(int priceSell)
	{
		this.priceSell = priceSell;
	}

	@Column(name = "time_of_use")
	public int getTimeOfUse()
	{
		return timeOfUse;
	}

	public void setTimeOfUse(int timeOfUse)
	{
		this.timeOfUse = timeOfUse;
	}
}
