package com.l2jfrozen.database.model.login;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.l2jfrozen.database.model.AbstractIdentifiable;

/**
 * author vadim.didenko 1/10/14.
 */
@Entity
@Table(name = "accounts")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Account extends AbstractIdentifiable
{
	private String login;
	private String password;
	private long lastActive;
	private int accessLevel;
	private String lastIp;
	private Integer lastServer;

	@Column(name = "access_level")
	public int getAccessLevel()
	{
		return accessLevel;
	}

	public void setAccessLevel(int accessLevel)
	{
		this.accessLevel = accessLevel;
	}

	@Column(name = "last_active")
	public long getLastActive()
	{
		return lastActive;
	}

	public void setLastActive(long lastActive)
	{
		this.lastActive = lastActive;
	}

	@Column(name = "lastIP")
	public String getLastIp()
	{
		return lastIp;
	}

	public void setLastIp(String lastIp)
	{
		this.lastIp = lastIp;
	}

	@Column(name = "last_server")
	public int getLastServer()
	{
		return lastServer;
	}

	public void setLastServer(int lastServer)
	{
		this.lastServer = lastServer;
	}

	@Id
	@Column(name = "login", nullable = false, length = 36)
	public String getLogin()
	{
		return login;
	}

	public void setLogin(String login)
	{
		this.login = login;
	}

	@Column(name = "password")
	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
}
