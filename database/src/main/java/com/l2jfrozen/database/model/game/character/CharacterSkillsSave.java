package com.l2jfrozen.database.model.game.character;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.l2jfrozen.database.model.AbstractIdentifiable;

/**
 * author vadim.didenko 1/12/14.
 */
@Entity
@Table(name = "character_skills_save")
public class CharacterSkillsSave extends AbstractIdentifiable
{
	private CharacterEntity character;

	private int skillId;

	private int skillLevel;

	private int effectCount;

	private int effectCurTime;

	private long reuseDelay;

	private long systime;

	private int restoreType;

	private int classIndex;

	private int buffIndex;

	@Basic
	@Column(name = "buff_index")
	public int getBuffIndex()
	{
		return buffIndex;
	}

	public void setBuffIndex(int buffIndex)
	{
		this.buffIndex = buffIndex;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "char_obj_id")
	public CharacterEntity getCharacter()
	{
		return character;
	}

	public void setCharacter(CharacterEntity charObjId)
	{
		this.character = charObjId;
	}

	@Column(name = "class_index")
	public int getClassIndex()
	{
		return classIndex;
	}

	public void setClassIndex(int classIndex)
	{
		this.classIndex = classIndex;
	}

	@Basic
	@Column(name = "effect_count")
	public int getEffectCount()
	{
		return effectCount;
	}

	public void setEffectCount(int effectCount)
	{
		this.effectCount = effectCount;
	}

	@Basic
	@Column(name = "effect_cur_time")
	public int getEffectCurTime()
	{
		return effectCurTime;
	}

	public void setEffectCurTime(int effectCurTime)
	{
		this.effectCurTime = effectCurTime;
	}

	@Basic
	@Column(name = "restore_type")
	public int getRestoreType()
	{
		return restoreType;
	}

	public void setRestoreType(int restoreType)
	{
		this.restoreType = restoreType;
	}

	@Basic
	@Column(name = "reuse_delay")
	public long getReuseDelay()
	{
		return reuseDelay;
	}

	public void setReuseDelay(long reuseDelay)
	{
		this.reuseDelay = reuseDelay;
	}

	@Column(name = "skill_id")
	public int getSkillId()
	{
		return skillId;
	}

	public void setSkillId(int skillId)
	{
		this.skillId = skillId;
	}

	@Basic
	@Column(name = "skill_level")
	public int getSkillLevel()
	{
		return skillLevel;
	}

	public void setSkillLevel(int skillLevel)
	{
		this.skillLevel = skillLevel;
	}

	@Basic
	@Column(name = "systime")
	public long getSystime()
	{
		return systime;
	}

	public void setSystime(long systime)
	{
		this.systime = systime;
	}
}
