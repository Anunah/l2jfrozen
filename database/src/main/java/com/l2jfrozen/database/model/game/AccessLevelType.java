package com.l2jfrozen.database.model.game;

/**
 * author vadim.didenko 09.02.14
 */
public enum AccessLevelType
{
	DEFAULT, MASTER_ACCESS, HEAD_GM, EVENT_GM, SUPPORT_GM, GENERAL_GM, TEST_GM, BANNED;
	public static AccessLevelType getType(int type)
	{
		for (AccessLevelType levelType : AccessLevelType.values())
		{
			if (levelType.ordinal() == type)
			{
				return levelType;
			}
		}
		return null;
	}
}
