package com.l2jfrozen.database.model.game.character;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.l2jfrozen.database.model.AbstractIdentifiable;
import com.l2jfrozen.database.model.game.AccessLevelType;

/**
 * author vadim.didenko 09.02.14
 */
@Entity
@Table(name = "character_access_level")
public class CharacterAccessLevel extends AbstractIdentifiable
{
	private AccessLevelType accessType;
	private String name;
	private String nameColor;
	private boolean useNameColor;
	private String titleColor;
	private boolean useTitleColor;
	private boolean gm;
	private boolean allowPeaceAttack;
	private boolean allowFixedRes;
	private boolean allowTransaction;
	private boolean allowAltg;
	private boolean giveDamage;
	private boolean takeAggro;
	private boolean gainExp;
	private boolean canDisableGmStatus;
	private Set<CharacterAdminCommandAccessRight> rights = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accessLevel", cascade = CascadeType.ALL)
	public Set<CharacterAdminCommandAccessRight> getRights()
	{
		return rights;
	}

	public void setRights(Set<CharacterAdminCommandAccessRight> rights)
	{
		this.rights = rights;
	}

	@Column(name = "access_level")
	@Enumerated(EnumType.ORDINAL)
	public AccessLevelType getAccessType()
	{
		return accessType;
	}

	public void setAccessType(AccessLevelType accessLevel)
	{
		this.accessType = accessLevel;
	}

	@Column(name = "name")
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Column(name = "nameColor")
	public String getNameColor()
	{
		return nameColor;
	}

	public void setNameColor(String nameColor)
	{
		this.nameColor = nameColor;
	}

	@Column(name = "titleColor")
	public String getTitleColor()
	{
		return titleColor;
	}

	public void setTitleColor(String titleColor)
	{
		this.titleColor = titleColor;
	}

	@Column(name = "allowAltg")
	public boolean isAllowAltg()
	{
		return allowAltg;
	}

	public void setAllowAltg(boolean allowAltg)
	{
		this.allowAltg = allowAltg;
	}

	@Column(name = "allowFixedRes")
	public boolean isAllowFixedRes()
	{
		return allowFixedRes;
	}

	public void setAllowFixedRes(boolean allowFixedRes)
	{
		this.allowFixedRes = allowFixedRes;
	}

	@Column(name = "allowPeaceAttack")
	public boolean isAllowPeaceAttack()
	{
		return allowPeaceAttack;
	}

	public void setAllowPeaceAttack(boolean allowPeaceAttack)
	{
		this.allowPeaceAttack = allowPeaceAttack;
	}

	@Column(name = "allowTransaction")
	public boolean isAllowTransaction()
	{
		return allowTransaction;
	}

	public void setAllowTransaction(boolean allowTransaction)
	{
		this.allowTransaction = allowTransaction;
	}

	@Column(name = "canDisableGmStatus")
	public boolean isCanDisableGmStatus()
	{
		return canDisableGmStatus;
	}

	public void setCanDisableGmStatus(boolean canDisableGmStatus)
	{
		this.canDisableGmStatus = canDisableGmStatus;
	}

	@Column(name = "gainExp")
	public boolean isGainExp()
	{
		return gainExp;
	}

	public void setGainExp(boolean gainExp)
	{
		this.gainExp = gainExp;
	}

	@Column(name = "giveDamage")
	public boolean isGiveDamage()
	{
		return giveDamage;
	}

	public void setGiveDamage(boolean giveDamage)
	{
		this.giveDamage = giveDamage;
	}

	@Column(name = "isGm")
	public boolean isGm()
	{
		return gm;
	}

	public void setGm(boolean isGm)
	{
		this.gm = isGm;
	}

	@Column(name = "takeAggro")
	public boolean isTakeAggro()
	{
		return takeAggro;
	}

	public void setTakeAggro(boolean takeAggro)
	{
		this.takeAggro = takeAggro;
	}

	@Column(name = "use_name_color")
	public boolean isUseNameColor()
	{
		return useNameColor;
	}

	public void setUseNameColor(boolean useNameColor)
	{
		this.useNameColor = useNameColor;
	}

	@Column(name = "useTitleColor")
	public boolean isUseTitleColor()
	{
		return useTitleColor;
	}

	public void setUseTitleColor(boolean useTitleColor)
	{
		this.useTitleColor = useTitleColor;
	}
}
