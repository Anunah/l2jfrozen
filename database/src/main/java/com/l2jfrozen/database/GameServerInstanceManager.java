package com.l2jfrozen.database;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;

import javolution.util.FastMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jfrozen.database.manager.GameServerManager;
import com.l2jfrozen.database.model.login.GameServer;
import com.l2jfrozen.thread.GameServerInfo;

/**
 * vdidenko 04.12.13
 */
public class GameServerInstanceManager
{
	private static final Logger LOGGER = LoggerFactory.getLogger(GameServerManager.class);
	private static GameServerInstanceManager serverManager;
	private GameServerManager gameServerManager = GameServerManager.getInstance();
	private final Map<Integer, GameServerInfo> gameServerList = new FastMap<Integer, GameServerInfo>().shared();

	public static GameServerInstanceManager getInstance()
	{
		if (serverManager == null)
		{
			serverManager = new GameServerInstanceManager();
		}
		return serverManager;
	}

	private GameServerInstanceManager()
	{
		loadRegisterServer();
	}

	public void loadRegisterServer()
	{
		for (GameServer server : gameServerManager.getAll())
		{
			GameServerInfo serverInfo = new GameServerInfo(server.getServerId(), new BigInteger(server.getHexid(), 16).toByteArray());
			gameServerList.put(server.getServerId(), serverInfo);
		}
	}

	public boolean register(int id, GameServerInfo gsi)
	{
		// avoid two servers registering with the same id
		synchronized (gameServerList)
		{
			if (!gameServerList.containsKey(id))
			{
				gameServerList.put(id, gsi);
				gsi.setId(id);
				return true;
			}
		}
		return false;
	}

	public GameServerInfo getServerInfo(int id)
	{
		if (gameServerList.containsKey(id))
		{
			return gameServerList.get(id);
		}
		LOGGER.error("Game server {} not found", id);
		return null;
	}

	public Collection<GameServerInfo> getServers()
	{
		return gameServerList.values();
	}

	public boolean registerWithFirstAvailableId(GameServerInfo gsi)
	{
		// avoid two servers registering with the same "free" id
		synchronized (gameServerList)
		{
			for (Map.Entry<Integer, String> entry : gameServerManager.getServerNames().entrySet())
			{
				if (!gameServerList.containsKey(entry.getKey()))
				{
					gameServerList.put(entry.getKey(), gsi);
					gsi.setId(entry.getKey());
					return true;
				}
			}
		}
		return false;
	}

	public void registerGameServer(GameServerInfo gameServerInfo)
	{
		gameServerManager.add(gameServerInfo.getId(), gameServerInfo.getExternalHost(), new BigInteger(gameServerInfo.getHexId()).toString());
	}

	public String getServerName(int id)
	{
		return gameServerManager.getServerName(id);
	}
}
