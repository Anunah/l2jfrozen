@echo off
title Frozen Backup Database

REM ###############################################
REM ## Configurate Database Connections please!!!!
REM ###############################################
set mysqlPath=./db/
set backupPath=./db/backup
set user=root
set pass=root
set db=l2jdb

echo Start backuping
%mysqlPath%\mysqldump.exe -u %user% -p %pass% %db%>%backupPath%/%db%.%date%.sql
exit