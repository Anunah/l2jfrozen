/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.loginserver;

import com.l2jfrozen.ServerType;
import com.l2jfrozen.configuration.LoginServerConfig;
import com.l2jfrozen.configuration.NetworkConfig;
import com.l2jfrozen.context.SpringApplicationContext;
import com.l2jfrozen.netcore.SelectorConfig;
import com.l2jfrozen.netcore.SelectorThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class LoginServer implements ApplicationListener {
    public static final int PROTOCOL_REV = 0x0102;
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginServer.class.getName());

    @Autowired
    private GameServerListener _gameServerListener;
    @Autowired
    private L2LoginPacketHandler loginPacketHandler;
    @Autowired
    private SelectorHelper sh;
    @Autowired
    private LoginServerConfig loginServerConfig;
    @Autowired
    private NetworkConfig networkConfig;
    private SelectorThread<L2LoginClient> _selectorThread;

    private LoginServer() {

    }

    public GameServerListener getGameServerListener() {
        return _gameServerListener;
    }

    public static void shutdown(boolean restart) {
        LoginController.shutdown();
        System.gc();
        Runtime.getRuntime().exit(restart ? 2 : 0);
    }

    public void load() throws UnknownHostException {
        ServerType.serverMode = ServerType.MODE_LOGINSERVER;

        InetAddress bindAddress = null;
        if (!loginServerConfig.LOGIN_BIND_ADDRESS.equals("*")) {
            try {
                bindAddress = InetAddress.getByName(loginServerConfig.LOGIN_BIND_ADDRESS);
            } catch (UnknownHostException e1) {
                LOGGER.error("WARNING: The LoginServer bind address is invalid, using all avaliable IPs. Reason: ", e1);
                bindAddress = InetAddress.getLocalHost();
            }
        }

        final SelectorConfig sc = new SelectorConfig();
        sc.setMaxReadPerPass(networkConfig.MMO_MAX_READ_PER_PASS);
        sc.setMaxSendPerPass(networkConfig.MMO_MAX_SEND_PER_PASS);
        sc.setSleepTime(networkConfig.MMO_SELECTOR_SLEEP_TIME);
        sc.setHelperBufferCount(networkConfig.MMO_HELPER_BUFFER_COUNT);

        try {
            _selectorThread = new SelectorThread<>(sc, sh, loginPacketHandler, sh, sh);
        } catch (IOException e) {
            LOGGER.error("FATAL: Failed to open Selector. Reason: ", e);
            System.exit(1);
        }

        _gameServerListener.start();
        LOGGER.info("Listening for GameServers on " + loginServerConfig.GAME_SERVER_LOGIN_HOST + ":" + loginServerConfig.GAME_SERVER_LOGIN_PORT);

        try {
            _selectorThread.openServerSocket(bindAddress, loginServerConfig.PORT_LOGIN);
            _selectorThread.start();
            LOGGER.info("Login Server ready on " + (bindAddress == null ? "*" : bindAddress.getHostAddress()) + ":" + loginServerConfig.PORT_LOGIN);
        } catch (IOException e) {
            LOGGER.error("FATAL: Failed to open server socket. Reason: ", e);
            System.exit(1);
        }
    }

    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {

        if (applicationEvent instanceof ContextRefreshedEvent) {

            ApplicationContext context = ((ContextRefreshedEvent) applicationEvent).getApplicationContext();
            SpringApplicationContext applicationContext = new SpringApplicationContext();
            applicationContext.setApplicationContext(context);

            try {
                this.load();
            } catch (UnknownHostException e) {
                LOGGER.error("FATAL: Failed to start login server. Reason: ", e);
                System.exit(1);
            }

        }

    }
}
