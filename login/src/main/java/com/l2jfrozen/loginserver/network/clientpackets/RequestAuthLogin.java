/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.loginserver.network.clientpackets;

import com.l2jfrozen.configuration.LoginServerConfig;
import com.l2jfrozen.loginserver.L2LoginClient;
import com.l2jfrozen.loginserver.LoginController;
import com.l2jfrozen.loginserver.LoginController.AuthLoginResult;
import com.l2jfrozen.loginserver.network.serverpackets.AccountKicked;
import com.l2jfrozen.loginserver.network.serverpackets.AccountKicked.AccountKickedReason;
import com.l2jfrozen.loginserver.network.serverpackets.LoginFail.LoginFailReason;
import com.l2jfrozen.loginserver.network.serverpackets.LoginOk;
import com.l2jfrozen.loginserver.network.serverpackets.ServerList;
import com.l2jfrozen.thread.GameServerInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import java.io.IOException;
import java.net.InetAddress;
import java.security.GeneralSecurityException;

/**
 * Format: x 0 (a leading null) x: the rsa encrypted block with the login an password
 */
public class RequestAuthLogin extends L2LoginClientPacket {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestAuthLogin.class.getName());

    private byte[] raw = new byte[128];

    private String user;
    private String password;
    private int _ncotp;

    public String getPassword() {
        return password;
    }

    public String getUser() {
        return user;
    }

    public int getOneTimePassword() {
        return _ncotp;
    }

    @Override
    public boolean readImpl() {
        if (super._buf.remaining() >= 128) {
            readB(raw);
            return true;
        }
        return false;
    }

    @Override
    public void run() {
        byte[] decrypted;
        try {
            Cipher rsaCipher = Cipher.getInstance("RSA/ECB/nopadding");
            rsaCipher.init(Cipher.DECRYPT_MODE, getClient().getRSAPrivateKey());
            decrypted = rsaCipher.doFinal(raw, 0x00, 0x80);
        } catch (GeneralSecurityException e) {
            LOGGER.error("", e);
            return;
        }

        user = new String(decrypted, 0x5E, 14).trim().toLowerCase();
        password = new String(decrypted, 0x6C, 16).trim();
        _ncotp = decrypted[0x7c];
        _ncotp |= decrypted[0x7d] << 8;
        _ncotp |= decrypted[0x7e] << 16;
        _ncotp |= decrypted[0x7f] << 24;

        L2LoginClient client = getClient();
        InetAddress address = getClient().getConnection().getInetAddress();
        if (address == null) {
            LOGGER.warn("Socket is not connected: " + client.getAccount());
            client.close(LoginFailReason.REASON_SYSTEM_ERROR);
            return;
        }
        String addhost = address.getHostAddress();
        AuthLoginResult result = LoginController.doAuthorized(user, password, getClient());

        switch (result) {
            case AUTH_SUCCESS:
                client.setSessionKey(LoginController.assignSessionKeyToClient(user, client));
                if (LoginServerConfig.SHOW_LICENCE) {
                    client.sendPacket(new LoginOk(getClient().getSessionKey()));
                } else {
                    getClient().sendPacket(new ServerList(getClient()));
                }
                if (LoginServerConfig.ENABLE_DDOS_PROTECTION_SYSTEM) {
                    String denyComms = LoginServerConfig.DDOS_COMMAND_BLOCK;
                    denyComms = denyComms.replace("$IP", addhost);

                    try {
                        Runtime.getRuntime().exec(denyComms);
                        if (LoginServerConfig.ENABLE_DEBUG_DDOS_PROTECTION_SYSTEM) {
                            LOGGER.info("Accepted IP access GS by " + addhost);
                            LOGGER.info("Command is" + denyComms);
                        }

                    } catch (IOException e1) {
                        LOGGER.info("Accepts by ip " + addhost + " no allowed");
                        LOGGER.info("Command is" + denyComms);
                    }

                }

                break;
            case INVALID_PASSWORD:
                client.close(LoginFailReason.REASON_USER_OR_PASS_WRONG);
                break;
            case ACCOUNT_BANNED:
                client.close(new AccountKicked(AccountKickedReason.REASON_PERMANENTLY_BANNED));
                break;
            case ALREADY_ON_LS:
                L2LoginClient oldClient;
                if ((oldClient = LoginController.getAuthedClient(user)) != null) {
                    // kick the other client
                    oldClient.close(LoginFailReason.REASON_ACCOUNT_IN_USE);
                    LoginController.removeAuthedLoginClient(user);
                }
                break;

            case ALREADY_ON_GS:
                GameServerInfo gsi;
                if ((gsi = LoginController.getAccountOnGameServer(user)) != null) {
                    client.close(LoginFailReason.REASON_ACCOUNT_IN_USE);

                    // kick from there
                    if (gsi.isAuthed()) {
                        gsi.getGameServerThread().kickPlayer(user);
                    }
                }
                gsi = null;
                break;
        }
    }
}
