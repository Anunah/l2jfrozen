package com.l2jfrozen.loginserver;

import com.l2jfrozen.context.ApplicationContextManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * vadim.didenko 2/22/14.
 */
public class Main {
    private static final String SPRING_CONFIG_LOCATION = "classpath:config/bean/spring-config-login.xml";
    private static final Logger LOGGER = LoggerFactory.getLogger(L2LoginClient.class.getName());

    public static void main(String[] args) {
        LOGGER.info("Start server");
        LOGGER.info("Spring initialize");
        ApplicationContextManager.init(SPRING_CONFIG_LOCATION);
        ApplicationContextManager.getApplicationContext();
        // SpringApplicationContext applicationContext = new SpringApplicationContext();
        // applicationContext.setApplicationContext(context);
    }
}