IMPORTANT!!!
-----
###Development must be carried out only in the branch "development."

 >If necessary, create new branches but merges them into a branch "development"

>After reaching a stable version - relocating all changes to a branch "master"

Create git history video


gource -s 0.00005 -a 1 --highlight-all-users --hide filenames -1280x720 -o l2jfrozen.ppm
ffmpeg -y -r 30 -f image2pipe -vcodec ppm -i l2jfrozen.ppm -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 1 -threads 0 -bf 0 l2jfrozen.mp4